TMGMT Plunet (tmgmt_plunet)
---------------------

This module provides integration with Plunet translation provider for the Translation Management Tool project.

The module has been specifically implemented for Syntax and might not work yet for other clients.

The integration is limited to customer-restricted API.

REQUIREMENTS
------------

This module requires TMGMT (http://drupal.org/project/tmgmt) module
to be installed.
