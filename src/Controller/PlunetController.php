<?php

namespace Drupal\tmgmt_plunet\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\tmgmt_plunet\Service\CallbackOrder30\CallbackOrder30Service;
use Drupal\tmgmt_plunet\Service\CallbackRequest30\CallbackRequest30Service;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Handles callback routes for tmgmt_plunet module.
 */
class PlunetController extends ControllerBase {

  /**
   * Provides a callback function for request status changes.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response to return.
   */
  public function requestCallback(Request $request) {
    if ($request->query->get('xsd') === '1') {
      $schema_path = drupal_get_path('module', 'tmgmt_plunet') . '/schema/request/request_callback_schema.xsd';
      return new Response(file_get_contents($schema_path), Response::HTTP_OK, ['Content-Type' => 'text/xml']);
    }

    $wsdl_url = Url::fromRoute('tmgmt_plunet.request_callback.wsdl')->setAbsolute()->toString();
    // Create the Soap server.
    $callback = new CallbackRequest30Service([], $wsdl_url);
    $server = new \SoapServer($wsdl_url, ['soap_version' => SOAP_1_2]);
    $server->setObject($callback);

    $response = new Response();
    $response->headers->set('Content-Type', 'text/xml; charset=ISO-8859-1');

    ob_start();
    $server->handle();
    $response->setContent(ob_get_clean());

    return $response;
  }

  /**
   * Returns the WSDL content for the order request callback.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response to return.
   */
  public function requestCallbackWsdl(Request $request) {
    $wsdl_path = drupal_get_path('module', 'tmgmt_plunet') . '/schema/request/request_callback_wsdl.xml';
    $wsdl_content = file_get_contents($wsdl_path);
    if (!$wsdl_content) {
      throw new NotFoundHttpException();
    }

    $url = Url::fromRoute('tmgmt_plunet.request_callback')->setAbsolute()->toString();
    $wsdl_content = str_replace('http://yoursite.example.org/tmgmt-plunet/CallbackRequest30', $url, $wsdl_content);

    return new Response($wsdl_content, Response::HTTP_OK, ['Content-Type' => 'text/xml']);
  }

  /**
   * Provides a callback function for order status changes.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response to return.
   */
  public function orderCallback(Request $request) {
    if ($request->query->get('xsd') === '1') {
      $schema_path = drupal_get_path('module', 'tmgmt_plunet') . '/schema/order/order_callback_schema.xsd';
      return new Response(file_get_contents($schema_path), Response::HTTP_OK, ['Content-Type' => 'text/xml']);
    }

    $wsdl_url = Url::fromRoute('tmgmt_plunet.order_callback.wsdl')->setAbsolute()->toString();
    // Create the Soap server.
    $callback = new CallbackOrder30Service([], $wsdl_url);
    $server = new \SoapServer($wsdl_url, ['soap_version' => SOAP_1_2]);
    $server->setObject($callback);

    $response = new Response();
    $response->headers->set('Content-Type', 'text/xml; charset=ISO-8859-1');

    ob_start();
    $server->handle();
    $response->setContent(ob_get_clean());

    return $response;
  }

  /**
   * Returns the WSDL content for the order request callback.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response to return.
   */
  public function orderCallbackWsdl(Request $request) {
    $wsdl_path = drupal_get_path('module', 'tmgmt_plunet') . '/schema/order/order_callback_wsdl.xml';
    $wsdl_content = file_get_contents($wsdl_path);
    if (!$wsdl_content) {
      throw new NotFoundHttpException();
    }

    $url = Url::fromRoute('tmgmt_plunet.order_callback')->setAbsolute()->toString();
    $wsdl_content = str_replace('http://yoursite.example.org/tmgmt-plunet/CallbackOrder30', $url, $wsdl_content);

    return new Response($wsdl_content, Response::HTTP_OK, ['Content-Type' => 'text/xml']);
  }

}
