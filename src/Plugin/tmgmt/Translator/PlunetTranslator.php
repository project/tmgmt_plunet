<?php

namespace Drupal\tmgmt_plunet\Plugin\tmgmt\Translator;

use Drupal\Component\Utility\SortArray;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\tmgmt\Entity\RemoteMapping;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\TranslatorPluginBase;
use Drupal\tmgmt_file\Format\FormatManager;
use Drupal\tmgmt_plunet\Service\DataDocument30\DataDocument30Service;
use Drupal\tmgmt_plunet\Service\DataOrder30\DataOrder30Service;
use Drupal\tmgmt_plunet\Service\DataRequest30\DataRequest30Service;
use Drupal\tmgmt_plunet\Service\DataRequest30\RequestIN;
use Drupal\tmgmt_plunet\Service\DataRequest30\RequestStatusType;
use Drupal\tmgmt_plunet\Service\PlunetAPI\PlunetAPIService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plunet translation plugin controller.
 *
 * @TranslatorPlugin(
 *   id = "plunet",
 *   label = @Translation("Plunet"),
 *   description = @Translation("Plunet Translation services."),
 *   ui = "Drupal\tmgmt_plunet\PlunetTranslatorUi",
 *   logo = "icons/logo.svg",
 * )
 */
class PlunetTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface {

  const ORDER_FINAL = 12;
  const REQUEST_SOURCE = 2;

  /**
   * The translator.
   *
   * @var TranslatorInterface
   */
  private $translator;

  /**
   * The TMGMT file format manager.
   *
   * @var \Drupal\tmgmt_file\Format\FormatManager
   */
  protected $fileFormatManager;

  /**
   * The UUID generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidGenerator;

  /**
   * The expirable key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected $keyValueExpirable;

  /**
   * PlunetTranslator constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\tmgmt_file\Format\FormatManager $file_format_manager
   *   The file format manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID generator.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface $key_value_expirable
   *   The key value expirable factory.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, FormatManager $file_format_manager, UuidInterface $uuid, KeyValueStoreExpirableInterface $key_value_expirable) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fileFormatManager = $file_format_manager;
    $this->uuidGenerator = $uuid;
    $this->keyValueExpirable = $key_value_expirable;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.tmgmt_file.format'),
      $container->get('uuid'),
      $container->get('keyvalue.expirable')->get('tmgmt_plunet')
    );
  }

  /**
   * Sets a Translator.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   */
  public function setTranslator(TranslatorInterface $translator) {
    if (!isset($this->translator)) {
      $this->translator = $translator;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedRemoteLanguages(TranslatorInterface $translator) {
    $language_labels = [];
    foreach ($this->loadSupportedRemoteLanguages() as $langcode => $label) {
      $language_labels[$langcode] = $label['title'];
    }
    return $language_labels;
  }

  /**
   * Loads and supported remote languages.
   *
   * @return array
   *   A list of languages, keyed by the standard language code (e.g. en-US).
   */
  protected function loadSupportedRemoteLanguages() {
    $module_path = \Drupal::service('module_handler')->getModule('tmgmt_plunet')->getPath();
    $filepath = $module_path . '/languages.xml';

    $remote_languages = [];
    $xml = new \SimpleXMLElement($filepath, 0, TRUE);
    $languages = $xml->children()->language;
    foreach ($languages as $language) {
      $remote_languages[(string) $language->code]['title'] = (string) $language->name;
    }

    // Sort the languages by label.
    uasort($remote_languages, [SortArray::class, 'sortByTitleElement']);

    return $remote_languages;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultRemoteLanguagesMappings() {
    return [
      'en' => 'en-US',
      'de' => 'de-CH',
      'fr' => 'fr-CH',
      'it' => 'it-CH',
      'es' => 'es-ES',
    ];
  }

  /**
   * Returns a flag whether the submitted username and password is valid.
   *
   * @param string $api_url
   *   The API URL.
   * @param string $username
   *   The username.
   * @param string $password
   *   The password.
   *
   * @return bool
   *   TRUE if the given credentials are valid. Otherwise, FALSE.
   */
  public function isValidAccount(string $api_url, string $username, string $password): bool {
    if ($uuid = $this->login($api_url, $username, $password)) {
      $this->logout($uuid, $api_url);
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Returns the service WSDL URL.
   *
   * @param string $path
   *   The path.
   * @param string $api_url
   *   (optional) The API URL.
   *
   * @return string
   *   The service WSDL URL.
   */
  public function getServiceWsdlUrl($path, $api_url = '') {
    $api_url = $api_url ?: $this->translator->getSetting('api_url');
    return rtrim($api_url, '/') . '/' . $path . '?wsdl';
  }

  /**
   * Logout request.
   *
   * @param string $uuid
   *   The UUID.
   * @param string $api_url
   *   (optional) The API URL.
   */
  protected function logout(string $uuid, string $api_url = '') {
    $api_url = $api_url ?: $this->translator->getSetting('api_url');
    try {
      $plunet_api = new PlunetAPIService([], $this->getServiceWsdlUrl('PlunetAPI', $api_url));
      $plunet_api->logout($uuid);
    }
    catch (\SoapFault $e) {
      $this->messenger()->addError($e->getMessage());
    }
  }

  /**
   * Login request.
   *
   * @param string $url
   *   (optional) The API URL.
   * @param string $username
   *   (optional) The username.
   * @param string $password
   *   (optional) The password.
   *
   * @return bool|string
   *   The UUID if logged in. Otherwise, FALSE.
   */
  protected function login(string $url = '', string $username = '', string $password = '') {
    $api_url = $url ?: $this->translator->getSetting('api_url');
    $username = $username ?: $this->translator->getSetting('api_user');
    $password = $password ?: $this->translator->getSetting('api_password');

    try {
      $plunet_api = new PlunetAPIService([], $this->getServiceWsdlUrl('PlunetAPI', $api_url));
      $uuid = $plunet_api->login($username, $password);
    }
    catch (\SoapFault $e) {
      $this->messenger()->addError($e->getMessage());
    }

    if ($uuid && $uuid != 'refused') {
      return $uuid;
    }

    return FALSE;
  }

  /**
   * Creates a request for order in the remote system.
   *
   * @param string $uuid
   *   The remote  UUID.
   * @param \Drupal\tmgmt\JobInterface $job
   *   The job.
   *
   * @return \Drupal\tmgmt_plunet\Service\DataRequest30\RequestIN|null
   *   The created request object.
   */
  protected function createRequest($uuid, JobInterface $job) {
    $order_request = new RequestIN(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
    $order_request->briefDescription = $job->label();
    $order_request->subject = $job->getSetting('description');
    $order_request->deliveryDate = $job->getSetting('deadline');
    $order_request->status = RequestStatusType::PENDING;

    try {
      // Create a new request for order.
      $request_service = new DataRequest30Service([], $this->getServiceWsdlUrl('DataRequest30'));
      $result = $request_service->insert2($uuid, $order_request);

      if ($result && $result->statusCode != 0) {
        throw new TMGMTException("Failed to create the order request. {$result->statusMessage}", [], $result->statusCode);
      }

      $request_id = $result->data;
      $this->addLanguageCombination($uuid, $request_id, $job);
      $url = Url::fromRoute('tmgmt_plunet.request_callback', ['wsdl' => NULL])->setAbsolute()->toString();
      $this->registerStatusChangeCallback($uuid, $job, $request_service, $request_id, $url);
    }
    catch (\Exception $e) {
      $job->rejected('Error code @code: @message', [
        '@code' => $e->getCode(),
        '@message' => $e->getMessage(),
      ], 'error');
      return NULL;
    }

    $order_request->requestID = $request_id;
    $job->addMessage('Order request (@request_id) has been created.', ['@request_id' => $request_id]);

    return $order_request;
  }

  /**
   * Uploads a file for translation.
   *
   * @param string $uuid
   *   The UUID.
   * @param string $main_id
   *   The main ID.
   * @param string $data
   *   The data.
   * @param string $filename
   *   The file name.
   *
   * @return \Drupal\tmgmt_plunet\Service\DataDocument30\Result
   *   The result of the request.
   */
  public function uploadDocument($uuid, $main_id, $data, $filename) {
    try {
      $data_document = new DataDocument30Service([], $this->getServiceWsdlUrl('DataDocument30'));
      return $data_document->upload_Document($uuid, $main_id, static::REQUEST_SOURCE, $data, $filename, 0);
    }
    catch (\SoapFault $e) {
      throw new TMGMTException("Failed to upload the source file. {$e->getMessage()}", [], $e->getCode());
    }
  }

  /**
   * Downloads a translation file.
   *
   * @param string $uuid
   *   The UUID.
   * @param string $order_id
   *   The order ID.
   * @param string $filename
   *   The file name.
   *
   * @return \Drupal\tmgmt_plunet\Service\DataDocument30\Result
   *   The result of the request.
   */
  public function downloadDocument($uuid, $order_id, $filename) {
    try {
      $data_document = new DataDocument30Service([], $this->getServiceWsdlUrl('DataDocument30'));
      return $data_document->download_Document($uuid, $order_id, static::ORDER_FINAL, $filename);
    }
    catch (\SoapFault $e) {
      throw new TMGMTException("Failed to download the translation file. {$e->getMessage()}", [], $e->getCode());
    }
  }

  /**
   * Generates XLIFF filename.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   The job entity.
   * @param string $job_item_id
   *   The job item ID.
   *
   * @return string
   *   The generated XLF filename.
   */
  protected function generateFilename(JobInterface $job, string $job_item_id) {
    $filename = implode('_', [
      $job->id(),
      $job_item_id,
      $job->getRemoteSourceLanguage(),
      $job->getRemoteTargetLanguage(),
      $this->uuidGenerator->generate(),
    ]);
    $filename .= '.xliff';

    return $filename;
  }

  /**
   * Returns the remote UUID used to connect with Plunet API.
   *
   * @param bool $new
   *  (optional) Force to get a new UUIDI.
   *
   * @return string|null
   *  Returns the remote UUID.
   */
  public function getRemoteUuid(bool $new = FALSE) {
    if (!$new && ($uuid = $this->keyValueExpirable->get($this->getPluginId()))) {
      return $uuid;
    }

    if ($uuid = $this->login()) {
      // Keep the UUID for 1 hour.
      $this->clearRemoteUuid();
      $this->keyValueExpirable->setWithExpire($this->getPluginId(), $uuid, 60 * 60);
      return $uuid;
    }

    return NULL;
  }

  /**
   * Clears the saved remote UUID.
   */
  public function clearRemoteUuid() {
    $this->keyValueExpirable->delete($this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job) {
    $this->setTranslator($job->getTranslator());
    if (!$uuid = $this->getRemoteUuid()) {
      $job->rejected('Could not login to the Plunet API.');
      return;
    }

    if ($order_request = $this->createRequest($uuid, $job)) {
      $xliff = $this->fileFormatManager->createInstance('xlf');
      $job->set('reference', $order_request->requestID);
      foreach ($job->getItems() as $job_item) {
        $job_item_id = $job_item->id();
        $xliff_content = $xliff->export($job, ['tjiid' => ['value' => $job_item_id]]);
        $filename = $this->generateFilename($job, $job_item_id);

        try {
          // Upload the file to the source folder.
          $result = $this->uploadDocument($uuid, $order_request->requestID, $xliff_content, $job->getRemoteSourceLanguage() . '\\' . $filename);
          if ($result && $result->statusCode != 0) {
            throw new TMGMTException("Failed to upload the source file. {$result->statusMessage}", [], $result->statusCode);
          }
        }
        catch (\Exception $e) {
          $job_item->addMessage('Error code @code: @message', [
            '@code' => $e->getCode(),
            '@message' => $e->getMessage(),
          ], 'error');
          continue;
        }

        $job_item->addRemoteMapping(NULL, $filename);
        $job_item->active();
      }

      $job->submitted('Job has been successfully submitted for translation.');
    }
  }

  /**
   * Fetches translations for job items of a given job.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   A job containing job items that translations will be fetched for.
   */
  public function fetchTranslations(JobInterface $job) {
    $this->setTranslator($job->getTranslator());
    $uuid = $this->getRemoteUuid();
    if (!$uuid) {
      $this->messenger()->addWarning($this->t('Could not connect to the Plunet API.'));
      return;
    }

    /** @var \Drupal\tmgmt\Entity\RemoteMapping[] $remotes */
    $remotes = RemoteMapping::loadByLocalData($job->id());
    $request_id = $job->getReference();

    try {
      // Update the job status.
      $request_service = new DataRequest30Service([], $this->getServiceWsdlUrl('DataRequest30'));
      $result = $request_service->getRequestObject($uuid, $request_id);

      if ($result && $result->statusCode != 0) {
        throw new TMGMTException("Failed to check the status of the order request ($request_id). {$result->statusMessage}", [], $result->statusCode);
      }
    } catch (\Exception $e) {
      $job->addMessage('Error code @code: @message', [
        '@code' => $e->getCode(),
        '@message' => $e->getMessage(),
      ], 'error');
      return;
    }

    // Update the status of the job.
    $request_object = $result->data;
    $job->addMessage('Order request (@request_id) is in %status status.', [
      '@request_id' => $request_object->requestID,
      '%status' => RequestStatusType::$names[$request_object->status],
    ]);

    // Extract the first order ID assigned to this job.
    $order_id = is_array($request_object->orderIDList) ? reset($request_object->orderIDList) : $request_object->orderIDList;
    $remote = reset($remotes);
    // Register the order callback only the first time we receive the order ID.
    if ($order_id && $remote && !$remote->getRemoteIdentifier3()) {
      // Deregister the order request status change callback.
      $this->deregisterStatusChangeCallback($uuid, $job, $request_service, $request_id);
      $order_service = new DataOrder30Service([], $this->getServiceWsdlUrl('DataOrder30'));
      // Register order status change callback.
      $url = Url::fromRoute('tmgmt_plunet.order_callback', ['wsdl' => NULL])->setAbsolute()->toString();
      $this->registerStatusChangeCallback($uuid, $job, $order_service, $order_id, $url);
    }

    // Check for if there are completed translations.
    /** @var \Drupal\tmgmt\RemoteMappingInterface $remote */
    foreach ($remotes as $remote) {
      $job_item = $remote->getJobItem();
      $filename = $remote->getRemoteIdentifier1();

      // Set the order ID reference.
      if (!$remote->getRemoteIdentifier3() && $order_id) {
        $remote->set('remote_identifier_3', $order_id);
        $remote->save();
        $job_item->addMessage('This item has been assigned to the order (@order_id).', ['@order_id' => $order_id]);
      }

      try {
        if ($order_id) {
          $result = $this->downloadDocument($uuid, $order_id, $filename);

          if ($result && $result->statusCode != 0) {
            throw new TMGMTException("Failed to download translation. {$result->statusMessage}", [], $result->statusCode);
          }
          else {
            $this->importTranslation($job_item, $result->fileContent);
          }
        }
      }
      catch (\Exception $e) {
        $job_item->addMessage('Error code @code: @message', [
          '@code' => $e->getCode(),
          '@message' => $e->getMessage(),
        ], 'error');
      }
    }
  }

  /**
   * Retrieves the translation.
   */
  public function importTranslation(JobItemInterface $job_item, $content) {
    /** @var \Drupal\tmgmt_file\Plugin\tmgmt_file\Format\Xliff $xliff */
    $xliff = $this->fileFormatManager->createInstance('xlf');

    $validated_job = $xliff->validateImport($content, FALSE);
    if (!$validated_job) {
      return;
    }
    elseif ($validated_job->id() != $job_item->getJob()->id()) {
      $job_item->getJob()->addMessage('The remote translation (Job ID: @target_job_id) does not match the current job ID @job_id.', [
        '@target_job_job' => $validated_job->id(),
        '@job_id' => $job_item->getJob()->id(),
      ], 'error');
    }
    else {
      if ($data = $xliff->import($xliff, FALSE)) {
        // The remote translation was successfully imported.
        $job_item->getJob()->addTranslatedData($data, NULL, TMGMT_DATA_ITEM_STATE_TRANSLATED);
        $job_item->addMessage('The translation has been received.');
      }
      else {
        $job_item->addMessage('Could not process received translation data for the target file.', [], 'error');
      }
    }
  }

  /**
   * Sets a callback to notify us if the order/request status changes.
   */
  protected function registerStatusChangeCallback($uuid, JobInterface $job, $service, $object_id, $url) {
    try {
      $result = $service->registerCallback_Observer($uuid, $job->getSetting('site_token'), $url, $object_id);

      if ($result && $result->statusCode != 0) {
        throw new TMGMTException("Failed to set the observer callback. {$result->statusMessage}", [], $result->statusCode);
      }
    }
    catch (\Exception $e) {
      $job->addMessage('Error code @code: @message', [
        '@code' => $e->getCode(),
        '@message' => $e->getMessage(),
      ], 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function abortTranslation(JobInterface $job) {
    $this->setTranslator($job->getTranslator());
    // Do not allow continuous jobs abortion.
    if ($job->isContinuous()) {
      return FALSE;
    }

    $request_id = $job->getReference();
    try {
      // Abort job in the current system.
      $aborted = FALSE;
      if ($job->isAbortable()) {
        $job->setState(JobInterface::STATE_ABORTED, 'Translation job has been aborted.');
        $aborted = TRUE;
      }
      if ($request_id && $aborted) {
        try {
          // Cancel the order request.
          $uuid = $this->getRemoteUuid();
          $request_service = new DataRequest30Service([], $this->getServiceWsdlUrl('DataRequest30'));
          $result = $request_service->setStatus($uuid, RequestStatusType::CANCELED, $request_id);

          if ($result && $result->statusCode != 0) {
            throw new TMGMTException("Failed to check the status of the order request ($request_id). {$result->statusMessage}", [], $result->statusCode);
          }

          /** @var \Drupal\tmgmt\Entity\RemoteMapping $remote */
          foreach ($job->getItems() as $job_item) {
            if (!$job_item->isAborted()) {
              try {
                // Abort the job item.
                $job_item->setState(JobItemInterface::STATE_ABORTED, 'Aborted by user.');
              }
              catch (TMGMTException $e) {
                $job_item->addMessage('Abortion failed: @error', ['@error' => $e->getMessage()], 'error');
              }
            }
          }

        } catch (\Exception $e) {
          $job->addMessage('Error code @code: @message', [
            '@code' => $e->getCode(),
            '@message' => $e->getMessage(),
          ], 'error');
          return FALSE;
        }

        return TRUE;
      }
    }
    catch (TMGMTException $e) {
      $job->addMessage('Failed to abort translation job. @error', ['@error' => $e->getMessage()], 'error');
    }

    return FALSE;
  }

  /**
   * Removes the observer status change callback.
   */
  protected function deregisterStatusChangeCallback($uuid, JobInterface $job, $request_service, $object_id) {
    try {
      $result = $request_service->deregisterCallback_Observer($uuid, $object_id);

      if ($result && $result->statusCode != 0) {
        throw new TMGMTException("Failed to remove the observer callback. {$result->statusMessage}", [], $result->statusCode);
      }
    }
    catch (\Exception $e) {
      $job->addMessage('Error code @code: @message', [
        '@code' => $e->getCode(),
        '@message' => $e->getMessage(),
      ], 'error');
    }
  }

  /**
   * Sets the language combination for this request.
   */
  protected function addLanguageCombination($uuid, $request_id, JobInterface $job) {
    try {
      $request_service = new DataRequest30Service([], $this->getServiceWsdlUrl('DataRequest30'));
      $plunet_languages = $job->getSetting('plunet_languages');
      $result = $request_service->addLanguageCombination($uuid, $plunet_languages[$job->getSourceLangcode()], $plunet_languages[$job->getTargetLangcode()], $request_id);

      if ($result && $result->statusCode != 0) {
        throw new TMGMTException("Failed to set language combination. {$result->statusMessage}", [], $result->statusCode);
      }
    }
    catch (\Exception $e) {
      $job->addMessage('Error code @code: @message', [
        '@code' => $e->getCode(),
        '@message' => $e->getMessage(),
      ], 'error');
    }
  }

}
