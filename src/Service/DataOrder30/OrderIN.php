<?php

namespace Drupal\tmgmt_plunet\Service\DataOrder30;

class OrderIN
{

    /**
     * @var dateTime $creationDate
     * @access public
     */
    public $creationDate = null;

    /**
     * @var string $currency
     * @access public
     */
    public $currency = null;

    /**
     * @var int $customerContactID
     * @access public
     */
    public $customerContactID = null;

    /**
     * @var int $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var dateTime $deliveryDeadline
     * @access public
     */
    public $deliveryDeadline = null;

    /**
     * @var int $orderID
     * @access public
     */
    public $orderID = null;

    /**
     * @var int $projectManagerID
     * @access public
     */
    public $projectManagerID = null;

    /**
     * @var string $projectManagerMemo
     * @access public
     */
    public $projectManagerMemo = null;

    /**
     * @var string $projectName
     * @access public
     */
    public $projectName = null;

    /**
     * @var float $rate
     * @access public
     */
    public $rate = null;

    /**
     * @var string $referenceNumber
     * @access public
     */
    public $referenceNumber = null;

    /**
     * @var string $subject
     * @access public
     */
    public $subject = null;

    /**
     * @param dateTime $creationDate
     * @param string $currency
     * @param int $customerContactID
     * @param int $customerID
     * @param dateTime $deliveryDeadline
     * @param int $orderID
     * @param int $projectManagerID
     * @param string $projectManagerMemo
     * @param string $projectName
     * @param float $rate
     * @param string $referenceNumber
     * @param string $subject
     * @access public
     */
    public function __construct($creationDate, $currency, $customerContactID, $customerID, $deliveryDeadline, $orderID, $projectManagerID, $projectManagerMemo, $projectName, $rate, $referenceNumber, $subject)
    {
      $this->creationDate = $creationDate;
      $this->currency = $currency;
      $this->customerContactID = $customerContactID;
      $this->customerID = $customerID;
      $this->deliveryDeadline = $deliveryDeadline;
      $this->orderID = $orderID;
      $this->projectManagerID = $projectManagerID;
      $this->projectManagerMemo = $projectManagerMemo;
      $this->projectName = $projectName;
      $this->rate = $rate;
      $this->referenceNumber = $referenceNumber;
      $this->subject = $subject;
    }

}
