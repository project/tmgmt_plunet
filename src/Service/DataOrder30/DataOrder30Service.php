<?php

namespace Drupal\tmgmt_plunet\Service\DataOrder30;






















class DataOrder30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'OrderResult' => 'Drupal\tmgmt_plunet\Service\DataOrder30\OrderResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\DataOrder30\Result',
      'Order' => 'Drupal\tmgmt_plunet\Service\DataOrder30\Order',
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataOrder30\StringResult',
      'OrderIN' => 'Drupal\tmgmt_plunet\Service\DataOrder30\OrderIN',
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\DataOrder30\IntegerResult',
      'DateResult' => 'Drupal\tmgmt_plunet\Service\DataOrder30\DateResult',
      'BooleanResult' => 'Drupal\tmgmt_plunet\Service\DataOrder30\BooleanResult',
      'StringArrayResult' => 'Drupal\tmgmt_plunet\Service\DataOrder30\StringArrayResult',
      'SearchFilter_Order' => 'Drupal\tmgmt_plunet\Service\DataOrder30\SearchFilter_Order',
      'Property' => 'Drupal\tmgmt_plunet\Service\DataOrder30\Property',
      'SelectionEntry_TeamMember' => 'Drupal\tmgmt_plunet\Service\DataOrder30\SelectionEntry_TeamMember',
      'Textmodule' => 'Drupal\tmgmt_plunet\Service\DataOrder30\Textmodule',
      'SelectionEntry_TimeFrame' => 'Drupal\tmgmt_plunet\Service\DataOrder30\SelectionEntry_TimeFrame',
      'IntegerArrayResult' => 'Drupal\tmgmt_plunet\Service\DataOrder30\IntegerArrayResult',
      'DoubleResult' => 'Drupal\tmgmt_plunet\Service\DataOrder30\DoubleResult',
      'TemplateListResult' => 'Drupal\tmgmt_plunet\Service\DataOrder30\TemplateListResult',
      'Template' => 'Drupal\tmgmt_plunet\Service\DataOrder30\Template',
      'IntegerList' => 'Drupal\tmgmt_plunet\Service\DataOrder30\IntegerList',
      'OrderListResult' => 'Drupal\tmgmt_plunet\Service\DataOrder30\OrderListResult');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataOrder30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @param string $propertyNameEnglish
     * @param string $propertyValueEnglish
     * @access public
     * @return Result
     */
    public function setProperty($UUID, $orderID, $propertyNameEnglish, $propertyValueEnglish)
    {
      return $this->__soapCall('setProperty', array($UUID, $orderID, $propertyNameEnglish, $propertyValueEnglish));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @param string $propertyNameEnglish
     * @access public
     * @return StringResult
     */
    public function getProperty($UUID, $orderID, $propertyNameEnglish)
    {
      return $this->__soapCall('getProperty', array($UUID, $orderID, $propertyNameEnglish));
    }

    /**
     * @param string $UUID
     * @param OrderIN $OrderIN
     * @param boolean $enableNullOrEmptyValues
     * @access public
     * @return Result
     */
    public function update($UUID, OrderIN $OrderIN, $enableNullOrEmptyValues)
    {
      return $this->__soapCall('update', array($UUID, $OrderIN, $enableNullOrEmptyValues));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return Result
     */
    public function delete($UUID, $orderID)
    {
      return $this->__soapCall('delete', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @access public
     * @return IntegerResult
     */
    public function insert($UUID)
    {
      return $this->__soapCall('insert', array($UUID));
    }

    /**
     * @param string $UUID
     * @param SearchFilter_Order $SearchFilter
     * @access public
     * @return IntegerArrayResult
     */
    public function search($UUID, SearchFilter_Order $SearchFilter)
    {
      return $this->__soapCall('search', array($UUID, $SearchFilter));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return StringResult
     */
    public function getCurrency($UUID, $orderID)
    {
      return $this->__soapCall('getCurrency', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return StringResult
     */
    public function getExternalID($UUID, $orderID)
    {
      return $this->__soapCall('getExternalID', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @param string $externalID
     * @access public
     * @return Result
     */
    public function setExternalID($UUID, $orderID, $externalID)
    {
      return $this->__soapCall('setExternalID', array($UUID, $orderID, $externalID));
    }

    /**
     * @param string $UUID
     * @param OrderIN $OrderIN
     * @access public
     * @return IntegerResult
     */
    public function insert2($UUID, OrderIN $OrderIN)
    {
      return $this->__soapCall('insert2', array($UUID, $OrderIN));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @param int $orderID
     * @access public
     * @return Result
     */
    public function setCustomerID($UUID, $customerID, $orderID)
    {
      return $this->__soapCall('setCustomerID', array($UUID, $customerID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return StringResult
     */
    public function getProjectName($UUID, $orderID)
    {
      return $this->__soapCall('getProjectName', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return DoubleResult
     */
    public function getRate($UUID, $orderID)
    {
      return $this->__soapCall('getRate', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param dateTime $creationDate
     * @param int $orderID
     * @access public
     * @return Result
     */
    public function setCreationDate($UUID, $creationDate, $orderID)
    {
      return $this->__soapCall('setCreationDate', array($UUID, $creationDate, $orderID));
    }

    /**
     * @param string $UUID
     * @param string $name
     * @param int $orderID
     * @access public
     * @return Result
     */
    public function setProjectName($UUID, $name, $orderID)
    {
      return $this->__soapCall('setProjectName', array($UUID, $name, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return StringResult
     */
    public function getSubject($UUID, $orderID)
    {
      return $this->__soapCall('getSubject', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @access public
     * @return TemplateListResult
     */
    public function getTemplateList($UUID)
    {
      return $this->__soapCall('getTemplateList', array($UUID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setRequestID($UUID, $orderID, $requestID)
    {
      return $this->__soapCall('setRequestID', array($UUID, $orderID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return OrderResult
     */
    public function getOrderObject($UUID, $orderID)
    {
      return $this->__soapCall('getOrderObject', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return IntegerResult
     */
    public function getRequestId($UUID, $orderID)
    {
      return $this->__soapCall('getRequestId', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return BooleanResult
     */
    public function checkEN15038($UUID, $orderID)
    {
      return $this->__soapCall('checkEN15038', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param string $sourceLanguage
     * @param string $targetLanguage
     * @param int $orderID
     * @access public
     * @return IntegerResult
     */
    public function getItemStatus($UUID, $sourceLanguage, $targetLanguage, $orderID)
    {
      return $this->__soapCall('getItemStatus', array($UUID, $sourceLanguage, $targetLanguage, $orderID));
    }

    /**
     * @param string $UUID
     * @param string $orderNumber
     * @access public
     * @return OrderResult
     */
    public function getOrderObject2($UUID, $orderNumber)
    {
      return $this->__soapCall('getOrderObject2', array($UUID, $orderNumber));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return IntegerResult
     */
    public function getProjectStatus($UUID, $orderID)
    {
      return $this->__soapCall('getProjectStatus', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @param int $userID
     * @param int $actionLinkType
     * @access public
     * @return StringResult
     */
    public function getActionLink($UUID, $orderID, $userID, $actionLinkType)
    {
      return $this->__soapCall('getActionLink', array($UUID, $orderID, $userID, $actionLinkType));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @param int $projectStatus
     * @access public
     * @return Result
     */
    public function setProjectStatus($UUID, $orderID, $projectStatus)
    {
      return $this->__soapCall('setProjectStatus', array($UUID, $orderID, $projectStatus));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return IntegerResult
     */
    public function getCustomerID($UUID, $orderID)
    {
      return $this->__soapCall('getCustomerID', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param string $displayNo
     * @access public
     * @return IntegerResult
     */
    public function getOrderID($UUID, $displayNo)
    {
      return $this->__soapCall('getOrderID', array($UUID, $displayNo));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return DateResult
     */
    public function getCreationDate($UUID, $orderID)
    {
      return $this->__soapCall('getCreationDate', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param string $subject
     * @param int $orderID
     * @access public
     * @return Result
     */
    public function setSubject($UUID, $subject, $orderID)
    {
      return $this->__soapCall('setSubject', array($UUID, $subject, $orderID));
    }

    /**
     * @param string $UUID
     * @param string $sourceLanguage
     * @param string $targetLanguage
     * @param int $orderID
     * @access public
     * @return IntegerResult
     */
    public function addLanguageCombination($UUID, $sourceLanguage, $targetLanguage, $orderID)
    {
      return $this->__soapCall('addLanguageCombination', array($UUID, $sourceLanguage, $targetLanguage, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return IntegerResult
     */
    public function getCustomerContactID($UUID, $orderID)
    {
      return $this->__soapCall('getCustomerContactID', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $customerContactID
     * @param int $orderID
     * @access public
     * @return Result
     */
    public function setCustomerContactID($UUID, $customerContactID, $orderID)
    {
      return $this->__soapCall('setCustomerContactID', array($UUID, $customerContactID, $orderID));
    }

    /**
     * @param string $UUID
     * @param string $ServerAuthenticationString
     * @param string $ServerAddress
     * @param int $EventType
     * @access public
     * @return Result
     */
    public function registerCallback_Notify($UUID, $ServerAuthenticationString, $ServerAddress, $EventType)
    {
      return $this->__soapCall('registerCallback_Notify', array($UUID, $ServerAuthenticationString, $ServerAddress, $EventType));
    }

    /**
     * @param string $UUID
     * @param string $ServerAuthenticationString
     * @param string $ServerAddress
     * @param int $OrderID
     * @access public
     * @return Result
     */
    public function registerCallback_Observer($UUID, $ServerAuthenticationString, $ServerAddress, $OrderID)
    {
      return $this->__soapCall('registerCallback_Observer', array($UUID, $ServerAuthenticationString, $ServerAddress, $OrderID));
    }

    /**
     * @param string $UUID
     * @param int $EventType
     * @access public
     * @return Result
     */
    public function deregisterCallback_Notify($UUID, $EventType)
    {
      return $this->__soapCall('deregisterCallback_Notify', array($UUID, $EventType));
    }

    /**
     * @param string $UUID
     * @param int $OrderID
     * @access public
     * @return Result
     */
    public function deregisterCallback_Observer($UUID, $OrderID)
    {
      return $this->__soapCall('deregisterCallback_Observer', array($UUID, $OrderID));
    }

    /**
     * @param string $UUID
     * @param string $language
     * @param int $orderID
     * @access public
     * @return StringArrayResult
     */
    public function getDocuments_Within_ReferenceFolder_ByLanguage($UUID, $language, $orderID)
    {
      return $this->__soapCall('getDocuments_Within_ReferenceFolder_ByLanguage', array($UUID, $language, $orderID));
    }

    /**
     * @param string $UUID
     * @param string $language
     * @param int $orderID
     * @access public
     * @return StringArrayResult
     */
    public function getDocuments_Within_SourceFolder_ByLanguage($UUID, $language, $orderID)
    {
      return $this->__soapCall('getDocuments_Within_SourceFolder_ByLanguage', array($UUID, $language, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return StringArrayResult
     */
    public function getDocuments_Within_ReferenceFolder($UUID, $orderID)
    {
      return $this->__soapCall('getDocuments_Within_ReferenceFolder', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return DateResult
     */
    public function getDeliveryDeadline($UUID, $orderID)
    {
      return $this->__soapCall('getDeliveryDeadline', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param dateTime $deliveryDeadline
     * @param int $orderID
     * @access public
     * @return Result
     */
    public function setDeliveryDeadline($UUID, $deliveryDeadline, $orderID)
    {
      return $this->__soapCall('setDeliveryDeadline', array($UUID, $deliveryDeadline, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return StringArrayResult
     */
    public function getDocuments_Within_SourceFolder($UUID, $orderID)
    {
      return $this->__soapCall('getDocuments_Within_SourceFolder', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return StringResult
     */
    public function getOrderNo_for_View($UUID, $orderID)
    {
      return $this->__soapCall('getOrderNo_for_View', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param string $template
     * @param int $formatId
     * @param int $orderID
     * @access public
     * @return StringResult
     */
    public function createOrderConfirmation($UUID, $template, $formatId, $orderID)
    {
      return $this->__soapCall('createOrderConfirmation', array($UUID, $template, $formatId, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return StringArrayResult
     */
    public function getOrderConfirmations($UUID, $orderID)
    {
      return $this->__soapCall('getOrderConfirmations', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return StringArrayResult
     */
    public function getDocuments_Within_FinalFolder($UUID, $orderID)
    {
      return $this->__soapCall('getDocuments_Within_FinalFolder', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param string $systemLanguageCode
     * @param int $orderID
     * @access public
     * @return StringResult
     */
    public function getProjectCategory($UUID, $systemLanguageCode, $orderID)
    {
      return $this->__soapCall('getProjectCategory', array($UUID, $systemLanguageCode, $orderID));
    }

    /**
     * @param string $UUID
     * @param string $projectCategory
     * @param string $systemLanguageCode
     * @param int $orderID
     * @access public
     * @return Result
     */
    public function setProjectCategory($UUID, $projectCategory, $systemLanguageCode, $orderID)
    {
      return $this->__soapCall('setProjectCategory', array($UUID, $projectCategory, $systemLanguageCode, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @param string $reference
     * @access public
     * @return Result
     */
    public function setReferenceNumber($UUID, $orderID, $reference)
    {
      return $this->__soapCall('setReferenceNumber', array($UUID, $orderID, $reference));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return BooleanResult
     */
    public function getEN15038Requested($UUID, $orderID)
    {
      return $this->__soapCall('getEN15038Requested', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return StringArrayResult
     */
    public function getLanguageCombination($UUID, $orderID)
    {
      return $this->__soapCall('getLanguageCombination', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return DateResult
     */
    public function getOrderClosingDate($UUID, $orderID)
    {
      return $this->__soapCall('getOrderClosingDate', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @param string $currencyIsoCode
     * @param float $rate
     * @access public
     * @return Result
     */
    public function setCurrencyAndRate($UUID, $orderID, $currencyIsoCode, $rate)
    {
      return $this->__soapCall('setCurrencyAndRate', array($UUID, $orderID, $currencyIsoCode, $rate));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return StringResult
     */
    public function getDeliveryComment($UUID, $orderID)
    {
      return $this->__soapCall('getDeliveryComment', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param boolean $isEN15038
     * @param int $orderID
     * @access public
     * @return Result
     */
    public function setEN15038Requested($UUID, $isEN15038, $orderID)
    {
      return $this->__soapCall('setEN15038Requested', array($UUID, $isEN15038, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @param int $orderID
     * @access public
     * @return Result
     */
    public function setProjectmanagerID($UUID, $resourceID, $orderID)
    {
      return $this->__soapCall('setProjectmanagerID', array($UUID, $resourceID, $orderID));
    }

    /**
     * @param string $UUID
     * @param IntegerList $orderIDList
     * @access public
     * @return OrderListResult
     */
    public function getOrderObjectList($UUID, IntegerList $orderIDList)
    {
      return $this->__soapCall('getOrderObjectList', array($UUID, $orderIDList));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return IntegerResult
     */
    public function getProjectmanagerID($UUID, $orderID)
    {
      return $this->__soapCall('getProjectmanagerID', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return StringResult
     */
    public function getProjectManagerMemo($UUID, $orderID)
    {
      return $this->__soapCall('getProjectManagerMemo', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param OrderIN $OrderIN
     * @param int $TemplateID
     * @access public
     * @return IntegerResult
     */
    public function insert_byTemplate($UUID, OrderIN $OrderIN, $TemplateID)
    {
      return $this->__soapCall('insert_byTemplate', array($UUID, $OrderIN, $TemplateID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @param int $MasterProjectID
     * @access public
     * @return Result
     */
    public function setMasterProjectID($UUID, $orderID, $MasterProjectID)
    {
      return $this->__soapCall('setMasterProjectID', array($UUID, $orderID, $MasterProjectID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return StringResult
     */
    public function getReferenceNumber($UUID, $orderID)
    {
      return $this->__soapCall('getReferenceNumber', array($UUID, $orderID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @param string $memo
     * @access public
     * @return Result
     */
    public function setProjectManagerMemo($UUID, $orderID, $memo)
    {
      return $this->__soapCall('setProjectManagerMemo', array($UUID, $orderID, $memo));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @access public
     * @return IntegerResult
     */
    public function getMasterProjectID($UUID, $orderID)
    {
      return $this->__soapCall('getMasterProjectID', array($UUID, $orderID));
    }

}
