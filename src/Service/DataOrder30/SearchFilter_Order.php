<?php

namespace Drupal\tmgmt_plunet\Service\DataOrder30;

class SearchFilter_Order
{

    /**
     * @var int $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var int[] $itemStatus
     * @access public
     */
    public $itemStatus = null;

    /**
     * @var string $languageCode
     * @access public
     */
    public $languageCode = null;

    /**
     * @var int $orderStatus
     * @access public
     */
    public $orderStatus = null;

    /**
     * @var string $projectDescription
     * @access public
     */
    public $projectDescription = null;

    /**
     * @var int $projectType
     * @access public
     */
    public $projectType = null;

    /**
     * @var Property[] $propertiesList
     * @access public
     */
    public $propertiesList = null;

    /**
     * @var string $sourceLanguage
     * @access public
     */
    public $sourceLanguage = null;

    /**
     * @var int $statusProjectFileArchiving
     * @access public
     */
    public $statusProjectFileArchiving = null;

    /**
     * @var string $targetLanguage
     * @access public
     */
    public $targetLanguage = null;

    /**
     * @var SelectionEntry_TeamMember $teamMember
     * @access public
     */
    public $teamMember = null;

    /**
     * @var Textmodule[] $textmodulesList
     * @access public
     */
    public $textmodulesList = null;

    /**
     * @var SelectionEntry_TimeFrame $timeFrame
     * @access public
     */
    public $timeFrame = null;

    /**
     * @param int $customerID
     * @param int[] $itemStatus
     * @param string $languageCode
     * @param int $orderStatus
     * @param string $projectDescription
     * @param int $projectType
     * @param Property[] $propertiesList
     * @param string $sourceLanguage
     * @param int $statusProjectFileArchiving
     * @param string $targetLanguage
     * @param SelectionEntry_TeamMember $teamMember
     * @param Textmodule[] $textmodulesList
     * @param SelectionEntry_TimeFrame $timeFrame
     * @access public
     */
    public function __construct($customerID, $itemStatus, $languageCode, $orderStatus, $projectDescription, $projectType, $propertiesList, $sourceLanguage, $statusProjectFileArchiving, $targetLanguage, $teamMember, $textmodulesList, $timeFrame)
    {
      $this->customerID = $customerID;
      $this->itemStatus = $itemStatus;
      $this->languageCode = $languageCode;
      $this->orderStatus = $orderStatus;
      $this->projectDescription = $projectDescription;
      $this->projectType = $projectType;
      $this->propertiesList = $propertiesList;
      $this->sourceLanguage = $sourceLanguage;
      $this->statusProjectFileArchiving = $statusProjectFileArchiving;
      $this->targetLanguage = $targetLanguage;
      $this->teamMember = $teamMember;
      $this->textmodulesList = $textmodulesList;
      $this->timeFrame = $timeFrame;
    }

}
