<?php

namespace Drupal\tmgmt_plunet\Service\DataOrder30;

class Order
{

    /**
     * @var dateTime $creationDate
     * @access public
     */
    public $creationDate = null;

    /**
     * @var string $currency
     * @access public
     */
    public $currency = null;

    /**
     * @var int $customerContactID
     * @access public
     */
    public $customerContactID = null;

    /**
     * @var int $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var dateTime $deliveryDeadline
     * @access public
     */
    public $deliveryDeadline = null;

    /**
     * @var dateTime $orderClosingDate
     * @access public
     */
    public $orderClosingDate = null;

    /**
     * @var string $orderDisplayName
     * @access public
     */
    public $orderDisplayName = null;

    /**
     * @var int $orderID
     * @access public
     */
    public $orderID = null;

    /**
     * @var int $projectManagerID
     * @access public
     */
    public $projectManagerID = null;

    /**
     * @var string $projectName
     * @access public
     */
    public $projectName = null;

    /**
     * @var float $rate
     * @access public
     */
    public $rate = null;

    /**
     * @var int $requestID
     * @access public
     */
    public $requestID = null;

    /**
     * @var string $subject
     * @access public
     */
    public $subject = null;

    /**
     * @param dateTime $creationDate
     * @param string $currency
     * @param int $customerContactID
     * @param int $customerID
     * @param dateTime $deliveryDeadline
     * @param dateTime $orderClosingDate
     * @param string $orderDisplayName
     * @param int $orderID
     * @param int $projectManagerID
     * @param string $projectName
     * @param float $rate
     * @param int $requestID
     * @param string $subject
     * @access public
     */
    public function __construct($creationDate, $currency, $customerContactID, $customerID, $deliveryDeadline, $orderClosingDate, $orderDisplayName, $orderID, $projectManagerID, $projectName, $rate, $requestID, $subject)
    {
      $this->creationDate = $creationDate;
      $this->currency = $currency;
      $this->customerContactID = $customerContactID;
      $this->customerID = $customerID;
      $this->deliveryDeadline = $deliveryDeadline;
      $this->orderClosingDate = $orderClosingDate;
      $this->orderDisplayName = $orderDisplayName;
      $this->orderID = $orderID;
      $this->projectManagerID = $projectManagerID;
      $this->projectName = $projectName;
      $this->rate = $rate;
      $this->requestID = $requestID;
      $this->subject = $subject;
    }

}
