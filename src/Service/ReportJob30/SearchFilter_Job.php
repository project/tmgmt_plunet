<?php

namespace Drupal\tmgmt_plunet\Service\ReportJob30;

class SearchFilter_Job
{

    /**
     * @var int $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var int $item_Status
     * @access public
     */
    public $item_Status = null;

    /**
     * @var dateTime $job_CreationDate_from
     * @access public
     */
    public $job_CreationDate_from = null;

    /**
     * @var dateTime $job_CreationDate_to
     * @access public
     */
    public $job_CreationDate_to = null;

    /**
     * @var string $job_SourceLanguage
     * @access public
     */
    public $job_SourceLanguage = null;

    /**
     * @var int $job_Status
     * @access public
     */
    public $job_Status = null;

    /**
     * @var string $job_TargetLanguage
     * @access public
     */
    public $job_TargetLanguage = null;

    /**
     * @var int $job_resourceID
     * @access public
     */
    public $job_resourceID = null;

    /**
     * @param int $customerID
     * @param int $item_Status
     * @param dateTime $job_CreationDate_from
     * @param dateTime $job_CreationDate_to
     * @param string $job_SourceLanguage
     * @param int $job_Status
     * @param string $job_TargetLanguage
     * @param int $job_resourceID
     * @access public
     */
    public function __construct($customerID, $item_Status, $job_CreationDate_from, $job_CreationDate_to, $job_SourceLanguage, $job_Status, $job_TargetLanguage, $job_resourceID)
    {
      $this->customerID = $customerID;
      $this->item_Status = $item_Status;
      $this->job_CreationDate_from = $job_CreationDate_from;
      $this->job_CreationDate_to = $job_CreationDate_to;
      $this->job_SourceLanguage = $job_SourceLanguage;
      $this->job_Status = $job_Status;
      $this->job_TargetLanguage = $job_TargetLanguage;
      $this->job_resourceID = $job_resourceID;
    }

}
