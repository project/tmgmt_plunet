<?php

namespace Drupal\tmgmt_plunet\Service\ReportJob30;





class ReportJob30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'SearchFilter_Job' => 'Drupal\tmgmt_plunet\Service\ReportJob30\SearchFilter_Job',
      'IntegerArrayResult' => 'Drupal\tmgmt_plunet\Service\ReportJob30\IntegerArrayResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\ReportJob30\Result');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/ReportJob30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param SearchFilter_Job $SearchFilter_Job
     * @access public
     * @return IntegerArrayResult
     */
    public function search($UUID, SearchFilter_Job $SearchFilter_Job)
    {
      return $this->__soapCall('search', array($UUID, $SearchFilter_Job));
    }

}
