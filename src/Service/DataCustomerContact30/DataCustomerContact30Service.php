<?php

namespace Drupal\tmgmt_plunet\Service\DataCustomerContact30;










class DataCustomerContact30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataCustomerContact30\StringResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\DataCustomerContact30\Result',
      'CustomerContactListResult' => 'Drupal\tmgmt_plunet\Service\DataCustomerContact30\CustomerContactListResult',
      'CustomerContact' => 'Drupal\tmgmt_plunet\Service\DataCustomerContact30\CustomerContact',
      'CustomerContactIN' => 'Drupal\tmgmt_plunet\Service\DataCustomerContact30\CustomerContactIN',
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\DataCustomerContact30\IntegerResult',
      'CustomerContactResult' => 'Drupal\tmgmt_plunet\Service\DataCustomerContact30\CustomerContactResult',
      'IntegerArrayResult' => 'Drupal\tmgmt_plunet\Service\DataCustomerContact30\IntegerArrayResult');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataCustomerContact30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param CustomerContactIN $CustomerContactIN
     * @param boolean $enableNullOrEmptyValues
     * @access public
     * @return Result
     */
    public function update($UUID, CustomerContactIN $CustomerContactIN, $enableNullOrEmptyValues)
    {
      return $this->__soapCall('update', array($UUID, $CustomerContactIN, $enableNullOrEmptyValues));
    }

    /**
     * @param string $UUID
     * @param int $partnerID
     * @access public
     * @return IntegerResult
     */
    public function insert($UUID, $partnerID)
    {
      return $this->__soapCall('insert', array($UUID, $partnerID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getFax($UUID, $ContactID)
    {
      return $this->__soapCall('getFax', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $Fax
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setFax($UUID, $Fax, $ContactID)
    {
      return $this->__soapCall('setFax', array($UUID, $Fax, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $PhoneNumber
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setMobilePhone($UUID, $PhoneNumber, $ContactID)
    {
      return $this->__soapCall('setMobilePhone', array($UUID, $PhoneNumber, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getEmail($UUID, $ContactID)
    {
      return $this->__soapCall('getEmail', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $PhoneNumber
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setPhone($UUID, $PhoneNumber, $ContactID)
    {
      return $this->__soapCall('setPhone', array($UUID, $PhoneNumber, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getPhone($UUID, $ContactID)
    {
      return $this->__soapCall('getPhone', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $EMail
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setEmail($UUID, $EMail, $ContactID)
    {
      return $this->__soapCall('setEmail', array($UUID, $EMail, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getMobilePhone($UUID, $ContactID)
    {
      return $this->__soapCall('getMobilePhone', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getExternalID($UUID, $ContactID)
    {
      return $this->__soapCall('getExternalID', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $ExternalID
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setExternalID($UUID, $ExternalID, $ContactID)
    {
      return $this->__soapCall('setExternalID', array($UUID, $ExternalID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getName1($UUID, $ContactID)
    {
      return $this->__soapCall('getName1', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $Name
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setName2($UUID, $Name, $ContactID)
    {
      return $this->__soapCall('setName2', array($UUID, $Name, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return IntegerResult
     */
    public function getAddressID($UUID, $ContactID)
    {
      return $this->__soapCall('getAddressID', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $CostCenter
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setCostCenter($UUID, $CostCenter, $ContactID)
    {
      return $this->__soapCall('setCostCenter', array($UUID, $CostCenter, $ContactID));
    }

    /**
     * @param string $UUID
     * @param CustomerContactIN $CustomerContactIN
     * @access public
     * @return IntegerResult
     */
    public function insert2($UUID, CustomerContactIN $CustomerContactIN)
    {
      return $this->__soapCall('insert2', array($UUID, $CustomerContactIN));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return IntegerResult
     */
    public function getUserId($UUID, $ContactID)
    {
      return $this->__soapCall('getUserId', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $ExternalID
     * @access public
     * @return IntegerArrayResult
     */
    public function seekByExternalID($UUID, $ExternalID)
    {
      return $this->__soapCall('seekByExternalID', array($UUID, $ExternalID));
    }

    /**
     * @param string $UUID
     * @param string $Name
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setName1($UUID, $Name, $ContactID)
    {
      return $this->__soapCall('setName1', array($UUID, $Name, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getName2($UUID, $ContactID)
    {
      return $this->__soapCall('getName2', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getCostCenter($UUID, $ContactID)
    {
      return $this->__soapCall('getCostCenter', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setAddressID($UUID, $AddressID, $ContactID)
    {
      return $this->__soapCall('setAddressID', array($UUID, $AddressID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $LoginName
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setSupervisor1($UUID, $LoginName, $ContactID)
    {
      return $this->__soapCall('setSupervisor1', array($UUID, $LoginName, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getSupervisor2($UUID, $ContactID)
    {
      return $this->__soapCall('getSupervisor2', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $CustomerID
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setCustomerID($UUID, $CustomerID, $ContactID)
    {
      return $this->__soapCall('setCustomerID', array($UUID, $CustomerID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $LoginName
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setSupervisor2($UUID, $LoginName, $ContactID)
    {
      return $this->__soapCall('setSupervisor2', array($UUID, $LoginName, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getSupervisor1($UUID, $ContactID)
    {
      return $this->__soapCall('getSupervisor1', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $CustomerID
     * @access public
     * @return IntegerArrayResult
     */
    public function getAllContacts($UUID, $CustomerID)
    {
      return $this->__soapCall('getAllContacts', array($UUID, $CustomerID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return CustomerContactResult
     */
    public function getContactObject($UUID, $ContactID)
    {
      return $this->__soapCall('getContactObject', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return IntegerResult
     */
    public function getCustomerID($UUID, $ContactID)
    {
      return $this->__soapCall('getCustomerID', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return IntegerResult
     */
    public function getStatus($UUID, $ContactID)
    {
      return $this->__soapCall('getStatus', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $Status
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setStatus($UUID, $Status, $ContactID)
    {
      return $this->__soapCall('setStatus', array($UUID, $Status, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $CustomerID
     * @access public
     * @return CustomerContactListResult
     */
    public function getAllContactObjects($UUID, $CustomerID)
    {
      return $this->__soapCall('getAllContactObjects', array($UUID, $CustomerID));
    }

}
