<?php

namespace Drupal\tmgmt_plunet\Service\DataCustomerContact30;

class CustomerContact
{

    /**
     * @var int $addressID
     * @access public
     */
    public $addressID = null;

    /**
     * @var string $costCenter
     * @access public
     */
    public $costCenter = null;

    /**
     * @var int $customerContactID
     * @access public
     */
    public $customerContactID = null;

    /**
     * @var int $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var string $email
     * @access public
     */
    public $email = null;

    /**
     * @var string $externalID
     * @access public
     */
    public $externalID = null;

    /**
     * @var string $fax
     * @access public
     */
    public $fax = null;

    /**
     * @var string $mobilePhone
     * @access public
     */
    public $mobilePhone = null;

    /**
     * @var string $name1
     * @access public
     */
    public $name1 = null;

    /**
     * @var string $name2
     * @access public
     */
    public $name2 = null;

    /**
     * @var string $phone
     * @access public
     */
    public $phone = null;

    /**
     * @var int $status
     * @access public
     */
    public $status = null;

    /**
     * @var string $supervisor1
     * @access public
     */
    public $supervisor1 = null;

    /**
     * @var string $supervisor2
     * @access public
     */
    public $supervisor2 = null;

    /**
     * @var int $userId
     * @access public
     */
    public $userId = null;

    /**
     * @param int $addressID
     * @param string $costCenter
     * @param int $customerContactID
     * @param int $customerID
     * @param string $email
     * @param string $externalID
     * @param string $fax
     * @param string $mobilePhone
     * @param string $name1
     * @param string $name2
     * @param string $phone
     * @param int $status
     * @param string $supervisor1
     * @param string $supervisor2
     * @param int $userId
     * @access public
     */
    public function __construct($addressID, $costCenter, $customerContactID, $customerID, $email, $externalID, $fax, $mobilePhone, $name1, $name2, $phone, $status, $supervisor1, $supervisor2, $userId)
    {
      $this->addressID = $addressID;
      $this->costCenter = $costCenter;
      $this->customerContactID = $customerContactID;
      $this->customerID = $customerID;
      $this->email = $email;
      $this->externalID = $externalID;
      $this->fax = $fax;
      $this->mobilePhone = $mobilePhone;
      $this->name1 = $name1;
      $this->name2 = $name2;
      $this->phone = $phone;
      $this->status = $status;
      $this->supervisor1 = $supervisor1;
      $this->supervisor2 = $supervisor2;
      $this->userId = $userId;
    }

}
