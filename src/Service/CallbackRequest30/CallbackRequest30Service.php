<?php

namespace Drupal\tmgmt_plunet\Service\CallbackRequest30;



class CallbackRequest30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Request' => 'Drupal\tmgmt_plunet\Service\CallbackRequest30\Request');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/CallbackRequest30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $Authenticationcode
     * @param int $RequestID
     * @param int $EventType
     * @access public
     * @return void
     */
    public function ReceiveNotifyCallback($Authenticationcode, $RequestID, $EventType)
    {
      return $this->__soapCall('ReceiveNotifyCallback', array($Authenticationcode, $RequestID, $EventType));
    }

    /**
     * @param string $Authenticationcode
     * @param Request $Request
     * @param int $EventType
     * @access public
     * @return void
     */
    public function receiveObserverCallback($Authenticationcode, $Request, $EventType)
    {
      return \Drupal::service('tmgmt_plunet.callbacks')->receiveOrderRequestCallback($Authenticationcode, $Request->requestID, $EventType);
    }

}
