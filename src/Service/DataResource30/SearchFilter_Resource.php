<?php

namespace Drupal\tmgmt_plunet\Service\DataResource30;

class SearchFilter_Resource
{

    /**
     * @var int $contact_resourceID
     * @access public
     */
    public $contact_resourceID = null;

    /**
     * @var string $email
     * @access public
     */
    public $email = null;

    /**
     * @var string $languageCode
     * @access public
     */
    public $languageCode = null;

    /**
     * @var string $name1
     * @access public
     */
    public $name1 = null;

    /**
     * @var string $name2
     * @access public
     */
    public $name2 = null;

    /**
     * @var Property[] $propertiesList
     * @access public
     */
    public $propertiesList = null;

    /**
     * @var int $resourceType
     * @access public
     */
    public $resourceType = null;

    /**
     * @var int $resourceStatus
     * @access public
     */
    public $resourceStatus = null;

    /**
     * @var string $sourceLanguageCode
     * @access public
     */
    public $sourceLanguageCode = null;

    /**
     * @var string $targetLanguageCode
     * @access public
     */
    public $targetLanguageCode = null;

    /**
     * @var Textmodule[] $textmodulesList
     * @access public
     */
    public $textmodulesList = null;

    /**
     * @var int $workingStatus
     * @access public
     */
    public $workingStatus = null;

    /**
     * @param int $contact_resourceID
     * @param string $email
     * @param string $languageCode
     * @param string $name1
     * @param string $name2
     * @param Property[] $propertiesList
     * @param int $resourceType
     * @param int $resourceStatus
     * @param string $sourceLanguageCode
     * @param string $targetLanguageCode
     * @param Textmodule[] $textmodulesList
     * @param int $workingStatus
     * @access public
     */
    public function __construct($contact_resourceID, $email, $languageCode, $name1, $name2, $propertiesList, $resourceType, $resourceStatus, $sourceLanguageCode, $targetLanguageCode, $textmodulesList, $workingStatus)
    {
      $this->contact_resourceID = $contact_resourceID;
      $this->email = $email;
      $this->languageCode = $languageCode;
      $this->name1 = $name1;
      $this->name2 = $name2;
      $this->propertiesList = $propertiesList;
      $this->resourceType = $resourceType;
      $this->resourceStatus = $resourceStatus;
      $this->sourceLanguageCode = $sourceLanguageCode;
      $this->targetLanguageCode = $targetLanguageCode;
      $this->textmodulesList = $textmodulesList;
      $this->workingStatus = $workingStatus;
    }

}
