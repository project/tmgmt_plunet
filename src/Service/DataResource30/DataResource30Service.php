<?php

namespace Drupal\tmgmt_plunet\Service\DataResource30;




















class DataResource30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Result' => 'Drupal\tmgmt_plunet\Service\DataResource30\Result',
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataResource30\StringResult',
      'IntegerList' => 'Drupal\tmgmt_plunet\Service\DataResource30\IntegerList',
      'ResourceListResult' => 'Drupal\tmgmt_plunet\Service\DataResource30\ResourceListResult',
      'Resource' => 'Drupal\tmgmt_plunet\Service\DataResource30\Resource',
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\DataResource30\IntegerResult',
      'ResourceResult' => 'Drupal\tmgmt_plunet\Service\DataResource30\ResourceResult',
      'IntegerArrayResult' => 'Drupal\tmgmt_plunet\Service\DataResource30\IntegerArrayResult',
      'SearchFilter_Resource' => 'Drupal\tmgmt_plunet\Service\DataResource30\SearchFilter_Resource',
      'Property' => 'Drupal\tmgmt_plunet\Service\DataResource30\Property',
      'Textmodule' => 'Drupal\tmgmt_plunet\Service\DataResource30\Textmodule',
      'AccountResult' => 'Drupal\tmgmt_plunet\Service\DataResource30\AccountResult',
      'Account' => 'Drupal\tmgmt_plunet\Service\DataResource30\Account',
      'PricelistListResult' => 'Drupal\tmgmt_plunet\Service\DataResource30\PricelistListResult',
      'Pricelist' => 'Drupal\tmgmt_plunet\Service\DataResource30\Pricelist',
      'ResourceIN' => 'Drupal\tmgmt_plunet\Service\DataResource30\ResourceIN',
      'PaymentInfoResult' => 'Drupal\tmgmt_plunet\Service\DataResource30\PaymentInfoResult',
      'PaymentInfo' => 'Drupal\tmgmt_plunet\Service\DataResource30\PaymentInfo');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataResource30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param ResourceIN $ResourceIN
     * @param boolean $enableNullOrEmptyValues
     * @access public
     * @return Result
     */
    public function update($UUID, ResourceIN $ResourceIN, $enableNullOrEmptyValues)
    {
      return $this->__soapCall('update', array($UUID, $ResourceIN, $enableNullOrEmptyValues));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function delete($UUID, $resourceID)
    {
      return $this->__soapCall('delete', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $WorkingStatus
     * @access public
     * @return IntegerResult
     */
    public function insert($UUID, $WorkingStatus)
    {
      return $this->__soapCall('insert', array($UUID, $WorkingStatus));
    }

    /**
     * @param string $UUID
     * @param SearchFilter_Resource $SearchFilterResource
     * @access public
     * @return IntegerArrayResult
     */
    public function search($UUID, SearchFilter_Resource $SearchFilterResource)
    {
      return $this->__soapCall('search', array($UUID, $SearchFilterResource));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getCurrency($UUID, $resourceID)
    {
      return $this->__soapCall('getCurrency', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @param string $currencyIsoCode
     * @access public
     * @return Result
     */
    public function setCurrency($UUID, $resourceID, $currencyIsoCode)
    {
      return $this->__soapCall('setCurrency', array($UUID, $resourceID, $currencyIsoCode));
    }

    /**
     * @param string $UUID
     * @param string $SkypeID
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setSkypeID($UUID, $SkypeID, $resourceID)
    {
      return $this->__soapCall('setSkypeID', array($UUID, $SkypeID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getFax($UUID, $resourceID)
    {
      return $this->__soapCall('getFax', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getSkypeID($UUID, $resourceID)
    {
      return $this->__soapCall('getSkypeID', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $Fax
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setFax($UUID, $Fax, $resourceID)
    {
      return $this->__soapCall('setFax', array($UUID, $Fax, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $PhoneNumber
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setMobilePhone($UUID, $PhoneNumber, $resourceID)
    {
      return $this->__soapCall('setMobilePhone', array($UUID, $PhoneNumber, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getEmail($UUID, $resourceID)
    {
      return $this->__soapCall('getEmail', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getWebsite($UUID, $resourceID)
    {
      return $this->__soapCall('getWebsite', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $PhoneNumber
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setPhone($UUID, $PhoneNumber, $resourceID)
    {
      return $this->__soapCall('setPhone', array($UUID, $PhoneNumber, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getPhone($UUID, $resourceID)
    {
      return $this->__soapCall('getPhone', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $Website
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setWebsite($UUID, $Website, $resourceID)
    {
      return $this->__soapCall('setWebsite', array($UUID, $Website, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $EMail
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setEmail($UUID, $EMail, $resourceID)
    {
      return $this->__soapCall('setEmail', array($UUID, $EMail, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getMobilePhone($UUID, $resourceID)
    {
      return $this->__soapCall('getMobilePhone', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getExternalID($UUID, $resourceID)
    {
      return $this->__soapCall('getExternalID', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $ExternalID
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setExternalID($UUID, $ExternalID, $resourceID)
    {
      return $this->__soapCall('setExternalID', array($UUID, $ExternalID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return IntegerResult
     */
    public function getFormOfAddress($UUID, $resourceID)
    {
      return $this->__soapCall('getFormOfAddress', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $AcademicTitle
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setAcademicTitle($UUID, $AcademicTitle, $resourceID)
    {
      return $this->__soapCall('setAcademicTitle', array($UUID, $AcademicTitle, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getName1($UUID, $resourceID)
    {
      return $this->__soapCall('getName1', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $Name
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setName2($UUID, $Name, $resourceID)
    {
      return $this->__soapCall('setName2', array($UUID, $Name, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $CostCenter
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setCostCenter($UUID, $CostCenter, $resourceID)
    {
      return $this->__soapCall('setCostCenter', array($UUID, $CostCenter, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getOpening($UUID, $resourceID)
    {
      return $this->__soapCall('getOpening', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return IntegerResult
     */
    public function getUserId($UUID, $resourceID)
    {
      return $this->__soapCall('getUserId', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $ExternalID
     * @access public
     * @return IntegerResult
     */
    public function seekByExternalID($UUID, $ExternalID)
    {
      return $this->__soapCall('seekByExternalID', array($UUID, $ExternalID));
    }

    /**
     * @param string $UUID
     * @param string $Name
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setName1($UUID, $Name, $resourceID)
    {
      return $this->__soapCall('setName1', array($UUID, $Name, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $FormOfAddress
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setFormOfAddress($UUID, $FormOfAddress, $resourceID)
    {
      return $this->__soapCall('setFormOfAddress', array($UUID, $FormOfAddress, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getAcademicTitle($UUID, $resourceID)
    {
      return $this->__soapCall('getAcademicTitle', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getName2($UUID, $resourceID)
    {
      return $this->__soapCall('getName2', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getCostCenter($UUID, $resourceID)
    {
      return $this->__soapCall('getCostCenter', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $Opening
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setOpening($UUID, $Opening, $resourceID)
    {
      return $this->__soapCall('setOpening', array($UUID, $Opening, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $sourcelanguage
     * @param string $targetlanguage
     * @param int $resourceID
     * @access public
     * @return PricelistListResult
     */
    public function getPricelists2($UUID, $sourcelanguage, $targetlanguage, $resourceID)
    {
      return $this->__soapCall('getPricelists2', array($UUID, $sourcelanguage, $targetlanguage, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $WorkingStatus
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setWorkingStatus($UUID, $WorkingStatus, $resourceID)
    {
      return $this->__soapCall('setWorkingStatus', array($UUID, $WorkingStatus, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return PricelistListResult
     */
    public function getPricelists($UUID, $resourceID)
    {
      return $this->__soapCall('getPricelists', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $LoginName
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setSupervisor1($UUID, $LoginName, $resourceID)
    {
      return $this->__soapCall('setSupervisor1', array($UUID, $LoginName, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getSupervisor2($UUID, $resourceID)
    {
      return $this->__soapCall('getSupervisor2', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return IntegerResult
     */
    public function getResourceType($UUID, $resourceID)
    {
      return $this->__soapCall('getResourceType', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $ResourceType
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setResourceType($UUID, $ResourceType, $resourceID)
    {
      return $this->__soapCall('setResourceType', array($UUID, $ResourceType, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $LoginName
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setSupervisor2($UUID, $LoginName, $resourceID)
    {
      return $this->__soapCall('setSupervisor2', array($UUID, $LoginName, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getSupervisor1($UUID, $resourceID)
    {
      return $this->__soapCall('getSupervisor1', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return IntegerResult
     */
    public function getWorkingStatus($UUID, $resourceID)
    {
      return $this->__soapCall('getWorkingStatus', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $AccountID
     * @access public
     * @return AccountResult
     */
    public function getAccount($UUID, $AccountID)
    {
      return $this->__soapCall('getAccount', array($UUID, $AccountID));
    }

    /**
     * @param string $UUID
     * @param ResourceIN $ResourceIN
     * @access public
     * @return IntegerResult
     */
    public function insertObject($UUID, ResourceIN $ResourceIN)
    {
      return $this->__soapCall('insertObject', array($UUID, $ResourceIN));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return StringResult
     */
    public function getFullName($UUID, $resourceID)
    {
      return $this->__soapCall('getFullName', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return IntegerResult
     */
    public function getStatus($UUID, $resourceID)
    {
      return $this->__soapCall('getStatus', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $Status
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setStatus($UUID, $Status, $resourceID)
    {
      return $this->__soapCall('setStatus', array($UUID, $Status, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $WorkingStatus
     * @param int $Status
     * @access public
     * @return ResourceListResult
     */
    public function getAllResourceObjects($UUID, $WorkingStatus, $Status)
    {
      return $this->__soapCall('getAllResourceObjects', array($UUID, $WorkingStatus, $Status));
    }

    /**
     * @param string $UUID
     * @param IntegerList $WorkingStatusList
     * @param IntegerList $StatusList
     * @access public
     * @return ResourceListResult
     */
    public function getAllResourceObjects2($UUID, IntegerList $WorkingStatusList, IntegerList $StatusList)
    {
      return $this->__soapCall('getAllResourceObjects2', array($UUID, $WorkingStatusList, $StatusList));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return ResourceResult
     */
    public function getResourceObject($UUID, $resourceID)
    {
      return $this->__soapCall('getResourceObject', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param string $ServerAuthenticationString
     * @param string $ServerAddress
     * @param int $EventType
     * @access public
     * @return Result
     */
    public function registerCallback_Notify($UUID, $ServerAuthenticationString, $ServerAddress, $EventType)
    {
      return $this->__soapCall('registerCallback_Notify', array($UUID, $ServerAuthenticationString, $ServerAddress, $EventType));
    }

    /**
     * @param string $UUID
     * @access public
     * @return IntegerArrayResult
     */
    public function getAvailablePaymentMethodList($UUID)
    {
      return $this->__soapCall('getAvailablePaymentMethodList', array($UUID));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @param PaymentInfo $paymentInfo
     * @access public
     * @return Result
     */
    public function setPaymentInformation($UUID, $resourceID, PaymentInfo $paymentInfo)
    {
      return $this->__soapCall('setPaymentInformation', array($UUID, $resourceID, $paymentInfo));
    }

    /**
     * @param string $UUID
     * @param string $ServerAuthenticationString
     * @param string $ServerAddress
     * @param int $ResourceID
     * @access public
     * @return Result
     */
    public function registerCallback_Observer($UUID, $ServerAuthenticationString, $ServerAddress, $ResourceID)
    {
      return $this->__soapCall('registerCallback_Observer', array($UUID, $ServerAuthenticationString, $ServerAddress, $ResourceID));
    }

    /**
     * @param string $UUID
     * @access public
     * @return IntegerArrayResult
     */
    public function getAvailableAccountIDList($UUID)
    {
      return $this->__soapCall('getAvailableAccountIDList', array($UUID));
    }

    /**
     * @param string $UUID
     * @param int $paymentMethodID
     * @param string $systemLanguageCode
     * @access public
     * @return StringResult
     */
    public function getPaymentMethodDescription($UUID, $paymentMethodID, $systemLanguageCode)
    {
      return $this->__soapCall('getPaymentMethodDescription', array($UUID, $paymentMethodID, $systemLanguageCode));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return PaymentInfoResult
     */
    public function getPaymentInformation($UUID, $resourceID)
    {
      return $this->__soapCall('getPaymentInformation', array($UUID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $EventType
     * @access public
     * @return Result
     */
    public function deregisterCallback_Notify($UUID, $EventType)
    {
      return $this->__soapCall('deregisterCallback_Notify', array($UUID, $EventType));
    }

    /**
     * @param string $UUID
     * @param int $ResourceID
     * @access public
     * @return Result
     */
    public function deregisterCallback_Observer($UUID, $ResourceID)
    {
      return $this->__soapCall('deregisterCallback_Observer', array($UUID, $ResourceID));
    }

}
