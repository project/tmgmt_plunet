<?php

namespace Drupal\tmgmt_plunet\Service\DataResource30;

class PaymentInfo
{

    /**
     * @var string $accountHolder
     * @access public
     */
    public $accountHolder = null;

    /**
     * @var int $accountID
     * @access public
     */
    public $accountID = null;

    /**
     * @var string $BIC
     * @access public
     */
    public $BIC = null;

    /**
     * @var string $contractNumber
     * @access public
     */
    public $contractNumber = null;

    /**
     * @var string $debitAccount
     * @access public
     */
    public $debitAccount = null;

    /**
     * @var string $IBAN
     * @access public
     */
    public $IBAN = null;

    /**
     * @var int $paymentMethodID
     * @access public
     */
    public $paymentMethodID = null;

    /**
     * @var int $preselectedTaxID
     * @access public
     */
    public $preselectedTaxID = null;

    /**
     * @var string $salesTaxID
     * @access public
     */
    public $salesTaxID = null;

    /**
     * @param string $accountHolder
     * @param int $accountID
     * @param string $BIC
     * @param string $contractNumber
     * @param string $debitAccount
     * @param string $IBAN
     * @param int $paymentMethodID
     * @param int $preselectedTaxID
     * @param string $salesTaxID
     * @access public
     */
    public function __construct($accountHolder, $accountID, $BIC, $contractNumber, $debitAccount, $IBAN, $paymentMethodID, $preselectedTaxID, $salesTaxID)
    {
      $this->accountHolder = $accountHolder;
      $this->accountID = $accountID;
      $this->BIC = $BIC;
      $this->contractNumber = $contractNumber;
      $this->debitAccount = $debitAccount;
      $this->IBAN = $IBAN;
      $this->paymentMethodID = $paymentMethodID;
      $this->preselectedTaxID = $preselectedTaxID;
      $this->salesTaxID = $salesTaxID;
    }

}
