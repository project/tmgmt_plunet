<?php

namespace Drupal\tmgmt_plunet\Service\DataResource30;



class ResourceResult extends Result
{

    /**
     * @var Resource $data
     * @access public
     */
    public $data = null;

    /**
     * @param int $statusCode
     * @param string $statusCodeAlphanumeric
     * @param string $statusMessage
     * @param Resource $data
     * @access public
     */
    public function __construct($statusCode, $statusCodeAlphanumeric, $statusMessage, $data)
    {
      parent::__construct($statusCode, $statusCodeAlphanumeric, $statusMessage);
      $this->data = $data;
    }

}
