<?php

namespace Drupal\tmgmt_plunet\Service\DataResource30;

class Account
{

    /**
     * @var string $accountDescription
     * @access public
     */
    public $accountDescription = null;

    /**
     * @var int $accountID
     * @access public
     */
    public $accountID = null;

    /**
     * @var string $accountNumber
     * @access public
     */
    public $accountNumber = null;

    /**
     * @var int $accountType
     * @access public
     */
    public $accountType = null;

    /**
     * @var boolean $active
     * @access public
     */
    public $active = null;

    /**
     * @param string $accountDescription
     * @param int $accountID
     * @param string $accountNumber
     * @param int $accountType
     * @param boolean $active
     * @access public
     */
    public function __construct($accountDescription, $accountID, $accountNumber, $accountType, $active)
    {
      $this->accountDescription = $accountDescription;
      $this->accountID = $accountID;
      $this->accountNumber = $accountNumber;
      $this->accountType = $accountType;
      $this->active = $active;
    }

}
