<?php

namespace Drupal\tmgmt_plunet\Service\DataDocument30;



class FileResult extends Result
{

    /**
     * @var base64Binary $fileContent
     * @access public
     */
    public $fileContent = null;

    /**
     * @var int $fileSize
     * @access public
     */
    public $fileSize = null;

    /**
     * @var string $filename
     * @access public
     */
    public $filename = null;

    /**
     * @param int $statusCode
     * @param string $statusCodeAlphanumeric
     * @param string $statusMessage
     * @param base64Binary $fileContent
     * @param int $fileSize
     * @param string $filename
     * @access public
     */
    public function __construct($statusCode, $statusCodeAlphanumeric, $statusMessage, $fileContent, $fileSize, $filename)
    {
      parent::__construct($statusCode, $statusCodeAlphanumeric, $statusMessage);
      $this->fileContent = $fileContent;
      $this->fileSize = $fileSize;
      $this->filename = $filename;
    }

}
