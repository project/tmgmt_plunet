<?php

namespace Drupal\tmgmt_plunet\Service\DataDocument30;





class DataDocument30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'StringArrayResult' => 'Drupal\tmgmt_plunet\Service\DataDocument30\StringArrayResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\DataDocument30\Result',
      'FileResult' => 'Drupal\tmgmt_plunet\Service\DataDocument30\FileResult');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataDocument30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param int $MainID
     * @param int $FolderType
     * @access public
     * @return StringArrayResult
     */
    public function getFileList($UUID, $MainID, $FolderType)
    {
      return $this->__soapCall('getFileList', array($UUID, $MainID, $FolderType));
    }

    /**
     * @param string $UUID
     * @param int $MainID
     * @param int $FolderType
     * @param base64Binary $FileByteStream
     * @param string $FilePathName
     * @param int $FileSize
     * @access public
     * @return Result
     */
    public function upload_Document($UUID, $MainID, $FolderType, $FileByteStream, $FilePathName, $FileSize)
    {
      return $this->__soapCall('upload_Document', array($UUID, $MainID, $FolderType, $FileByteStream, $FilePathName, $FileSize));
    }

    /**
     * @param string $UUID
     * @param int $MainID
     * @param int $FolderType
     * @param string $FilePathName
     * @access public
     * @return FileResult
     */
    public function download_Document($UUID, $MainID, $FolderType, $FilePathName)
    {
      return $this->__soapCall('download_Document', array($UUID, $MainID, $FolderType, $FilePathName));
    }

}
