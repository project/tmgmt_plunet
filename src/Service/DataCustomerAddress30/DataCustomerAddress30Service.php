<?php

namespace Drupal\tmgmt_plunet\Service\DataCustomerAddress30;







class DataCustomerAddress30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataCustomerAddress30\StringResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\DataCustomerAddress30\Result',
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\DataCustomerAddress30\IntegerResult',
      'AddressIN' => 'Drupal\tmgmt_plunet\Service\DataCustomerAddress30\AddressIN',
      'IntegerArrayResult' => 'Drupal\tmgmt_plunet\Service\DataCustomerAddress30\IntegerArrayResult');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataCustomerAddress30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param AddressIN $AddressIN
     * @param boolean $enableNullOrEmptyValues
     * @access public
     * @return Result
     */
    public function update($UUID, AddressIN $AddressIN, $enableNullOrEmptyValues)
    {
      return $this->__soapCall('update', array($UUID, $AddressIN, $enableNullOrEmptyValues));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @access public
     * @return Result
     */
    public function delete($UUID, $AddressID)
    {
      return $this->__soapCall('delete', array($UUID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @access public
     * @return StringResult
     */
    public function getState($UUID, $AddressID)
    {
      return $this->__soapCall('getState', array($UUID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $partnerID
     * @access public
     * @return IntegerResult
     */
    public function insert($UUID, $partnerID)
    {
      return $this->__soapCall('insert', array($UUID, $partnerID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @access public
     * @return StringResult
     */
    public function getCountry($UUID, $AddressID)
    {
      return $this->__soapCall('getCountry', array($UUID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param string $State
     * @param int $AddressID
     * @access public
     * @return Result
     */
    public function setState($UUID, $State, $AddressID)
    {
      return $this->__soapCall('setState', array($UUID, $State, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @access public
     * @return StringResult
     */
    public function getStreet2($UUID, $AddressID)
    {
      return $this->__soapCall('getStreet2', array($UUID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param string $ExternalID
     * @param int $AddressID
     * @access public
     * @return Result
     */
    public function setOffice($UUID, $ExternalID, $AddressID)
    {
      return $this->__soapCall('setOffice', array($UUID, $ExternalID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param string $Street
     * @param int $AddressID
     * @access public
     * @return Result
     */
    public function setStreet($UUID, $Street, $AddressID)
    {
      return $this->__soapCall('setStreet', array($UUID, $Street, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @access public
     * @return StringResult
     */
    public function getName1($UUID, $AddressID)
    {
      return $this->__soapCall('getName1', array($UUID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param string $Name
     * @param int $AddressID
     * @access public
     * @return Result
     */
    public function setName2($UUID, $Name, $AddressID)
    {
      return $this->__soapCall('setName2', array($UUID, $Name, $AddressID));
    }

    /**
     * @param string $UUID
     * @access public
     * @return IntegerResult
     */
    public function getAddressID($UUID)
    {
      return $this->__soapCall('getAddressID', array($UUID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @access public
     * @return StringResult
     */
    public function getStreet($UUID, $AddressID)
    {
      return $this->__soapCall('getStreet', array($UUID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param string $CostCenter
     * @param int $AddressID
     * @access public
     * @return Result
     */
    public function setCostCenter($UUID, $CostCenter, $AddressID)
    {
      return $this->__soapCall('setCostCenter', array($UUID, $CostCenter, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @access public
     * @return StringResult
     */
    public function getCity($UUID, $AddressID)
    {
      return $this->__soapCall('getCity', array($UUID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $CustomerID
     * @param AddressIN $AddressIN
     * @access public
     * @return IntegerResult
     */
    public function insert2($UUID, $CustomerID, AddressIN $AddressIN)
    {
      return $this->__soapCall('insert2', array($UUID, $CustomerID, $AddressIN));
    }

    /**
     * @param string $UUID
     * @param string $Name
     * @param int $AddressID
     * @access public
     * @return Result
     */
    public function setName1($UUID, $Name, $AddressID)
    {
      return $this->__soapCall('setName1', array($UUID, $Name, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @access public
     * @return StringResult
     */
    public function getZip($UUID, $AddressID)
    {
      return $this->__soapCall('getZip', array($UUID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param string $Country
     * @param int $AddressID
     * @access public
     * @return Result
     */
    public function setCountry($UUID, $Country, $AddressID)
    {
      return $this->__soapCall('setCountry', array($UUID, $Country, $AddressID));
    }

    /**
     * @param string $UUID
     * @param string $Street2
     * @param int $AddressID
     * @access public
     * @return Result
     */
    public function setStreet2($UUID, $Street2, $AddressID)
    {
      return $this->__soapCall('setStreet2', array($UUID, $Street2, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $CustomerID
     * @access public
     * @return IntegerArrayResult
     */
    public function getAllAddresses($UUID, $CustomerID)
    {
      return $this->__soapCall('getAllAddresses', array($UUID, $CustomerID));
    }

    /**
     * @param string $UUID
     * @param string $Zip
     * @param int $AddressID
     * @access public
     * @return Result
     */
    public function setZip($UUID, $Zip, $AddressID)
    {
      return $this->__soapCall('setZip', array($UUID, $Zip, $AddressID));
    }

    /**
     * @param string $UUID
     * @param string $City
     * @param int $AddressID
     * @access public
     * @return Result
     */
    public function setCity($UUID, $City, $AddressID)
    {
      return $this->__soapCall('setCity', array($UUID, $City, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @access public
     * @return StringResult
     */
    public function getName2($UUID, $AddressID)
    {
      return $this->__soapCall('getName2', array($UUID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @access public
     * @return StringResult
     */
    public function getOffice($UUID, $AddressID)
    {
      return $this->__soapCall('getOffice', array($UUID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @access public
     * @return StringResult
     */
    public function getCostCenter($UUID, $AddressID)
    {
      return $this->__soapCall('getCostCenter', array($UUID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @access public
     * @return IntegerResult
     */
    public function getAddressType($UUID, $AddressID)
    {
      return $this->__soapCall('getAddressType', array($UUID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @param string $language
     * @access public
     * @return StringResult
     */
    public function getTaxationType($UUID, $AddressID, $language)
    {
      return $this->__soapCall('getTaxationType', array($UUID, $AddressID, $language));
    }

    /**
     * @param string $UUID
     * @param int $AddressType
     * @param int $addressID
     * @access public
     * @return Result
     */
    public function setAddressType($UUID, $AddressType, $addressID)
    {
      return $this->__soapCall('setAddressType', array($UUID, $AddressType, $addressID));
    }

    /**
     * @param string $UUID
     * @param string $Description
     * @param int $AddressID
     * @access public
     * @return Result
     */
    public function setDescription($UUID, $Description, $AddressID)
    {
      return $this->__soapCall('setDescription', array($UUID, $Description, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @access public
     * @return StringResult
     */
    public function getDescription($UUID, $AddressID)
    {
      return $this->__soapCall('getDescription', array($UUID, $AddressID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @param string $language
     * @access public
     * @return StringResult
     */
    public function getSalesTaxationType($UUID, $AddressID, $language)
    {
      return $this->__soapCall('getSalesTaxationType', array($UUID, $AddressID, $language));
    }

}
