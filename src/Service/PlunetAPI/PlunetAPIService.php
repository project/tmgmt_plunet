<?php

namespace Drupal\tmgmt_plunet\Service\PlunetAPI;




class PlunetAPIService extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'BooleanResult' => 'Drupal\tmgmt_plunet\Service\PlunetAPI\BooleanResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\PlunetAPI\Result');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/PlunetAPI?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param string $Username
     * @param string $Password
     * @access public
     * @return BooleanResult
     */
    public function validate($UUID, $Username, $Password)
    {
      return $this->__soapCall('validate', array($UUID, $Username, $Password));
    }

    /**
     * @access public
     * @return double
     */
    public function getVersion()
    {
      return $this->__soapCall('getVersion', array());
    }

    /**
     * @param string $arg0
     * @access public
     * @return void
     */
    public function logout($arg0)
    {
      return $this->__soapCall('logout', array($arg0));
    }

    /**
     * @param string $arg0
     * @param string $arg1
     * @access public
     * @return string
     */
    public function login($arg0, $arg1)
    {
      return $this->__soapCall('login', array($arg0, $arg1));
    }

}
