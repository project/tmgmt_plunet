<?php

namespace Drupal\tmgmt_plunet\Service\RequestDocText30;






class RequestDocText30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\RequestDocText30\IntegerResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\RequestDocText30\Result',
      'StringResult' => 'Drupal\tmgmt_plunet\Service\RequestDocText30\StringResult',
      'IntegerArrayResult' => 'Drupal\tmgmt_plunet\Service\RequestDocText30\IntegerArrayResult');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/RequestDocText30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param int $requestDocTextID
     * @access public
     * @return Result
     */
    public function update($UUID, $requestDocTextID)
    {
      return $this->__soapCall('update', array($UUID, $requestDocTextID));
    }

    /**
     * @param string $UUID
     * @param int $requestDocTextID
     * @access public
     * @return Result
     */
    public function delete($UUID, $requestDocTextID)
    {
      return $this->__soapCall('delete', array($UUID, $requestDocTextID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerResult
     */
    public function insert($UUID, $requestID)
    {
      return $this->__soapCall('insert', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestDocTextID
     * @access public
     * @return StringResult
     */
    public function getSourceText($UUID, $requestDocTextID)
    {
      return $this->__soapCall('getSourceText', array($UUID, $requestDocTextID));
    }

    /**
     * @param string $UUID
     * @param string $targetLanguage
     * @param int $requestDocTextID
     * @access public
     * @return StringResult
     */
    public function getTargetText($UUID, $targetLanguage, $requestDocTextID)
    {
      return $this->__soapCall('getTargetText', array($UUID, $targetLanguage, $requestDocTextID));
    }

    /**
     * @param string $UUID
     * @param int $requestDocTextID
     * @access public
     * @return IntegerResult
     */
    public function getWordCount($UUID, $requestDocTextID)
    {
      return $this->__soapCall('getWordCount', array($UUID, $requestDocTextID));
    }

    /**
     * @param string $UUID
     * @param string $sourceText
     * @param string $encoding
     * @param int $requestDocTextID
     * @access public
     * @return Result
     */
    public function setSourceText2($UUID, $sourceText, $encoding, $requestDocTextID)
    {
      return $this->__soapCall('setSourceText2', array($UUID, $sourceText, $encoding, $requestDocTextID));
    }

    /**
     * @param string $UUID
     * @param string $sourceText
     * @param int $requestDocTextID
     * @access public
     * @return Result
     */
    public function setSourceText($UUID, $sourceText, $requestDocTextID)
    {
      return $this->__soapCall('setSourceText', array($UUID, $sourceText, $requestDocTextID));
    }

    /**
     * @param string $UUID
     * @param int $wordCount
     * @param int $requestDocTextID
     * @access public
     * @return Result
     */
    public function setWordCount($UUID, $wordCount, $requestDocTextID)
    {
      return $this->__soapCall('setWordCount', array($UUID, $wordCount, $requestDocTextID));
    }

    /**
     * @param string $UUID
     * @param string $additionalInfo
     * @param int $requestDocTextID
     * @access public
     * @return Result
     */
    public function setAdditionalInfo($UUID, $additionalInfo, $requestDocTextID)
    {
      return $this->__soapCall('setAdditionalInfo', array($UUID, $additionalInfo, $requestDocTextID));
    }

    /**
     * @param string $UUID
     * @param int $requestDocTextID
     * @access public
     * @return IntegerResult
     */
    public function getCustomerTextID($UUID, $requestDocTextID)
    {
      return $this->__soapCall('getCustomerTextID', array($UUID, $requestDocTextID));
    }

    /**
     * @param string $UUID
     * @param int $requestDocTextID
     * @access public
     * @return StringResult
     */
    public function getAdditionalInfo($UUID, $requestDocTextID)
    {
      return $this->__soapCall('getAdditionalInfo', array($UUID, $requestDocTextID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerArrayResult
     */
    public function getAll_ByRequestID($UUID, $requestID)
    {
      return $this->__soapCall('getAll_ByRequestID', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $customerTextID
     * @param int $requestDocTextID
     * @access public
     * @return Result
     */
    public function setCustomerTextID($UUID, $customerTextID, $requestDocTextID)
    {
      return $this->__soapCall('setCustomerTextID', array($UUID, $customerTextID, $requestDocTextID));
    }

}
