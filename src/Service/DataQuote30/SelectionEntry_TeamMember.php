<?php

namespace Drupal\tmgmt_plunet\Service\DataQuote30;

class SelectionEntry_TeamMember
{

    /**
     * @var int $ID
     * @access public
     */
    public $ID = null;

    /**
     * @var int $teamMember
     * @access public
     */
    public $teamMember = null;

    /**
     * @param int $ID
     * @param int $teamMember
     * @access public
     */
    public function __construct($ID, $teamMember)
    {
      $this->ID = $ID;
      $this->teamMember = $teamMember;
    }

}
