<?php

namespace Drupal\tmgmt_plunet\Service\DataQuote30;

class SearchFilter_Quote
{

    /**
     * @var string $languageCode
     * @access public
     */
    public $languageCode = null;

    /**
     * @var Property[] $propertiesList
     * @access public
     */
    public $propertiesList = null;

    /**
     * @var SelectionEntry_Customer $selectionEntryCustomer
     * @access public
     */
    public $selectionEntryCustomer = null;

    /**
     * @var SelectionEntry_Resource $SelectionEntry_Resource
     * @access public
     */
    public $SelectionEntry_Resource = null;

    /**
     * @var SelectionEntry_TeamMember $SelectionEntry_TeamMember
     * @access public
     */
    public $SelectionEntry_TeamMember = null;

    /**
     * @var string $sourceLanguage
     * @access public
     */
    public $sourceLanguage = null;

    /**
     * @var int $quoteStatus
     * @access public
     */
    public $quoteStatus = null;

    /**
     * @var string $targetLanguage
     * @access public
     */
    public $targetLanguage = null;

    /**
     * @var Textmodule[] $textmodulesList
     * @access public
     */
    public $textmodulesList = null;

    /**
     * @var SelectionEntry_TimeFrame $timeFrame
     * @access public
     */
    public $timeFrame = null;

    /**
     * @param string $languageCode
     * @param Property[] $propertiesList
     * @param SelectionEntry_Customer $selectionEntryCustomer
     * @param SelectionEntry_Resource $SelectionEntry_Resource
     * @param SelectionEntry_TeamMember $SelectionEntry_TeamMember
     * @param string $sourceLanguage
     * @param int $quoteStatus
     * @param string $targetLanguage
     * @param Textmodule[] $textmodulesList
     * @param SelectionEntry_TimeFrame $timeFrame
     * @access public
     */
    public function __construct($languageCode, $propertiesList, $selectionEntryCustomer, $SelectionEntry_Resource, $SelectionEntry_TeamMember, $sourceLanguage, $quoteStatus, $targetLanguage, $textmodulesList, $timeFrame)
    {
      $this->languageCode = $languageCode;
      $this->propertiesList = $propertiesList;
      $this->selectionEntryCustomer = $selectionEntryCustomer;
      $this->SelectionEntry_Resource = $SelectionEntry_Resource;
      $this->SelectionEntry_TeamMember = $SelectionEntry_TeamMember;
      $this->sourceLanguage = $sourceLanguage;
      $this->quoteStatus = $quoteStatus;
      $this->targetLanguage = $targetLanguage;
      $this->textmodulesList = $textmodulesList;
      $this->timeFrame = $timeFrame;
    }

}
