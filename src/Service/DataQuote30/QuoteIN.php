<?php

namespace Drupal\tmgmt_plunet\Service\DataQuote30;

class QuoteIN
{

    /**
     * @var dateTime $creationDate
     * @access public
     */
    public $creationDate = null;

    /**
     * @var string $currency
     * @access public
     */
    public $currency = null;

    /**
     * @var int $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var string $projectManagerMemo
     * @access public
     */
    public $projectManagerMemo = null;

    /**
     * @var string $projectName
     * @access public
     */
    public $projectName = null;

    /**
     * @var int $quoteID
     * @access public
     */
    public $quoteID = null;

    /**
     * @var string $referenceNumber
     * @access public
     */
    public $referenceNumber = null;

    /**
     * @var int $status
     * @access public
     */
    public $status = null;

    /**
     * @var string $subject
     * @access public
     */
    public $subject = null;

    /**
     * @param dateTime $creationDate
     * @param string $currency
     * @param int $customerID
     * @param string $projectManagerMemo
     * @param string $projectName
     * @param int $quoteID
     * @param string $referenceNumber
     * @param int $status
     * @param string $subject
     * @access public
     */
    public function __construct($creationDate, $currency, $customerID, $projectManagerMemo, $projectName, $quoteID, $referenceNumber, $status, $subject)
    {
      $this->creationDate = $creationDate;
      $this->currency = $currency;
      $this->customerID = $customerID;
      $this->projectManagerMemo = $projectManagerMemo;
      $this->projectName = $projectName;
      $this->quoteID = $quoteID;
      $this->referenceNumber = $referenceNumber;
      $this->status = $status;
      $this->subject = $subject;
    }

}
