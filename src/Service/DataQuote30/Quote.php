<?php

namespace Drupal\tmgmt_plunet\Service\DataQuote30;

class Quote
{

    /**
     * @var dateTime $creationDate
     * @access public
     */
    public $creationDate = null;

    /**
     * @var string $currency
     * @access public
     */
    public $currency = null;

    /**
     * @var int[] $orderID
     * @access public
     */
    public $orderID = null;

    /**
     * @var string $projectName
     * @access public
     */
    public $projectName = null;

    /**
     * @var int $quoteID
     * @access public
     */
    public $quoteID = null;

    /**
     * @var string $quoteNumber
     * @access public
     */
    public $quoteNumber = null;

    /**
     * @var float $rate
     * @access public
     */
    public $rate = null;

    /**
     * @var int $requestID
     * @access public
     */
    public $requestID = null;

    /**
     * @var int $status
     * @access public
     */
    public $status = null;

    /**
     * @var string $subject
     * @access public
     */
    public $subject = null;

    /**
     * @param dateTime $creationDate
     * @param string $currency
     * @param string $projectName
     * @param int $quoteID
     * @param string $quoteNumber
     * @param float $rate
     * @param int $requestID
     * @param int $status
     * @param string $subject
     * @access public
     */
    public function __construct($creationDate, $currency, $projectName, $quoteID, $quoteNumber, $rate, $requestID, $status, $subject)
    {
      $this->creationDate = $creationDate;
      $this->currency = $currency;
      $this->projectName = $projectName;
      $this->quoteID = $quoteID;
      $this->quoteNumber = $quoteNumber;
      $this->rate = $rate;
      $this->requestID = $requestID;
      $this->status = $status;
      $this->subject = $subject;
    }

}
