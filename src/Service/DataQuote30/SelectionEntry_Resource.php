<?php

namespace Drupal\tmgmt_plunet\Service\DataQuote30;

class SelectionEntry_Resource
{

    /**
     * @var int $mainID
     * @access public
     */
    public $mainID = null;

    /**
     * @var int $resourceEntryType
     * @access public
     */
    public $resourceEntryType = null;

    /**
     * @param int $mainID
     * @param int $resourceEntryType
     * @access public
     */
    public function __construct($mainID, $resourceEntryType)
    {
      $this->mainID = $mainID;
      $this->resourceEntryType = $resourceEntryType;
    }

}
