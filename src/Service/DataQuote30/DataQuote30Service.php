<?php

namespace Drupal\tmgmt_plunet\Service\DataQuote30;






















class DataQuote30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Result' => 'Drupal\tmgmt_plunet\Service\DataQuote30\Result',
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\DataQuote30\IntegerResult',
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataQuote30\StringResult',
      'TemplateListResult' => 'Drupal\tmgmt_plunet\Service\DataQuote30\TemplateListResult',
      'Template' => 'Drupal\tmgmt_plunet\Service\DataQuote30\Template',
      'QuoteResult' => 'Drupal\tmgmt_plunet\Service\DataQuote30\QuoteResult',
      'Quote' => 'Drupal\tmgmt_plunet\Service\DataQuote30\Quote',
      'IntegerList' => 'Drupal\tmgmt_plunet\Service\DataQuote30\IntegerList',
      'QuoteListResult' => 'Drupal\tmgmt_plunet\Service\DataQuote30\QuoteListResult',
      'QuoteIN' => 'Drupal\tmgmt_plunet\Service\DataQuote30\QuoteIN',
      'SearchFilter_Quote' => 'Drupal\tmgmt_plunet\Service\DataQuote30\SearchFilter_Quote',
      'Property' => 'Drupal\tmgmt_plunet\Service\DataQuote30\Property',
      'SelectionEntry_Customer' => 'Drupal\tmgmt_plunet\Service\DataQuote30\SelectionEntry_Customer',
      'SelectionEntry_Resource' => 'Drupal\tmgmt_plunet\Service\DataQuote30\SelectionEntry_Resource',
      'SelectionEntry_TeamMember' => 'Drupal\tmgmt_plunet\Service\DataQuote30\SelectionEntry_TeamMember',
      'Textmodule' => 'Drupal\tmgmt_plunet\Service\DataQuote30\Textmodule',
      'SelectionEntry_TimeFrame' => 'Drupal\tmgmt_plunet\Service\DataQuote30\SelectionEntry_TimeFrame',
      'IntegerArrayResult' => 'Drupal\tmgmt_plunet\Service\DataQuote30\IntegerArrayResult',
      'DoubleResult' => 'Drupal\tmgmt_plunet\Service\DataQuote30\DoubleResult',
      'DateResult' => 'Drupal\tmgmt_plunet\Service\DataQuote30\DateResult');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataQuote30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param QuoteIN $QuoteIN
     * @param boolean $enableNullOrEmptyValues
     * @access public
     * @return Result
     */
    public function update($UUID, QuoteIN $QuoteIN, $enableNullOrEmptyValues)
    {
      return $this->__soapCall('update', array($UUID, $QuoteIN, $enableNullOrEmptyValues));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return Result
     */
    public function delete($UUID, $quoteID)
    {
      return $this->__soapCall('delete', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @access public
     * @return IntegerResult
     */
    public function insert($UUID)
    {
      return $this->__soapCall('insert', array($UUID));
    }

    /**
     * @param string $UUID
     * @param SearchFilter_Quote $SearchFilter
     * @access public
     * @return IntegerArrayResult
     */
    public function search($UUID, SearchFilter_Quote $SearchFilter)
    {
      return $this->__soapCall('search', array($UUID, $SearchFilter));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return StringResult
     */
    public function getCurrency($UUID, $quoteID)
    {
      return $this->__soapCall('getCurrency', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return StringResult
     */
    public function getExternalID($UUID, $quoteID)
    {
      return $this->__soapCall('getExternalID', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @param string $externalID
     * @access public
     * @return Result
     */
    public function setExternalID($UUID, $quoteID, $externalID)
    {
      return $this->__soapCall('setExternalID', array($UUID, $quoteID, $externalID));
    }

    /**
     * @param string $UUID
     * @param QuoteIN $QuoteIN
     * @access public
     * @return IntegerResult
     */
    public function insert2($UUID, QuoteIN $QuoteIN)
    {
      return $this->__soapCall('insert2', array($UUID, $QuoteIN));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @param int $quoteID
     * @access public
     * @return Result
     */
    public function setCustomerID($UUID, $customerID, $quoteID)
    {
      return $this->__soapCall('setCustomerID', array($UUID, $customerID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return StringResult
     */
    public function getProjectName($UUID, $quoteID)
    {
      return $this->__soapCall('getProjectName', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return DoubleResult
     */
    public function getRate($UUID, $quoteID)
    {
      return $this->__soapCall('getRate', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param dateTime $date
     * @param int $quoteID
     * @access public
     * @return Result
     */
    public function setCreationDate($UUID, $date, $quoteID)
    {
      return $this->__soapCall('setCreationDate', array($UUID, $date, $quoteID));
    }

    /**
     * @param string $UUID
     * @param string $ProjectName
     * @param int $quoteID
     * @access public
     * @return Result
     */
    public function setProjectName($UUID, $ProjectName, $quoteID)
    {
      return $this->__soapCall('setProjectName', array($UUID, $ProjectName, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return StringResult
     */
    public function getQuoteDocument($UUID, $quoteID)
    {
      return $this->__soapCall('getQuoteDocument', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return StringResult
     */
    public function getSubject($UUID, $quoteID)
    {
      return $this->__soapCall('getSubject', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @access public
     * @return TemplateListResult
     */
    public function getTemplateList($UUID)
    {
      return $this->__soapCall('getTemplateList', array($UUID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setRequestID($UUID, $quoteID, $requestID)
    {
      return $this->__soapCall('setRequestID', array($UUID, $quoteID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return IntegerResult
     */
    public function getRequestId($UUID, $quoteID)
    {
      return $this->__soapCall('getRequestId', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return QuoteResult
     */
    public function getQuoteObject($UUID, $quoteID)
    {
      return $this->__soapCall('getQuoteObject', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param string $quoteNumber
     * @access public
     * @return QuoteResult
     */
    public function getQuoteObject2($UUID, $quoteNumber)
    {
      return $this->__soapCall('getQuoteObject2', array($UUID, $quoteNumber));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return IntegerResult
     */
    public function getProjectStatus($UUID, $quoteID)
    {
      return $this->__soapCall('getProjectStatus', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $projectClassType
     * @access public
     * @return Result
     */
    public function requestQuote($UUID, $projectClassType)
    {
      return $this->__soapCall('requestQuote', array($UUID, $projectClassType));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return Result
     */
    public function requestOrder($UUID, $quoteID)
    {
      return $this->__soapCall('requestOrder', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @param int $projectStatus
     * @access public
     * @return Result
     */
    public function setProjectStatus($UUID, $quoteID, $projectStatus)
    {
      return $this->__soapCall('setProjectStatus', array($UUID, $quoteID, $projectStatus));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return IntegerResult
     */
    public function getCustomerID($UUID, $quoteID)
    {
      return $this->__soapCall('getCustomerID', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param string $displayNo
     * @access public
     * @return IntegerResult
     */
    public function getQuoteID($UUID, $displayNo)
    {
      return $this->__soapCall('getQuoteID', array($UUID, $displayNo));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return DateResult
     */
    public function getCreationDate($UUID, $quoteID)
    {
      return $this->__soapCall('getCreationDate', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param string $subject
     * @param int $quoteID
     * @access public
     * @return Result
     */
    public function setSubject($UUID, $subject, $quoteID)
    {
      return $this->__soapCall('setSubject', array($UUID, $subject, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return IntegerResult
     */
    public function getStatus($UUID, $quoteID)
    {
      return $this->__soapCall('getStatus', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param string $sourceLanguage
     * @param string $targetLanguage
     * @param int $quoteID
     * @access public
     * @return IntegerResult
     */
    public function addLanguageCombination($UUID, $sourceLanguage, $targetLanguage, $quoteID)
    {
      return $this->__soapCall('addLanguageCombination', array($UUID, $sourceLanguage, $targetLanguage, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $Status
     * @param int $quoteID
     * @access public
     * @return Result
     */
    public function setStatus($UUID, $Status, $quoteID)
    {
      return $this->__soapCall('setStatus', array($UUID, $Status, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return IntegerResult
     */
    public function getCustomerContactID($UUID, $quoteID)
    {
      return $this->__soapCall('getCustomerContactID', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $customerContactID
     * @param int $quoteID
     * @access public
     * @return Result
     */
    public function setCustomerContactID($UUID, $customerContactID, $quoteID)
    {
      return $this->__soapCall('setCustomerContactID', array($UUID, $customerContactID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param string $ServerAuthenticationString
     * @param string $ServerAddress
     * @param int $EventType
     * @access public
     * @return Result
     */
    public function registerCallback_Notify($UUID, $ServerAuthenticationString, $ServerAddress, $EventType)
    {
      return $this->__soapCall('registerCallback_Notify', array($UUID, $ServerAuthenticationString, $ServerAddress, $EventType));
    }

    /**
     * @param string $UUID
     * @param string $ServerAuthenticationString
     * @param string $ServerAddress
     * @param int $QuoteID
     * @access public
     * @return Result
     */
    public function registerCallback_Observer($UUID, $ServerAuthenticationString, $ServerAddress, $QuoteID)
    {
      return $this->__soapCall('registerCallback_Observer', array($UUID, $ServerAuthenticationString, $ServerAddress, $QuoteID));
    }

    /**
     * @param string $UUID
     * @param int $EventType
     * @access public
     * @return Result
     */
    public function deregisterCallback_Notify($UUID, $EventType)
    {
      return $this->__soapCall('deregisterCallback_Notify', array($UUID, $EventType));
    }

    /**
     * @param string $UUID
     * @param int $QuoteID
     * @access public
     * @return Result
     */
    public function deregisterCallback_Observer($UUID, $QuoteID)
    {
      return $this->__soapCall('deregisterCallback_Observer', array($UUID, $QuoteID));
    }

    /**
     * @param string $UUID
     * @param string $systemLanguageCode
     * @param int $quoteID
     * @access public
     * @return StringResult
     */
    public function getProjectCategory($UUID, $systemLanguageCode, $quoteID)
    {
      return $this->__soapCall('getProjectCategory', array($UUID, $systemLanguageCode, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return StringResult
     */
    public function getQuoteNo_for_View($UUID, $quoteID)
    {
      return $this->__soapCall('getQuoteNo_for_View', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return IntegerResult
     */
    public function getOrderIDFirstItem($UUID, $quoteID)
    {
      return $this->__soapCall('getOrderIDFirstItem', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param string $template
     * @param int $formatId
     * @param int $quoteID
     * @access public
     * @return StringResult
     */
    public function createQuoteDocument($UUID, $template, $formatId, $quoteID)
    {
      return $this->__soapCall('createQuoteDocument', array($UUID, $template, $formatId, $quoteID));
    }

    /**
     * @param string $UUID
     * @param string $projectCategory
     * @param string $systemLanguageCode
     * @param int $quoteID
     * @access public
     * @return Result
     */
    public function setProjectCategory($UUID, $projectCategory, $systemLanguageCode, $quoteID)
    {
      return $this->__soapCall('setProjectCategory', array($UUID, $projectCategory, $systemLanguageCode, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @param string $reference
     * @access public
     * @return Result
     */
    public function setReferenceNumber($UUID, $quoteID, $reference)
    {
      return $this->__soapCall('setReferenceNumber', array($UUID, $quoteID, $reference));
    }

    /**
     * @param string $UUID
     * @param IntegerList $quoteIDList
     * @access public
     * @return QuoteListResult
     */
    public function getQuoteObjectList($UUID, IntegerList $quoteIDList)
    {
      return $this->__soapCall('getQuoteObjectList', array($UUID, $quoteIDList));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @param int $quoteID
     * @access public
     * @return Result
     */
    public function setProjectmanagerID($UUID, $resourceID, $quoteID)
    {
      return $this->__soapCall('setProjectmanagerID', array($UUID, $resourceID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return IntegerResult
     */
    public function getProjectmanagerID($UUID, $quoteID)
    {
      return $this->__soapCall('getProjectmanagerID', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return StringResult
     */
    public function getProjectManagerMemo($UUID, $quoteID)
    {
      return $this->__soapCall('getProjectManagerMemo', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param QuoteIN $QuoteIN
     * @param int $TemplateID
     * @access public
     * @return IntegerResult
     */
    public function insert_byTemplate($UUID, QuoteIN $QuoteIN, $TemplateID)
    {
      return $this->__soapCall('insert_byTemplate', array($UUID, $QuoteIN, $TemplateID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @access public
     * @return StringResult
     */
    public function getReferenceNumber($UUID, $quoteID)
    {
      return $this->__soapCall('getReferenceNumber', array($UUID, $quoteID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @param string $memo
     * @access public
     * @return Result
     */
    public function setProjectManagerMemo($UUID, $quoteID, $memo)
    {
      return $this->__soapCall('setProjectManagerMemo', array($UUID, $quoteID, $memo));
    }

}
