<?php

namespace Drupal\tmgmt_plunet\Service\DataQuote30;

class Template
{

    /**
     * @var int $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var string $templateDescription
     * @access public
     */
    public $templateDescription = null;

    /**
     * @var int $templateID
     * @access public
     */
    public $templateID = null;

    /**
     * @param int $customerID
     * @param string $templateDescription
     * @param int $templateID
     * @access public
     */
    public function __construct($customerID, $templateDescription, $templateID)
    {
      $this->customerID = $customerID;
      $this->templateDescription = $templateDescription;
      $this->templateID = $templateID;
    }

}
