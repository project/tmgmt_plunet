<?php

namespace Drupal\tmgmt_plunet\Service\CallbackResource30;


class CallbackResource30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Resource' => 'Drupal\tmgmt_plunet\Service\CallbackResource30\Resource');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/CallbackResource30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $Authenticationcode
     * @param Resource $Resource
     * @param int $EventType
     * @access public
     * @return void
     */
    public function receiveObserverCallback($Authenticationcode, Resource $Resource, $EventType)
    {
      return $this->__soapCall('receiveObserverCallback', array($Authenticationcode, $Resource, $EventType));
    }

    /**
     * @param string $Authenticationcode
     * @param int $ResourceID
     * @param int $EventType
     * @access public
     * @return void
     */
    public function receiveNotifyCallback($Authenticationcode, $ResourceID, $EventType)
    {
      return $this->__soapCall('receiveNotifyCallback', array($Authenticationcode, $ResourceID, $EventType));
    }

}
