<?php

namespace Drupal\tmgmt_plunet\Service\DataCustomer30;



class PaymentInfoResult extends Result
{

    /**
     * @var PaymentInfo $data
     * @access public
     */
    public $data = null;

    /**
     * @param int $statusCode
     * @param string $statusCodeAlphanumeric
     * @param string $statusMessage
     * @param PaymentInfo $data
     * @access public
     */
    public function __construct($statusCode, $statusCodeAlphanumeric, $statusMessage, $data)
    {
      parent::__construct($statusCode, $statusCodeAlphanumeric, $statusMessage);
      $this->data = $data;
    }

}
