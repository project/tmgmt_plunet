<?php

namespace Drupal\tmgmt_plunet\Service\DataCustomer30;

class CustomerIN
{

    /**
     * @var string $academicTitle
     * @access public
     */
    public $academicTitle = null;

    /**
     * @var string $costCenter
     * @access public
     */
    public $costCenter = null;

    /**
     * @var string $currency
     * @access public
     */
    public $currency = null;

    /**
     * @var int $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var string $email
     * @access public
     */
    public $email = null;

    /**
     * @var string $externalID
     * @access public
     */
    public $externalID = null;

    /**
     * @var string $fax
     * @access public
     */
    public $fax = null;

    /**
     * @var int $formOfAddress
     * @access public
     */
    public $formOfAddress = null;

    /**
     * @var string $fullName
     * @access public
     */
    public $fullName = null;

    /**
     * @var string $mobilePhone
     * @access public
     */
    public $mobilePhone = null;

    /**
     * @var string $name1
     * @access public
     */
    public $name1 = null;

    /**
     * @var string $name2
     * @access public
     */
    public $name2 = null;

    /**
     * @var string $opening
     * @access public
     */
    public $opening = null;

    /**
     * @var string $phone
     * @access public
     */
    public $phone = null;

    /**
     * @var string $skypeID
     * @access public
     */
    public $skypeID = null;

    /**
     * @var int $status
     * @access public
     */
    public $status = null;

    /**
     * @var int $userId
     * @access public
     */
    public $userId = null;

    /**
     * @var string $website
     * @access public
     */
    public $website = null;

    /**
     * @param string $academicTitle
     * @param string $costCenter
     * @param string $currency
     * @param int $customerID
     * @param string $email
     * @param string $externalID
     * @param string $fax
     * @param int $formOfAddress
     * @param string $fullName
     * @param string $mobilePhone
     * @param string $name1
     * @param string $name2
     * @param string $opening
     * @param string $phone
     * @param string $skypeID
     * @param int $status
     * @param int $userId
     * @param string $website
     * @access public
     */
    public function __construct($academicTitle, $costCenter, $currency, $customerID, $email, $externalID, $fax, $formOfAddress, $fullName, $mobilePhone, $name1, $name2, $opening, $phone, $skypeID, $status, $userId, $website)
    {
      $this->academicTitle = $academicTitle;
      $this->costCenter = $costCenter;
      $this->currency = $currency;
      $this->customerID = $customerID;
      $this->email = $email;
      $this->externalID = $externalID;
      $this->fax = $fax;
      $this->formOfAddress = $formOfAddress;
      $this->fullName = $fullName;
      $this->mobilePhone = $mobilePhone;
      $this->name1 = $name1;
      $this->name2 = $name2;
      $this->opening = $opening;
      $this->phone = $phone;
      $this->skypeID = $skypeID;
      $this->status = $status;
      $this->userId = $userId;
      $this->website = $website;
    }

}
