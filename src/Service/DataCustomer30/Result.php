<?php

namespace Drupal\tmgmt_plunet\Service\DataCustomer30;

class Result
{

    /**
     * @var int $statusCode
     * @access public
     */
    public $statusCode = null;

    /**
     * @var string $statusCodeAlphanumeric
     * @access public
     */
    public $statusCodeAlphanumeric = null;

    /**
     * @var string $statusMessage
     * @access public
     */
    public $statusMessage = null;

    /**
     * @var int[] $warning_StatusCodeList
     * @access public
     */
    public $warning_StatusCodeList = null;

    /**
     * @param int $statusCode
     * @param string $statusCodeAlphanumeric
     * @param string $statusMessage
     * @access public
     */
    public function __construct($statusCode, $statusCodeAlphanumeric, $statusMessage)
    {
      $this->statusCode = $statusCode;
      $this->statusCodeAlphanumeric = $statusCodeAlphanumeric;
      $this->statusMessage = $statusMessage;
    }

}
