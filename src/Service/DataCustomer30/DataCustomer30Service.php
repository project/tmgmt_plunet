<?php

namespace Drupal\tmgmt_plunet\Service\DataCustomer30;





















class DataCustomer30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\StringResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\Result',
      'DateResult' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\DateResult',
      'CustomerListResult' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\CustomerListResult',
      'Customer' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\Customer',
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\IntegerResult',
      'CustomerIN' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\CustomerIN',
      'IntegerArrayResult' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\IntegerArrayResult',
      'IntegerList' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\IntegerList',
      'CustomerResult' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\CustomerResult',
      'AccountResult' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\AccountResult',
      'Account' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\Account',
      'SearchFilter_Customer' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\SearchFilter_Customer',
      'Property' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\Property',
      'Textmodule' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\Textmodule',
      'PaymentInfoResult' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\PaymentInfoResult',
      'PaymentInfo' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\PaymentInfo',
      'WorkflowListResult' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\WorkflowListResult',
      'Workflow' => 'Drupal\tmgmt_plunet\Service\DataCustomer30\Workflow');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataCustomer30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param CustomerIN $CustomerIN
     * @param boolean $enableNullOrEmptyValues
     * @access public
     * @return Result
     */
    public function update($UUID, CustomerIN $CustomerIN, $enableNullOrEmptyValues)
    {
      return $this->__soapCall('update', array($UUID, $CustomerIN, $enableNullOrEmptyValues));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function delete($UUID, $customerID)
    {
      return $this->__soapCall('delete', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @access public
     * @return IntegerResult
     */
    public function insert($UUID)
    {
      return $this->__soapCall('insert', array($UUID));
    }

    /**
     * @param string $UUID
     * @param SearchFilter_Customer $SearchFilter
     * @access public
     * @return IntegerArrayResult
     */
    public function search($UUID, SearchFilter_Customer $SearchFilter)
    {
      return $this->__soapCall('search', array($UUID, $SearchFilter));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getCurrency($UUID, $customerID)
    {
      return $this->__soapCall('getCurrency', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param string $SkypeID
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function setSkypeID($UUID, $SkypeID, $customerID)
    {
      return $this->__soapCall('setSkypeID', array($UUID, $SkypeID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getFax($UUID, $customerID)
    {
      return $this->__soapCall('getFax', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getSkypeID($UUID, $customerID)
    {
      return $this->__soapCall('getSkypeID', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param string $Fax
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function setFax($UUID, $Fax, $customerID)
    {
      return $this->__soapCall('setFax', array($UUID, $Fax, $customerID));
    }

    /**
     * @param string $UUID
     * @param string $PhoneNumber
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function setMobilePhone($UUID, $PhoneNumber, $customerID)
    {
      return $this->__soapCall('setMobilePhone', array($UUID, $PhoneNumber, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getEmail($UUID, $customerID)
    {
      return $this->__soapCall('getEmail', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getWebsite($UUID, $customerID)
    {
      return $this->__soapCall('getWebsite', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param string $PhoneNumber
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function setPhone($UUID, $PhoneNumber, $customerID)
    {
      return $this->__soapCall('setPhone', array($UUID, $PhoneNumber, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getPhone($UUID, $customerID)
    {
      return $this->__soapCall('getPhone', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param string $Website
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function setWebsite($UUID, $Website, $customerID)
    {
      return $this->__soapCall('setWebsite', array($UUID, $Website, $customerID));
    }

    /**
     * @param string $UUID
     * @param string $EMail
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function setEmail($UUID, $EMail, $customerID)
    {
      return $this->__soapCall('setEmail', array($UUID, $EMail, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getMobilePhone($UUID, $customerID)
    {
      return $this->__soapCall('getMobilePhone', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getExternalID($UUID, $customerID)
    {
      return $this->__soapCall('getExternalID', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param string $ExternalID
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function setExternalID($UUID, $ExternalID, $customerID)
    {
      return $this->__soapCall('setExternalID', array($UUID, $ExternalID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return IntegerResult
     */
    public function getFormOfAddress($UUID, $customerID)
    {
      return $this->__soapCall('getFormOfAddress', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param string $AcademicTitle
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function setAcademicTitle($UUID, $AcademicTitle, $customerID)
    {
      return $this->__soapCall('setAcademicTitle', array($UUID, $AcademicTitle, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getName1($UUID, $customerID)
    {
      return $this->__soapCall('getName1', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param string $Name
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function setName2($UUID, $Name, $customerID)
    {
      return $this->__soapCall('setName2', array($UUID, $Name, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getOpening($UUID, $customerID)
    {
      return $this->__soapCall('getOpening', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param CustomerIN $CustomerIN
     * @access public
     * @return IntegerResult
     */
    public function insert2($UUID, CustomerIN $CustomerIN)
    {
      return $this->__soapCall('insert2', array($UUID, $CustomerIN));
    }

    /**
     * @param string $UUID
     * @param string $ExternalID
     * @access public
     * @return IntegerResult
     */
    public function seekByExternalID($UUID, $ExternalID)
    {
      return $this->__soapCall('seekByExternalID', array($UUID, $ExternalID));
    }

    /**
     * @param string $UUID
     * @param string $Name
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function setName1($UUID, $Name, $customerID)
    {
      return $this->__soapCall('setName1', array($UUID, $Name, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $FormOfAddress
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function setFormOfAddress($UUID, $FormOfAddress, $customerID)
    {
      return $this->__soapCall('setFormOfAddress', array($UUID, $FormOfAddress, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getAcademicTitle($UUID, $customerID)
    {
      return $this->__soapCall('getAcademicTitle', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getName2($UUID, $customerID)
    {
      return $this->__soapCall('getName2', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param string $Opening
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function setOpening($UUID, $Opening, $customerID)
    {
      return $this->__soapCall('setOpening', array($UUID, $Opening, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $AccountID
     * @access public
     * @return AccountResult
     */
    public function getAccount($UUID, $AccountID)
    {
      return $this->__soapCall('getAccount', array($UUID, $AccountID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getDossier($UUID, $customerID)
    {
      return $this->__soapCall('getDossier', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @param string $dossier
     * @access public
     * @return Result
     */
    public function setDossier($UUID, $customerID, $dossier)
    {
      return $this->__soapCall('setDossier', array($UUID, $customerID, $dossier));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getFullName($UUID, $customerID)
    {
      return $this->__soapCall('getFullName', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return IntegerResult
     */
    public function getStatus($UUID, $customerID)
    {
      return $this->__soapCall('getStatus', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $Status
     * @param int $customerID
     * @access public
     * @return Result
     */
    public function setStatus($UUID, $Status, $customerID)
    {
      return $this->__soapCall('setStatus', array($UUID, $Status, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $Status
     * @access public
     * @return CustomerListResult
     */
    public function getAllCustomerObjects($UUID, $Status)
    {
      return $this->__soapCall('getAllCustomerObjects', array($UUID, $Status));
    }

    /**
     * @param string $UUID
     * @param IntegerList $StatusList
     * @access public
     * @return CustomerListResult
     */
    public function getAllCustomerObjects2($UUID, IntegerList $StatusList)
    {
      return $this->__soapCall('getAllCustomerObjects2', array($UUID, $StatusList));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return CustomerResult
     */
    public function getCustomerObject($UUID, $customerID)
    {
      return $this->__soapCall('getCustomerObject', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return DateResult
     */
    public function getDateOfInitialContact($UUID, $customerID)
    {
      return $this->__soapCall('getDateOfInitialContact', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param string $ServerAuthenticationString
     * @param string $ServerAddress
     * @param int $EventType
     * @access public
     * @return Result
     */
    public function registerCallback_Notify($UUID, $ServerAuthenticationString, $ServerAddress, $EventType)
    {
      return $this->__soapCall('registerCallback_Notify', array($UUID, $ServerAuthenticationString, $ServerAddress, $EventType));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setProjectManagerID($UUID, $customerID, $resourceID)
    {
      return $this->__soapCall('setProjectManagerID', array($UUID, $customerID, $resourceID));
    }

    /**
     * @param string $UUID
     * @access public
     * @return IntegerArrayResult
     */
    public function getAvailablePaymentMethodList($UUID)
    {
      return $this->__soapCall('getAvailablePaymentMethodList', array($UUID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @param PaymentInfo $paymentInfo
     * @access public
     * @return Result
     */
    public function setPaymentInformation($UUID, $customerID, PaymentInfo $paymentInfo)
    {
      return $this->__soapCall('setPaymentInformation', array($UUID, $customerID, $paymentInfo));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @param dateTime $dateInitialContact
     * @access public
     * @return Result
     */
    public function setDateOfInitialContact($UUID, $customerID, $dateInitialContact)
    {
      return $this->__soapCall('setDateOfInitialContact', array($UUID, $customerID, $dateInitialContact));
    }

    /**
     * @param string $UUID
     * @param string $ServerAuthenticationString
     * @param string $ServerAddress
     * @param int $CustomerID
     * @access public
     * @return Result
     */
    public function registerCallback_Observer($UUID, $ServerAuthenticationString, $ServerAddress, $CustomerID)
    {
      return $this->__soapCall('registerCallback_Observer', array($UUID, $ServerAuthenticationString, $ServerAddress, $CustomerID));
    }

    /**
     * @param string $UUID
     * @access public
     * @return IntegerArrayResult
     */
    public function getAvailableAccountIDList($UUID)
    {
      return $this->__soapCall('getAvailableAccountIDList', array($UUID));
    }

    /**
     * @param string $UUID
     * @param int $paymentMethodID
     * @param string $systemLanguageCode
     * @access public
     * @return StringResult
     */
    public function getPaymentMethodDescription($UUID, $paymentMethodID, $systemLanguageCode)
    {
      return $this->__soapCall('getPaymentMethodDescription', array($UUID, $paymentMethodID, $systemLanguageCode));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return IntegerResult
     */
    public function getAccountManagerID($UUID, $customerID)
    {
      return $this->__soapCall('getAccountManagerID', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setAccountManagerID($UUID, $customerID, $resourceID)
    {
      return $this->__soapCall('setAccountManagerID', array($UUID, $customerID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @param string $text
     * @access public
     * @return Result
     */
    public function setSourceOfContact($UUID, $customerID, $text)
    {
      return $this->__soapCall('setSourceOfContact', array($UUID, $customerID, $text));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return StringResult
     */
    public function getSourceOfContact($UUID, $customerID)
    {
      return $this->__soapCall('getSourceOfContact', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return IntegerResult
     */
    public function getProjectManagerID($UUID, $customerID)
    {
      return $this->__soapCall('getProjectManagerID', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return PaymentInfoResult
     */
    public function getPaymentInformation($UUID, $customerID)
    {
      return $this->__soapCall('getPaymentInformation', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return IntegerResult
     */
    public function getCreatedByResourceID($UUID, $customerID)
    {
      return $this->__soapCall('getCreatedByResourceID', array($UUID, $customerID));
    }

    /**
     * @param string $UUID
     * @param int $EventType
     * @access public
     * @return Result
     */
    public function deregisterCallback_Notify($UUID, $EventType)
    {
      return $this->__soapCall('deregisterCallback_Notify', array($UUID, $EventType));
    }

    /**
     * @param string $UUID
     * @param int $CustomerID
     * @access public
     * @return Result
     */
    public function deregisterCallback_Observer($UUID, $CustomerID)
    {
      return $this->__soapCall('deregisterCallback_Observer', array($UUID, $CustomerID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @access public
     * @return WorkflowListResult
     */
    public function getAvailableWorkflows($UUID, $customerID)
    {
      return $this->__soapCall('getAvailableWorkflows', array($UUID, $customerID));
    }

}
