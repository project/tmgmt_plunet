<?php

namespace Drupal\tmgmt_plunet\Service\DataCustomer30;

class Workflow
{

    /**
     * @var string $description
     * @access public
     */
    public $description = null;

    /**
     * @var string $name
     * @access public
     */
    public $name = null;

    /**
     * @var int $status
     * @access public
     */
    public $status = null;

    /**
     * @var int $type
     * @access public
     */
    public $type = null;

    /**
     * @var int $workflowId
     * @access public
     */
    public $workflowId = null;

    /**
     * @param string $description
     * @param string $name
     * @param int $status
     * @param int $type
     * @param int $workflowId
     * @access public
     */
    public function __construct($description, $name, $status, $type, $workflowId)
    {
      $this->description = $description;
      $this->name = $name;
      $this->status = $status;
      $this->type = $type;
      $this->workflowId = $workflowId;
    }

}
