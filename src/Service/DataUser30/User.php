<?php

namespace Drupal\tmgmt_plunet\Service\DataUser30;

class User
{

    /**
     * @var int $contactID
     * @access public
     */
    public $contactID = null;

    /**
     * @var int $contactType
     * @access public
     */
    public $contactType = null;

    /**
     * @var int $customerContactID
     * @access public
     */
    public $customerContactID = null;

    /**
     * @var string $password
     * @access public
     */
    public $password = null;

    /**
     * @var int $userID
     * @access public
     */
    public $userID = null;

    /**
     * @var string $userName
     * @access public
     */
    public $userName = null;

    /**
     * @param int $contactID
     * @param int $contactType
     * @param int $customerContactID
     * @param string $password
     * @param int $userID
     * @param string $userName
     * @access public
     */
    public function __construct($contactID, $contactType, $customerContactID, $password, $userID, $userName)
    {
      $this->contactID = $contactID;
      $this->contactType = $contactType;
      $this->customerContactID = $customerContactID;
      $this->password = $password;
      $this->userID = $userID;
      $this->userName = $userName;
    }

}
