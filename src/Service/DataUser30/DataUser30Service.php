<?php

namespace Drupal\tmgmt_plunet\Service\DataUser30;






class DataUser30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'UserResult' => 'Drupal\tmgmt_plunet\Service\DataUser30\UserResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\DataUser30\Result',
      'User' => 'Drupal\tmgmt_plunet\Service\DataUser30\User',
      'UserListResult' => 'Drupal\tmgmt_plunet\Service\DataUser30\UserListResult');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataUser30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param int $userID
     * @access public
     * @return UserResult
     */
    public function getUserByID($UUID, $userID)
    {
      return $this->__soapCall('getUserByID', array($UUID, $userID));
    }

    /**
     * @param string $UUID
     * @param string $UserLoginName
     * @access public
     * @return UserResult
     */
    public function getUserByLoginName($UUID, $UserLoginName)
    {
      return $this->__soapCall('getUserByLoginName', array($UUID, $UserLoginName));
    }

    /**
     * @param string $UUID
     * @param int $resourceID
     * @access public
     * @return UserListResult
     */
    public function getUserListByResourceID($UUID, $resourceID)
    {
      return $this->__soapCall('getUserListByResourceID', array($UUID, $resourceID));
    }

}
