<?php

namespace Drupal\tmgmt_plunet\Service\DataJob30;

class JobTrackingTime
{

    /**
     * @var int $resourceID
     * @access public
     */
    public $resourceID = null;

    /**
     * @var string $comment
     * @access public
     */
    public $comment = null;

    /**
     * @var dateTime $dateFrom
     * @access public
     */
    public $dateFrom = null;

    /**
     * @var dateTime $dateTo
     * @access public
     */
    public $dateTo = null;

    /**
     * @param int $resourceID
     * @param string $comment
     * @param dateTime $dateFrom
     * @param dateTime $dateTo
     * @access public
     */
    public function __construct($resourceID, $comment, $dateFrom, $dateTo)
    {
      $this->resourceID = $resourceID;
      $this->comment = $comment;
      $this->dateFrom = $dateFrom;
      $this->dateTo = $dateTo;
    }

}
