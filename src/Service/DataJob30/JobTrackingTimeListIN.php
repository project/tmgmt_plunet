<?php

namespace Drupal\tmgmt_plunet\Service\DataJob30;

class JobTrackingTimeListIN
{

    /**
     * @var JobTrackingTimeIN[] $trackingTimes
     * @access public
     */
    public $trackingTimes = null;

    /**
     * @param JobTrackingTimeIN[] $trackingTimes
     * @access public
     */
    public function __construct($trackingTimes)
    {
      $this->trackingTimes = $trackingTimes;
    }

}
