<?php

namespace Drupal\tmgmt_plunet\Service\DataJob30;

class Pricelist
{

    /**
     * @var int $ResourcePricelistID
     * @access public
     */
    public $ResourcePricelistID = null;

    /**
     * @var int $pricelistID
     * @access public
     */
    public $pricelistID = null;

    /**
     * @var string $PricelistNameEN
     * @access public
     */
    public $PricelistNameEN = null;

    /**
     * @var string $currency
     * @access public
     */
    public $currency = null;

    /**
     * @var string $memo
     * @access public
     */
    public $memo = null;

    /**
     * @var boolean $withWhiteSpace
     * @access public
     */
    public $withWhiteSpace = null;

    /**
     * @param int $ResourcePricelistID
     * @param int $pricelistID
     * @param string $PricelistNameEN
     * @param string $currency
     * @param string $memo
     * @param boolean $withWhiteSpace
     * @access public
     */
    public function __construct($ResourcePricelistID, $pricelistID, $PricelistNameEN, $currency, $memo, $withWhiteSpace)
    {
      $this->ResourcePricelistID = $ResourcePricelistID;
      $this->pricelistID = $pricelistID;
      $this->PricelistNameEN = $PricelistNameEN;
      $this->currency = $currency;
      $this->memo = $memo;
      $this->withWhiteSpace = $withWhiteSpace;
    }

}
