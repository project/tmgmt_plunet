<?php

namespace Drupal\tmgmt_plunet\Service\DataJob30;




























class DataJob30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Result' => 'Drupal\tmgmt_plunet\Service\DataJob30\Result',
      'DateResult' => 'Drupal\tmgmt_plunet\Service\DataJob30\DateResult',
      'PricelistEntryList' => 'Drupal\tmgmt_plunet\Service\DataJob30\PricelistEntryList',
      'PricelistEntry' => 'Drupal\tmgmt_plunet\Service\DataJob30\PricelistEntry',
      'StringArrayResult' => 'Drupal\tmgmt_plunet\Service\DataJob30\StringArrayResult',
      'JobResult' => 'Drupal\tmgmt_plunet\Service\DataJob30\JobResult',
      'Job' => 'Drupal\tmgmt_plunet\Service\DataJob30\Job',
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataJob30\StringResult',
      'PriceLineListResult' => 'Drupal\tmgmt_plunet\Service\DataJob30\PriceLineListResult',
      'PriceLine' => 'Drupal\tmgmt_plunet\Service\DataJob30\PriceLine',
      'JobIN' => 'Drupal\tmgmt_plunet\Service\DataJob30\JobIN',
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\DataJob30\IntegerResult',
      'PricelistResult' => 'Drupal\tmgmt_plunet\Service\DataJob30\PricelistResult',
      'Pricelist' => 'Drupal\tmgmt_plunet\Service\DataJob30\Pricelist',
      'JobTrackingTimeIN' => 'Drupal\tmgmt_plunet\Service\DataJob30\JobTrackingTimeIN',
      'JobTrackingTimeResult' => 'Drupal\tmgmt_plunet\Service\DataJob30\JobTrackingTimeResult',
      'TrackingTimeList' => 'Drupal\tmgmt_plunet\Service\DataJob30\TrackingTimeList',
      'JobTrackingTime' => 'Drupal\tmgmt_plunet\Service\DataJob30\JobTrackingTime',
      'JobListResult' => 'Drupal\tmgmt_plunet\Service\DataJob30\JobListResult',
      'JobTrackingTimeListIN' => 'Drupal\tmgmt_plunet\Service\DataJob30\JobTrackingTimeListIN',
      'PriceUnitResult' => 'Drupal\tmgmt_plunet\Service\DataJob30\PriceUnitResult',
      'PriceUnit' => 'Drupal\tmgmt_plunet\Service\DataJob30\PriceUnit',
      'PricelistListResult' => 'Drupal\tmgmt_plunet\Service\DataJob30\PricelistListResult',
      'PriceLineIN' => 'Drupal\tmgmt_plunet\Service\DataJob30\PriceLineIN',
      'PriceLineResult' => 'Drupal\tmgmt_plunet\Service\DataJob30\PriceLineResult',
      'PriceUnitListResult' => 'Drupal\tmgmt_plunet\Service\DataJob30\PriceUnitListResult');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataJob30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param JobIN $JobIN
     * @param boolean $enableNullOrEmptyValues
     * @access public
     * @return Result
     */
    public function update($UUID, JobIN $JobIN, $enableNullOrEmptyValues)
    {
      return $this->__soapCall('update', array($UUID, $JobIN, $enableNullOrEmptyValues));
    }

    /**
     * @param string $UUID
     * @param int $projectID
     * @param int $projectType
     * @param string $jobTypeAbbrevation
     * @access public
     * @return IntegerResult
     */
    public function insert($UUID, $projectID, $projectType, $jobTypeAbbrevation)
    {
      return $this->__soapCall('insert', array($UUID, $projectID, $projectType, $jobTypeAbbrevation));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @access public
     * @return StringResult
     */
    public function getComment($UUID, $projectType, $jobID)
    {
      return $this->__soapCall('getComment', array($UUID, $projectType, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @param string $comment
     * @access public
     * @return Result
     */
    public function setComment($UUID, $projectType, $jobID, $comment)
    {
      return $this->__soapCall('setComment', array($UUID, $projectType, $jobID, $comment));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @access public
     * @return StringResult
     */
    public function getCurrency($UUID, $jobID, $projectType)
    {
      return $this->__soapCall('getCurrency', array($UUID, $jobID, $projectType));
    }

    /**
     * @param string $UUID
     * @param int $projectID
     * @param int $projectType
     * @param string $jobTypeAbbrevation
     * @param int $itemID
     * @access public
     * @return IntegerResult
     */
    public function insert2($UUID, $projectID, $projectType, $jobTypeAbbrevation, $itemID)
    {
      return $this->__soapCall('insert2', array($UUID, $projectID, $projectType, $jobTypeAbbrevation, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $resourceID
     * @param int $jobID
     * @access public
     * @return Result
     */
    public function setResourceID($UUID, $projectType, $resourceID, $jobID)
    {
      return $this->__soapCall('setResourceID', array($UUID, $projectType, $resourceID, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @param int $jobID
     * @access public
     * @return Result
     */
    public function setItemID($UUID, $projectType, $itemID, $jobID)
    {
      return $this->__soapCall('setItemID', array($UUID, $projectType, $itemID, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @access public
     * @return DateResult
     */
    public function getDeliveryDate($UUID, $projectType, $jobID)
    {
      return $this->__soapCall('getDeliveryDate', array($UUID, $projectType, $jobID));
    }

    /**
     * @param string $UUID
     * @param string $pathOrUrl
     * @param boolean $overwriteExistingPriceLines
     * @param int $catType
     * @param int $projectType
     * @param boolean $analyzeAndCopyResultToJob
     * @param int $jobID
     * @access public
     * @return Result
     */
    public function setCatReport($UUID, $pathOrUrl, $overwriteExistingPriceLines, $catType, $projectType, $analyzeAndCopyResultToJob, $jobID)
    {
      return $this->__soapCall('setCatReport', array($UUID, $pathOrUrl, $overwriteExistingPriceLines, $catType, $projectType, $analyzeAndCopyResultToJob, $jobID));
    }

    /**
     * @param string $UUID
     * @param base64Binary $FileByteStream
     * @param string $FilePathName
     * @param int $Filesize
     * @param int $catType
     * @param int $projectType
     * @param boolean $analyzeAndCopyResultToJob
     * @param int $jobID
     * @access public
     * @return Result
     */
    public function setCatReport2($UUID, $FileByteStream, $FilePathName, $Filesize, $catType, $projectType, $analyzeAndCopyResultToJob, $jobID)
    {
      return $this->__soapCall('setCatReport2', array($UUID, $FileByteStream, $FilePathName, $Filesize, $catType, $projectType, $analyzeAndCopyResultToJob, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @param int $priceLineID
     * @access public
     * @return Result
     */
    public function deletePriceLine($UUID, $jobID, $projectType, $priceLineID)
    {
      return $this->__soapCall('deletePriceLine', array($UUID, $jobID, $projectType, $priceLineID));
    }

    /**
     * @param string $UUID
     * @param int $PriceUnitID
     * @param string $languageCode
     * @access public
     * @return PriceUnitResult
     */
    public function getPriceUnit($UUID, $PriceUnitID, $languageCode)
    {
      return $this->__soapCall('getPriceUnit', array($UUID, $PriceUnitID, $languageCode));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @access public
     * @return PricelistResult
     */
    public function getPricelist($UUID, $jobID, $projectType)
    {
      return $this->__soapCall('getPricelist', array($UUID, $jobID, $projectType));
    }

    /**
     * @param string $UUID
     * @param string $languageCode
     * @access public
     * @return StringArrayResult
     */
    public function getServices_List($UUID, $languageCode)
    {
      return $this->__soapCall('getServices_List', array($UUID, $languageCode));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @param PriceLineIN $priceLineIN
     * @access public
     * @return PriceLineResult
     */
    public function updatePriceLine($UUID, $jobID, $projectType, PriceLineIN $priceLineIN)
    {
      return $this->__soapCall('updatePriceLine', array($UUID, $jobID, $projectType, $priceLineIN));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @param PriceLineIN $priceLineIN
     * @param boolean $createAsFirstItem
     * @access public
     * @return PriceLineResult
     */
    public function insertPriceLine($UUID, $jobID, $projectType, PriceLineIN $priceLineIN, $createAsFirstItem)
    {
      return $this->__soapCall('insertPriceLine', array($UUID, $jobID, $projectType, $priceLineIN, $createAsFirstItem));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @param int $priceListID
     * @access public
     * @return Result
     */
    public function setPricelist($UUID, $jobID, $projectType, $priceListID)
    {
      return $this->__soapCall('setPricelist', array($UUID, $jobID, $projectType, $priceListID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @param int $userID
     * @param int $actionLinkType
     * @access public
     * @return StringResult
     */
    public function getActionLink($UUID, $projectType, $jobID, $userID, $actionLinkType)
    {
      return $this->__soapCall('getActionLink', array($UUID, $projectType, $jobID, $userID, $actionLinkType));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @access public
     * @return DateResult
     */
    public function getDueDate($UUID, $projectType, $jobID)
    {
      return $this->__soapCall('getDueDate', array($UUID, $projectType, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param dateTime $startDate
     * @param int $jobID
     * @access public
     * @return Result
     */
    public function setStartDate($UUID, $projectType, $startDate, $jobID)
    {
      return $this->__soapCall('setStartDate', array($UUID, $projectType, $startDate, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @access public
     * @return StringResult
     */
    public function getJobNumber($UUID, $projectType, $jobID)
    {
      return $this->__soapCall('getJobNumber', array($UUID, $projectType, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @access public
     * @return StringResult
     */
    public function getDeliveryNote($UUID, $projectType, $jobID)
    {
      return $this->__soapCall('getDeliveryNote', array($UUID, $projectType, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @access public
     * @return IntegerResult
     */
    public function getPayableID($UUID, $jobID, $projectType)
    {
      return $this->__soapCall('getPayableID', array($UUID, $jobID, $projectType));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @param int $status
     * @access public
     * @return Result
     */
    public function setJobStatus($UUID, $projectType, $jobID, $status)
    {
      return $this->__soapCall('setJobStatus', array($UUID, $projectType, $jobID, $status));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function assignJob($UUID, $projectType, $jobID, $resourceID)
    {
      return $this->__soapCall('assignJob', array($UUID, $projectType, $jobID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @access public
     * @return Result
     */
    public function deleteJob($UUID, $jobID, $projectType)
    {
      return $this->__soapCall('deleteJob', array($UUID, $jobID, $projectType));
    }

    /**
     * @param string $UUID
     * @param JobIN $JobIN
     * @param string $JobTypeShort
     * @access public
     * @return IntegerResult
     */
    public function insert3($UUID, JobIN $JobIN, $JobTypeShort)
    {
      return $this->__soapCall('insert3', array($UUID, $JobIN, $JobTypeShort));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $priceListID
     * @param int $jobID
     * @access public
     * @return Result
     */
    public function setPriceListeID($UUID, $projectType, $priceListID, $jobID)
    {
      return $this->__soapCall('setPriceListeID', array($UUID, $projectType, $priceListID, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @param string $note
     * @access public
     * @return Result
     */
    public function setDeliveryNote($UUID, $projectType, $jobID, $note)
    {
      return $this->__soapCall('setDeliveryNote', array($UUID, $projectType, $jobID, $note));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @access public
     * @return JobResult
     */
    public function getJob_ForView($UUID, $jobID, $projectType)
    {
      return $this->__soapCall('getJob_ForView', array($UUID, $jobID, $projectType));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param dateTime $dueDate
     * @param int $jobID
     * @access public
     * @return Result
     */
    public function setDueDate($UUID, $projectType, $dueDate, $jobID)
    {
      return $this->__soapCall('setDueDate', array($UUID, $projectType, $dueDate, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @access public
     * @return IntegerResult
     */
    public function getResourceID($UUID, $projectType, $jobID)
    {
      return $this->__soapCall('getResourceID', array($UUID, $projectType, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @access public
     * @return DateResult
     */
    public function getCreationDate($UUID, $projectType, $jobID)
    {
      return $this->__soapCall('getCreationDate', array($UUID, $projectType, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @param string $description
     * @access public
     * @return Result
     */
    public function setDescription($UUID, $projectType, $jobID, $description)
    {
      return $this->__soapCall('setDescription', array($UUID, $projectType, $jobID, $description));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @access public
     * @return StringResult
     */
    public function getDescription($UUID, $projectType, $jobID)
    {
      return $this->__soapCall('getDescription', array($UUID, $projectType, $jobID));
    }

    /**
     * @param string $UUID
     * @param string $ServerAuthenticationString
     * @param string $ServerAddress
     * @param int $EventType
     * @access public
     * @return Result
     */
    public function registerCallback_Notify($UUID, $ServerAuthenticationString, $ServerAddress, $EventType)
    {
      return $this->__soapCall('registerCallback_Notify', array($UUID, $ServerAuthenticationString, $ServerAddress, $EventType));
    }

    /**
     * @param string $UUID
     * @param string $ServerAuthenticationString
     * @param string $ServerAddress
     * @param int $JobID
     * @param int $ProjectType
     * @access public
     * @return Result
     */
    public function registerCallback_Observer($UUID, $ServerAuthenticationString, $ServerAddress, $JobID, $ProjectType)
    {
      return $this->__soapCall('registerCallback_Observer', array($UUID, $ServerAuthenticationString, $ServerAddress, $JobID, $ProjectType));
    }

    /**
     * @param string $UUID
     * @param int $EventType
     * @access public
     * @return Result
     */
    public function deregisterCallback_Notify($UUID, $EventType)
    {
      return $this->__soapCall('deregisterCallback_Notify', array($UUID, $EventType));
    }

    /**
     * @param string $UUID
     * @param int $JobID
     * @param int $ProjectType
     * @access public
     * @return Result
     */
    public function deregisterCallback_Observer($UUID, $JobID, $ProjectType)
    {
      return $this->__soapCall('deregisterCallback_Observer', array($UUID, $JobID, $ProjectType));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @access public
     * @return PricelistListResult
     */
    public function getPricelist_List($UUID, $jobID, $projectType)
    {
      return $this->__soapCall('getPricelist_List', array($UUID, $jobID, $projectType));
    }

    /**
     * @param string $UUID
     * @param int $PricelistID
     * @param string $SourceLanguage
     * @param string $TargetLanguage
     * @access public
     * @return PricelistEntryList
     */
    public function getPricelistEntry_List($UUID, $PricelistID, $SourceLanguage, $TargetLanguage)
    {
      return $this->__soapCall('getPricelistEntry_List', array($UUID, $PricelistID, $SourceLanguage, $TargetLanguage));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @access public
     * @return PriceLineListResult
     */
    public function getPriceLine_List($UUID, $jobID, $projectType)
    {
      return $this->__soapCall('getPriceLine_List', array($UUID, $jobID, $projectType));
    }

    /**
     * @param string $UUID
     * @param string $languageCode
     * @param string $service
     * @access public
     * @return PriceUnitListResult
     */
    public function getPriceUnit_List($UUID, $languageCode, $service)
    {
      return $this->__soapCall('getPriceUnit_List', array($UUID, $languageCode, $service));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @access public
     * @return IntegerResult
     */
    public function getContactPersonID($UUID, $projectType, $jobID)
    {
      return $this->__soapCall('getContactPersonID', array($UUID, $projectType, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @access public
     * @return StringResult
     */
    public function getJobType_LongName($UUID, $projectType, $jobID)
    {
      return $this->__soapCall('getJobType_LongName', array($UUID, $projectType, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @param JobTrackingTimeListIN $JobTrackingTimeListIN
     * @access public
     * @return Result
     */
    public function addJobTrackingTimesList($UUID, $jobID, $projectType, JobTrackingTimeListIN $JobTrackingTimeListIN)
    {
      return $this->__soapCall('addJobTrackingTimesList', array($UUID, $jobID, $projectType, $JobTrackingTimeListIN));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @access public
     * @return IntegerResult
     */
    public function getResourceContactPersonID($UUID, $projectType, $jobID)
    {
      return $this->__soapCall('getResourceContactPersonID', array($UUID, $projectType, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @param JobTrackingTimeIN $JobTrackingTimeIN
     * @access public
     * @return Result
     */
    public function addJobTrackingTime($UUID, $jobID, $projectType, JobTrackingTimeIN $JobTrackingTimeIN)
    {
      return $this->__soapCall('addJobTrackingTime', array($UUID, $jobID, $projectType, $JobTrackingTimeIN));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @access public
     * @return JobTrackingTimeResult
     */
    public function getJobTrackingTimesList($UUID, $jobID, $projectType)
    {
      return $this->__soapCall('getJobTrackingTimesList', array($UUID, $jobID, $projectType));
    }

    /**
     * @param string $UUID
     * @param int $itemID
     * @param int $projectType
     * @access public
     * @return JobListResult
     */
    public function getJobListOfItem_ForView($UUID, $itemID, $projectType)
    {
      return $this->__soapCall('getJobListOfItem_ForView', array($UUID, $itemID, $projectType));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @access public
     * @return StringResult
     */
    public function getJobType_ShortName($UUID, $projectType, $jobID)
    {
      return $this->__soapCall('getJobType_ShortName', array($UUID, $projectType, $jobID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @param int $resourceID
     * @access public
     * @return Result
     */
    public function setContactPersonID($UUID, $projectType, $jobID, $resourceID)
    {
      return $this->__soapCall('setContactPersonID', array($UUID, $projectType, $jobID, $resourceID));
    }

    /**
     * @param string $UUID
     * @param int $jobID
     * @param int $projectType
     * @param int $currencyType
     * @access public
     * @return PriceLineListResult
     */
    public function getPriceLine_ListByCurrencyType($UUID, $jobID, $projectType, $currencyType)
    {
      return $this->__soapCall('getPriceLine_ListByCurrencyType', array($UUID, $jobID, $projectType, $currencyType));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $jobID
     * @param int $contactID
     * @access public
     * @return Result
     */
    public function setResourceContactPersonID($UUID, $projectType, $jobID, $contactID)
    {
      return $this->__soapCall('setResourceContactPersonID', array($UUID, $projectType, $jobID, $contactID));
    }

    /**
     * @param string $UUID
     * @param string $targetFileName
     * @param int $projectType
     * @param int $jobID
     * @access public
     * @return StringResult
     */
    public function getDownloadUrl_SourceData($UUID, $targetFileName, $projectType, $jobID)
    {
      return $this->__soapCall('getDownloadUrl_SourceData', array($UUID, $targetFileName, $projectType, $jobID));
    }

    /**
     * @param string $UUID
     * @param string $jobIDs
     * @param int $projectType
     * @access public
     * @return JobListResult
     */
    public function getJobList_ForView($UUID, $jobIDs, $projectType)
    {
      return $this->__soapCall('getJobList_ForView', array($UUID, $jobIDs, $projectType));
    }

}
