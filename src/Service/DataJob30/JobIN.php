<?php

namespace Drupal\tmgmt_plunet\Service\DataJob30;

class JobIN
{

    /**
     * @var int $contactPersonID
     * @access public
     */
    public $contactPersonID = null;

    /**
     * @var dateTime $dueDate
     * @access public
     */
    public $dueDate = null;

    /**
     * @var int $itemID
     * @access public
     */
    public $itemID = null;

    /**
     * @var int $jobID
     * @access public
     */
    public $jobID = null;

    /**
     * @var int $projectID
     * @access public
     */
    public $projectID = null;

    /**
     * @var int $projectType
     * @access public
     */
    public $projectType = null;

    /**
     * @var int $resourceID
     * @access public
     */
    public $resourceID = null;

    /**
     * @var dateTime $startDate
     * @access public
     */
    public $startDate = null;

    /**
     * @var int $status
     * @access public
     */
    public $status = null;

    /**
     * @param int $contactPersonID
     * @param dateTime $dueDate
     * @param int $itemID
     * @param int $jobID
     * @param int $projectID
     * @param int $projectType
     * @param int $resourceID
     * @param dateTime $startDate
     * @param int $status
     * @access public
     */
    public function __construct($contactPersonID, $dueDate, $itemID, $jobID, $projectID, $projectType, $resourceID, $startDate, $status)
    {
      $this->contactPersonID = $contactPersonID;
      $this->dueDate = $dueDate;
      $this->itemID = $itemID;
      $this->jobID = $jobID;
      $this->projectID = $projectID;
      $this->projectType = $projectType;
      $this->resourceID = $resourceID;
      $this->startDate = $startDate;
      $this->status = $status;
    }

}
