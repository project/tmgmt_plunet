<?php

namespace Drupal\tmgmt_plunet\Service\DataJob30;

class TrackingTimeList
{

    /**
     * @var float $completed
     * @access public
     */
    public $completed = null;

    /**
     * @var JobTrackingTime[] $trackingTimeList
     * @access public
     */
    public $trackingTimeList = null;

    /**
     * @param float $completed
     * @access public
     */
    public function __construct($completed)
    {
      $this->completed = $completed;
    }

}
