<?php

namespace Drupal\tmgmt_plunet\Service\DataJob30;

class Job
{

    /**
     * @var int $countSourceFiles
     * @access public
     */
    public $countSourceFiles = null;

    /**
     * @var dateTime $dueDate
     * @access public
     */
    public $dueDate = null;

    /**
     * @var int $itemID
     * @access public
     */
    public $itemID = null;

    /**
     * @var int $jobID
     * @access public
     */
    public $jobID = null;

    /**
     * @var string $jobTypeFull
     * @access public
     */
    public $jobTypeFull = null;

    /**
     * @var string $jobTypeShort
     * @access public
     */
    public $jobTypeShort = null;

    /**
     * @var int $projectID
     * @access public
     */
    public $projectID = null;

    /**
     * @var int $projectType
     * @access public
     */
    public $projectType = null;

    /**
     * @var int $resourceID
     * @access public
     */
    public $resourceID = null;

    /**
     * @var dateTime $startDate
     * @access public
     */
    public $startDate = null;

    /**
     * @var int $status
     * @access public
     */
    public $status = null;

    /**
     * @param int $countSourceFiles
     * @param dateTime $dueDate
     * @param int $itemID
     * @param int $jobID
     * @param string $jobTypeFull
     * @param string $jobTypeShort
     * @param int $projectID
     * @param int $projectType
     * @param int $resourceID
     * @param dateTime $startDate
     * @param int $status
     * @access public
     */
    public function __construct($countSourceFiles, $dueDate, $itemID, $jobID, $jobTypeFull, $jobTypeShort, $projectID, $projectType, $resourceID, $startDate, $status)
    {
      $this->countSourceFiles = $countSourceFiles;
      $this->dueDate = $dueDate;
      $this->itemID = $itemID;
      $this->jobID = $jobID;
      $this->jobTypeFull = $jobTypeFull;
      $this->jobTypeShort = $jobTypeShort;
      $this->projectID = $projectID;
      $this->projectType = $projectType;
      $this->resourceID = $resourceID;
      $this->startDate = $startDate;
      $this->status = $status;
    }

}
