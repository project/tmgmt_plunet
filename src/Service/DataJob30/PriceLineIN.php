<?php

namespace Drupal\tmgmt_plunet\Service\DataJob30;

class PriceLineIN
{

    /**
     * @var float $amount
     * @access public
     */
    public $amount = null;

    /**
     * @var float $amount_perUnit
     * @access public
     */
    public $amount_perUnit = null;

    /**
     * @var string $memo
     * @access public
     */
    public $memo = null;

    /**
     * @var int $priceLineID
     * @access public
     */
    public $priceLineID = null;

    /**
     * @var int $priceUnitID
     * @access public
     */
    public $priceUnitID = null;

    /**
     * @var int $taxType
     * @access public
     */
    public $taxType = null;

    /**
     * @var float $time_perUnit
     * @access public
     */
    public $time_perUnit = null;

    /**
     * @var float $unit_price
     * @access public
     */
    public $unit_price = null;

    /**
     * @param float $amount
     * @param float $amount_perUnit
     * @param string $memo
     * @param int $priceLineID
     * @param int $priceUnitID
     * @param int $taxType
     * @param float $time_perUnit
     * @param float $unit_price
     * @access public
     */
    public function __construct($amount, $amount_perUnit, $memo, $priceLineID, $priceUnitID, $taxType, $time_perUnit, $unit_price)
    {
      $this->amount = $amount;
      $this->amount_perUnit = $amount_perUnit;
      $this->memo = $memo;
      $this->priceLineID = $priceLineID;
      $this->priceUnitID = $priceUnitID;
      $this->taxType = $taxType;
      $this->time_perUnit = $time_perUnit;
      $this->unit_price = $unit_price;
    }

}
