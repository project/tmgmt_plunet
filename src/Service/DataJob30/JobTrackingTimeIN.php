<?php

namespace Drupal\tmgmt_plunet\Service\DataJob30;

class JobTrackingTimeIN
{

    /**
     * @var string $comment
     * @access public
     */
    public $comment = null;

    /**
     * @var float $completed
     * @access public
     */
    public $completed = null;

    /**
     * @var dateTime $dateFrom
     * @access public
     */
    public $dateFrom = null;

    /**
     * @var dateTime $dateTo
     * @access public
     */
    public $dateTo = null;

    /**
     * @var int $resourceID
     * @access public
     */
    public $resourceID = null;

    /**
     * @param string $comment
     * @param float $completed
     * @param dateTime $dateFrom
     * @param dateTime $dateTo
     * @param int $resourceID
     * @access public
     */
    public function __construct($comment, $completed, $dateFrom, $dateTo, $resourceID)
    {
      $this->comment = $comment;
      $this->completed = $completed;
      $this->dateFrom = $dateFrom;
      $this->dateTo = $dateTo;
      $this->resourceID = $resourceID;
    }

}
