<?php

namespace Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30;

class InvoiceItem
{

    /**
     * @var string $briefDescription
     * @access public
     */
    public $briefDescription = null;

    /**
     * @var string $comment
     * @access public
     */
    public $comment = null;

    /**
     * @var int $invoiceID
     * @access public
     */
    public $invoiceID = null;

    /**
     * @var int $invoiceItemID
     * @access public
     */
    public $invoiceItemID = null;

    /**
     * @var string $itemNumber
     * @access public
     */
    public $itemNumber = null;

    /**
     * @var int $languageCombinationID
     * @access public
     */
    public $languageCombinationID = null;

    /**
     * @var int $orderID
     * @access public
     */
    public $orderID = null;

    /**
     * @var float $totalPrice
     * @access public
     */
    public $totalPrice = null;

    /**
     * @param string $briefDescription
     * @param string $comment
     * @param int $invoiceID
     * @param int $invoiceItemID
     * @param string $itemNumber
     * @param int $languageCombinationID
     * @param int $orderID
     * @param float $totalPrice
     * @access public
     */
    public function __construct($briefDescription, $comment, $invoiceID, $invoiceItemID, $itemNumber, $languageCombinationID, $orderID, $totalPrice)
    {
      $this->briefDescription = $briefDescription;
      $this->comment = $comment;
      $this->invoiceID = $invoiceID;
      $this->invoiceItemID = $invoiceItemID;
      $this->itemNumber = $itemNumber;
      $this->languageCombinationID = $languageCombinationID;
      $this->orderID = $orderID;
      $this->totalPrice = $totalPrice;
    }

}
