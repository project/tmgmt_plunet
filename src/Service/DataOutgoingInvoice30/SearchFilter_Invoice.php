<?php

namespace Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30;

class SearchFilter_Invoice
{

    /**
     * @var int $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var string $languageCode
     * @access public
     */
    public $languageCode = null;

    /**
     * @var Property[] $propertiesList
     * @access public
     */
    public $propertiesList = null;

    /**
     * @var int $invoiceStatus
     * @access public
     */
    public $invoiceStatus = null;

    /**
     * @var Textmodule[] $textmodulesList
     * @access public
     */
    public $textmodulesList = null;

    /**
     * @var SelectionEntry_TimeFrame $timeFrame
     * @access public
     */
    public $timeFrame = null;

    /**
     * @var boolean $withCreditNotes
     * @access public
     */
    public $withCreditNotes = null;

    /**
     * @param int $customerID
     * @param string $languageCode
     * @param Property[] $propertiesList
     * @param int $invoiceStatus
     * @param Textmodule[] $textmodulesList
     * @param SelectionEntry_TimeFrame $timeFrame
     * @param boolean $withCreditNotes
     * @access public
     */
    public function __construct($customerID, $languageCode, $propertiesList, $invoiceStatus, $textmodulesList, $timeFrame, $withCreditNotes)
    {
      $this->customerID = $customerID;
      $this->languageCode = $languageCode;
      $this->propertiesList = $propertiesList;
      $this->invoiceStatus = $invoiceStatus;
      $this->textmodulesList = $textmodulesList;
      $this->timeFrame = $timeFrame;
      $this->withCreditNotes = $withCreditNotes;
    }

}
