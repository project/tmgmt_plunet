<?php

namespace Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30;



class PriceUnitListResult extends Result
{

    /**
     * @var PriceUnit[] $data
     * @access public
     */
    public $data = null;

    /**
     * @param int $statusCode
     * @param string $statusCodeAlphanumeric
     * @param string $statusMessage
     * @access public
     */
    public function __construct($statusCode, $statusCodeAlphanumeric, $statusMessage)
    {
      parent::__construct($statusCode, $statusCodeAlphanumeric, $statusMessage);
    }

}
