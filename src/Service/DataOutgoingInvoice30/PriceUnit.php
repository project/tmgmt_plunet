<?php

namespace Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30;

class PriceUnit
{

    /**
     * @var string $articleNumber
     * @access public
     */
    public $articleNumber = null;

    /**
     * @var string $description
     * @access public
     */
    public $description = null;

    /**
     * @var string $memo
     * @access public
     */
    public $memo = null;

    /**
     * @var int $priceUnitID
     * @access public
     */
    public $priceUnitID = null;

    /**
     * @var string $service
     * @access public
     */
    public $service = null;

    /**
     * @param string $articleNumber
     * @param string $description
     * @param string $memo
     * @param int $priceUnitID
     * @param string $service
     * @access public
     */
    public function __construct($articleNumber, $description, $memo, $priceUnitID, $service)
    {
      $this->articleNumber = $articleNumber;
      $this->description = $description;
      $this->memo = $memo;
      $this->priceUnitID = $priceUnitID;
      $this->service = $service;
    }

}
