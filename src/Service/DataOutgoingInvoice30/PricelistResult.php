<?php

namespace Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30;



class PricelistResult extends Result
{

    /**
     * @var Pricelist $data
     * @access public
     */
    public $data = null;

    /**
     * @param int $statusCode
     * @param string $statusCodeAlphanumeric
     * @param string $statusMessage
     * @param Pricelist $data
     * @access public
     */
    public function __construct($statusCode, $statusCodeAlphanumeric, $statusMessage, $data)
    {
      parent::__construct($statusCode, $statusCodeAlphanumeric, $statusMessage);
      $this->data = $data;
    }

}
