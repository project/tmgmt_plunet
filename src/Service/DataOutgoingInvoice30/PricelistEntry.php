<?php

namespace Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30;

class PricelistEntry
{

    /**
     * @var float $amountPerUnit
     * @access public
     */
    public $amountPerUnit = null;

    /**
     * @var float $pricePerUnit
     * @access public
     */
    public $pricePerUnit = null;

    /**
     * @var int $priceUnitID
     * @access public
     */
    public $priceUnitID = null;

    /**
     * @param float $amountPerUnit
     * @param float $pricePerUnit
     * @param int $priceUnitID
     * @access public
     */
    public function __construct($amountPerUnit, $pricePerUnit, $priceUnitID)
    {
      $this->amountPerUnit = $amountPerUnit;
      $this->pricePerUnit = $pricePerUnit;
      $this->priceUnitID = $priceUnitID;
    }

}
