<?php

namespace Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30;

































class DataOutgoingInvoice30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\IntegerResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\Result',
      'PriceUnitResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\PriceUnitResult',
      'PriceUnit' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\PriceUnit',
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\StringResult',
      'InvoiceResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\InvoiceResult',
      'Invoice' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\Invoice',
      'BooleanResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\BooleanResult',
      'DateResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\DateResult',
      'DoubleResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\DoubleResult',
      'TaxListResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\TaxListResult',
      'Tax' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\Tax',
      'InvoiceItemIN' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\InvoiceItemIN',
      'InvoiceItemResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\InvoiceItemResult',
      'InvoiceItem' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\InvoiceItem',
      'PriceUnitListResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\PriceUnitListResult',
      'PriceLineIN' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\PriceLineIN',
      'PriceLineResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\PriceLineResult',
      'PriceLine' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\PriceLine',
      'PricelistListResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\PricelistListResult',
      'Pricelist' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\Pricelist',
      'IntegerArrayResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\IntegerArrayResult',
      'PriceLineListResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\PriceLineListResult',
      'SearchFilter_Invoice' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\SearchFilter_Invoice',
      'Property' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\Property',
      'Textmodule' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\Textmodule',
      'SelectionEntry_TimeFrame' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\SelectionEntry_TimeFrame',
      'StringArrayResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\StringArrayResult',
      'PricelistEntryList' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\PricelistEntryList',
      'PricelistEntry' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\PricelistEntry',
      'PricelistResult' => 'Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30\PricelistResult');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataOutgoingInvoice30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return Result
     */
    public function delete($UUID, $invoiceID)
    {
      return $this->__soapCall('delete', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param SearchFilter_Invoice $SearchFilter
     * @access public
     * @return IntegerArrayResult
     */
    public function search($UUID, SearchFilter_Invoice $SearchFilter)
    {
      return $this->__soapCall('search', array($UUID, $SearchFilter));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return StringResult
     */
    public function getCurrencyCode($UUID, $invoiceID)
    {
      return $this->__soapCall('getCurrencyCode', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @param int $addressID
     * @access public
     * @return Result
     */
    public function setAddressID($UUID, $invoiceID, $addressID)
    {
      return $this->__soapCall('setAddressID', array($UUID, $invoiceID, $addressID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @param int $invoiceID
     * @access public
     * @return Result
     */
    public function setCustomerID($UUID, $customerID, $invoiceID)
    {
      return $this->__soapCall('setCustomerID', array($UUID, $customerID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return StringResult
     */
    public function getSubject($UUID, $invoiceID)
    {
      return $this->__soapCall('getSubject', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceItemID
     * @param int $priceLineID
     * @access public
     * @return Result
     */
    public function deletePriceLine($UUID, $invoiceItemID, $priceLineID)
    {
      return $this->__soapCall('deletePriceLine', array($UUID, $invoiceItemID, $priceLineID));
    }

    /**
     * @param string $UUID
     * @param int $PriceUnitID
     * @param string $languageCode
     * @access public
     * @return PriceUnitResult
     */
    public function getPriceUnit($UUID, $PriceUnitID, $languageCode)
    {
      return $this->__soapCall('getPriceUnit', array($UUID, $PriceUnitID, $languageCode));
    }

    /**
     * @param string $UUID
     * @param int $InvoiceItemID
     * @access public
     * @return PricelistResult
     */
    public function getPricelist($UUID, $InvoiceItemID)
    {
      return $this->__soapCall('getPricelist', array($UUID, $InvoiceItemID));
    }

    /**
     * @param string $UUID
     * @param string $languageCode
     * @access public
     * @return StringArrayResult
     */
    public function getServices_List($UUID, $languageCode)
    {
      return $this->__soapCall('getServices_List', array($UUID, $languageCode));
    }

    /**
     * @param string $UUID
     * @param int $invoiceItemID
     * @param PriceLineIN $priceLineIN
     * @access public
     * @return PriceLineResult
     */
    public function updatePriceLine($UUID, $invoiceItemID, PriceLineIN $priceLineIN)
    {
      return $this->__soapCall('updatePriceLine', array($UUID, $invoiceItemID, $priceLineIN));
    }

    /**
     * @param string $UUID
     * @param int $invoiceItemID
     * @param PriceLineIN $priceLineIN
     * @param boolean $createAsFirstItem
     * @access public
     * @return PriceLineResult
     */
    public function insertPriceLine($UUID, $invoiceItemID, PriceLineIN $priceLineIN, $createAsFirstItem)
    {
      return $this->__soapCall('insertPriceLine', array($UUID, $invoiceItemID, $priceLineIN, $createAsFirstItem));
    }

    /**
     * @param string $UUID
     * @param int $InvoiceItemID
     * @param int $priceListID
     * @access public
     * @return Result
     */
    public function setPricelist($UUID, $InvoiceItemID, $priceListID)
    {
      return $this->__soapCall('setPricelist', array($UUID, $InvoiceItemID, $priceListID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return DateResult
     */
    public function getValueDate($UUID, $invoiceID)
    {
      return $this->__soapCall('getValueDate', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param dateTime $invoiceDate
     * @param int $invoiceID
     * @access public
     * @return Result
     */
    public function setInvoiceDate($UUID, $invoiceDate, $invoiceID)
    {
      return $this->__soapCall('setInvoiceDate', array($UUID, $invoiceDate, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param dateTime $valueDate
     * @param int $invoiceID
     * @access public
     * @return Result
     */
    public function setValueDate($UUID, $valueDate, $invoiceID)
    {
      return $this->__soapCall('setValueDate', array($UUID, $valueDate, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return DoubleResult
     */
    public function getNet($UUID, $invoiceID)
    {
      return $this->__soapCall('getNet', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return DateResult
     */
    public function getInvoiceDate($UUID, $invoiceID)
    {
      return $this->__soapCall('getInvoiceDate', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return IntegerArrayResult
     */
    public function getOrderIDs($UUID, $invoiceID)
    {
      return $this->__soapCall('getOrderIDs', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return StringResult
     */
    public function getInvoiceNr($UUID, $invoiceID)
    {
      return $this->__soapCall('getInvoiceNr', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return DoubleResult
     */
    public function getGross($UUID, $invoiceID)
    {
      return $this->__soapCall('getGross', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return DoubleResult
     */
    public function getTax($UUID, $invoiceID)
    {
      return $this->__soapCall('getTax', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return DoubleResult
     */
    public function getPaid($UUID, $invoiceID)
    {
      return $this->__soapCall('getPaid', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $InvoiceID
     * @access public
     * @return IntegerResult
     */
    public function getAdressID($UUID, $InvoiceID)
    {
      return $this->__soapCall('getAdressID', array($UUID, $InvoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return StringResult
     */
    public function getPONumber($UUID, $invoiceID)
    {
      return $this->__soapCall('getPONumber', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return DateResult
     */
    public function getPaidDate($UUID, $invoiceID)
    {
      return $this->__soapCall('getPaidDate', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return InvoiceResult
     */
    public function getInvoiceObject($UUID, $invoiceID)
    {
      return $this->__soapCall('getInvoiceObject', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return DoubleResult
     */
    public function getOutstanding($UUID, $invoiceID)
    {
      return $this->__soapCall('getOutstanding', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @param string $poNumber
     * @access public
     * @return Result
     */
    public function setPONumber($UUID, $invoiceID, $poNumber)
    {
      return $this->__soapCall('setPONumber', array($UUID, $invoiceID, $poNumber));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @param dateTime $paidDate
     * @access public
     * @return Result
     */
    public function setPaidDate($UUID, $invoiceID, $paidDate)
    {
      return $this->__soapCall('setPaidDate', array($UUID, $invoiceID, $paidDate));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return IntegerResult
     */
    public function getCustomerID($UUID, $invoiceID)
    {
      return $this->__soapCall('getCustomerID', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param string $subject
     * @param int $invoiceID
     * @access public
     * @return Result
     */
    public function setSubject($UUID, $subject, $invoiceID)
    {
      return $this->__soapCall('setSubject', array($UUID, $subject, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return IntegerResult
     */
    public function getStatus($UUID, $invoiceID)
    {
      return $this->__soapCall('getStatus', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $Status
     * @param int $invoiceID
     * @access public
     * @return Result
     */
    public function setStatus($UUID, $Status, $invoiceID)
    {
      return $this->__soapCall('setStatus', array($UUID, $Status, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @param int $currencyType
     * @access public
     * @return DoubleResult
     */
    public function getTaxByCurrencyType($UUID, $invoiceID, $currencyType)
    {
      return $this->__soapCall('getTaxByCurrencyType', array($UUID, $invoiceID, $currencyType));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return DateResult
     */
    public function getPaymentDueDate($UUID, $invoiceID)
    {
      return $this->__soapCall('getPaymentDueDate', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @param dateTime $dueDate
     * @access public
     * @return Result
     */
    public function setPaymentDueDate($UUID, $invoiceID, $dueDate)
    {
      return $this->__soapCall('setPaymentDueDate', array($UUID, $invoiceID, $dueDate));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @param int $currencyType
     * @access public
     * @return DoubleResult
     */
    public function getNetByCurrencyType($UUID, $invoiceID, $currencyType)
    {
      return $this->__soapCall('getNetByCurrencyType', array($UUID, $invoiceID, $currencyType));
    }

    /**
     * @param string $UUID
     * @param InvoiceItemIN $InvoiceItem
     * @param boolean $enableNullOrEmptyValues
     * @access public
     * @return Result
     */
    public function updateInvoiceItem($UUID, InvoiceItemIN $InvoiceItem, $enableNullOrEmptyValues)
    {
      return $this->__soapCall('updateInvoiceItem', array($UUID, $InvoiceItem, $enableNullOrEmptyValues));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return BooleanResult
     */
    public function getIsInvoiceExported($UUID, $invoiceID)
    {
      return $this->__soapCall('getIsInvoiceExported', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param InvoiceItemIN $InvoiceItem
     * @access public
     * @return IntegerResult
     */
    public function insertInvoiceItem($UUID, InvoiceItemIN $InvoiceItem)
    {
      return $this->__soapCall('insertInvoiceItem', array($UUID, $InvoiceItem));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @param int $currencyType
     * @access public
     * @return DoubleResult
     */
    public function getGrossByCurrencyType($UUID, $invoiceID, $currencyType)
    {
      return $this->__soapCall('getGrossByCurrencyType', array($UUID, $invoiceID, $currencyType));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @param boolean $isExported
     * @access public
     * @return Result
     */
    public function setIsInvoiceExported($UUID, $invoiceID, $isExported)
    {
      return $this->__soapCall('setIsInvoiceExported', array($UUID, $invoiceID, $isExported));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @param int $currencyType
     * @access public
     * @return DoubleResult
     */
    public function getPaidByCurrencyType($UUID, $invoiceID, $currencyType)
    {
      return $this->__soapCall('getPaidByCurrencyType', array($UUID, $invoiceID, $currencyType));
    }

    /**
     * @param string $UUID
     * @param int $InvoiceItemID
     * @access public
     * @return Result
     */
    public function deleteInvoiceItem($UUID, $InvoiceItemID)
    {
      return $this->__soapCall('deleteInvoiceItem', array($UUID, $InvoiceItemID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return InvoiceItemResult
     */
    public function getInvoiceItemList($UUID, $invoiceID)
    {
      return $this->__soapCall('getInvoiceItemList', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @param int $currencyType
     * @param int $taxtypes
     * @access public
     * @return DoubleResult
     */
    public function getTaxByTypeAndCurrencyType($UUID, $invoiceID, $currencyType, $taxtypes)
    {
      return $this->__soapCall('getTaxByTypeAndCurrencyType', array($UUID, $invoiceID, $currencyType, $taxtypes));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @param int $currencyType
     * @access public
     * @return DoubleResult
     */
    public function getOutstandingByCurrencyType($UUID, $invoiceID, $currencyType)
    {
      return $this->__soapCall('getOutstandingByCurrencyType', array($UUID, $invoiceID, $currencyType));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return TaxListResult
     */
    public function getInvoiceTaxTypes($UUID, $invoiceID)
    {
      return $this->__soapCall('getInvoiceTaxTypes', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param string $description
     * @param int $invoiceID
     * @access public
     * @return Result
     */
    public function setBriefDescription($UUID, $description, $invoiceID)
    {
      return $this->__soapCall('setBriefDescription', array($UUID, $description, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return StringResult
     */
    public function getBriefDescription($UUID, $invoiceID)
    {
      return $this->__soapCall('getBriefDescription', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceItemID
     * @access public
     * @return PricelistListResult
     */
    public function getPricelist_List($UUID, $invoiceItemID)
    {
      return $this->__soapCall('getPricelist_List', array($UUID, $invoiceItemID));
    }

    /**
     * @param string $UUID
     * @param int $PricelistID
     * @param string $SourceLanguage
     * @param string $TargetLanguage
     * @access public
     * @return PricelistEntryList
     */
    public function getPricelistEntry_List($UUID, $PricelistID, $SourceLanguage, $TargetLanguage)
    {
      return $this->__soapCall('getPricelistEntry_List', array($UUID, $PricelistID, $SourceLanguage, $TargetLanguage));
    }

    /**
     * @param string $UUID
     * @param int $invoiceItemID
     * @access public
     * @return PriceLineListResult
     */
    public function getPriceLine_List($UUID, $invoiceItemID)
    {
      return $this->__soapCall('getPriceLine_List', array($UUID, $invoiceItemID));
    }

    /**
     * @param string $UUID
     * @param string $languageCode
     * @param string $service
     * @access public
     * @return PriceUnitListResult
     */
    public function getPriceUnit_List($UUID, $languageCode, $service)
    {
      return $this->__soapCall('getPriceUnit_List', array($UUID, $languageCode, $service));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return IntegerResult
     */
    public function getContactPersonID($UUID, $invoiceID)
    {
      return $this->__soapCall('getContactPersonID', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $accountID
     * @param int $invoiceID
     * @access public
     * @return Result
     */
    public function setReceivableAccount($UUID, $accountID, $invoiceID)
    {
      return $this->__soapCall('setReceivableAccount', array($UUID, $accountID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $accountID
     * @param int $invoiceID
     * @access public
     * @return Result
     */
    public function setRevenueAccount($UUID, $accountID, $invoiceID)
    {
      return $this->__soapCall('setRevenueAccount', array($UUID, $accountID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return StringResult
     */
    public function getReceivableAccount($UUID, $invoiceID)
    {
      return $this->__soapCall('getReceivableAccount', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return StringResult
     */
    public function getRevenueAccount($UUID, $invoiceID)
    {
      return $this->__soapCall('getRevenueAccount', array($UUID, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @param int $contactPersonID
     * @access public
     * @return Result
     */
    public function setContactPersonID($UUID, $invoiceID, $contactPersonID)
    {
      return $this->__soapCall('setContactPersonID', array($UUID, $invoiceID, $contactPersonID));
    }

    /**
     * @param string $UUID
     * @param string $template
     * @param int $formatId
     * @param int $invoiceID
     * @access public
     * @return StringResult
     */
    public function createInvoiceDocument($UUID, $template, $formatId, $invoiceID)
    {
      return $this->__soapCall('createInvoiceDocument', array($UUID, $template, $formatId, $invoiceID));
    }

    /**
     * @param string $UUID
     * @param int $invoiceID
     * @access public
     * @return StringArrayResult
     */
    public function getInvoiceDocuments($UUID, $invoiceID)
    {
      return $this->__soapCall('getInvoiceDocuments', array($UUID, $invoiceID));
    }

}
