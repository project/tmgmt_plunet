<?php

namespace Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30;

class Textmodule
{

    /**
     * @var string[] $availableValues
     * @access public
     */
    public $availableValues = null;

    /**
     * @var dateTime $dateValue
     * @access public
     */
    public $dateValue = null;

    /**
     * @var string $flag
     * @access public
     */
    public $flag = null;

    /**
     * @var string $flag_MainTextModule
     * @access public
     */
    public $flag_MainTextModule = null;

    /**
     * @var string[] $selectedValues
     * @access public
     */
    public $selectedValues = null;

    /**
     * @var string $stringValue
     * @access public
     */
    public $stringValue = null;

    /**
     * @var int $textModuleType
     * @access public
     */
    public $textModuleType = null;

    /**
     * @param dateTime $dateValue
     * @param string $flag
     * @param string $flag_MainTextModule
     * @param string $stringValue
     * @param int $textModuleType
     * @access public
     */
    public function __construct($dateValue, $flag, $flag_MainTextModule, $stringValue, $textModuleType)
    {
      $this->dateValue = $dateValue;
      $this->flag = $flag;
      $this->flag_MainTextModule = $flag_MainTextModule;
      $this->stringValue = $stringValue;
      $this->textModuleType = $textModuleType;
    }

}
