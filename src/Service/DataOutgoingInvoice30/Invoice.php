<?php

namespace Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30;

class Invoice
{

    /**
     * @var string $briefDescription
     * @access public
     */
    public $briefDescription = null;

    /**
     * @var string $currencyCode
     * @access public
     */
    public $currencyCode = null;

    /**
     * @var int $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var float $gross
     * @access public
     */
    public $gross = null;

    /**
     * @var dateTime $invoiceDate
     * @access public
     */
    public $invoiceDate = null;

    /**
     * @var string[] $invoiceDocuments
     * @access public
     */
    public $invoiceDocuments = null;

    /**
     * @var int $invoiceID
     * @access public
     */
    public $invoiceID = null;

    /**
     * @var string $invoiceNr
     * @access public
     */
    public $invoiceNr = null;

    /**
     * @var float $net
     * @access public
     */
    public $net = null;

    /**
     * @var int[] $orderIDs
     * @access public
     */
    public $orderIDs = null;

    /**
     * @var float $outgoing
     * @access public
     */
    public $outgoing = null;

    /**
     * @var float $paid
     * @access public
     */
    public $paid = null;

    /**
     * @var int $status
     * @access public
     */
    public $status = null;

    /**
     * @var string $subject
     * @access public
     */
    public $subject = null;

    /**
     * @var float $tax
     * @access public
     */
    public $tax = null;

    /**
     * @var dateTime $valueDate
     * @access public
     */
    public $valueDate = null;

    /**
     * @param string $briefDescription
     * @param string $currencyCode
     * @param int $customerID
     * @param float $gross
     * @param dateTime $invoiceDate
     * @param int $invoiceID
     * @param string $invoiceNr
     * @param float $net
     * @param float $outgoing
     * @param float $paid
     * @param int $status
     * @param string $subject
     * @param float $tax
     * @param dateTime $valueDate
     * @access public
     */
    public function __construct($briefDescription, $currencyCode, $customerID, $gross, $invoiceDate, $invoiceID, $invoiceNr, $net, $outgoing, $paid, $status, $subject, $tax, $valueDate)
    {
      $this->briefDescription = $briefDescription;
      $this->currencyCode = $currencyCode;
      $this->customerID = $customerID;
      $this->gross = $gross;
      $this->invoiceDate = $invoiceDate;
      $this->invoiceID = $invoiceID;
      $this->invoiceNr = $invoiceNr;
      $this->net = $net;
      $this->outgoing = $outgoing;
      $this->paid = $paid;
      $this->status = $status;
      $this->subject = $subject;
      $this->tax = $tax;
      $this->valueDate = $valueDate;
    }

}
