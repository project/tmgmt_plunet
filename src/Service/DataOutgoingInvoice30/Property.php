<?php

namespace Drupal\tmgmt_plunet\Service\DataOutgoingInvoice30;

class Property
{

    /**
     * @var int[] $avaliablePropertyValueIDList
     * @access public
     */
    public $avaliablePropertyValueIDList = null;

    /**
     * @var string $mainPropertyNameEnglish
     * @access public
     */
    public $mainPropertyNameEnglish = null;

    /**
     * @var string $propertyNameEnglish
     * @access public
     */
    public $propertyNameEnglish = null;

    /**
     * @var int $propertyType
     * @access public
     */
    public $propertyType = null;

    /**
     * @var int $selectedPropertyValueID
     * @access public
     */
    public $selectedPropertyValueID = null;

    /**
     * @var int[] $selectedPropertyValueList
     * @access public
     */
    public $selectedPropertyValueList = null;

    /**
     * @param string $mainPropertyNameEnglish
     * @param string $propertyNameEnglish
     * @param int $propertyType
     * @param int $selectedPropertyValueID
     * @access public
     */
    public function __construct($mainPropertyNameEnglish, $propertyNameEnglish, $propertyType, $selectedPropertyValueID)
    {
      $this->mainPropertyNameEnglish = $mainPropertyNameEnglish;
      $this->propertyNameEnglish = $propertyNameEnglish;
      $this->propertyType = $propertyType;
      $this->selectedPropertyValueID = $selectedPropertyValueID;
    }

}
