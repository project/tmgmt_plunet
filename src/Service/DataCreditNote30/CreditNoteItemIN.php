<?php

namespace Drupal\tmgmt_plunet\Service\DataCreditNote30;

class CreditNoteItemIN
{

    /**
     * @var string $briefDescription
     * @access public
     */
    public $briefDescription = null;

    /**
     * @var int $creditNoteID
     * @access public
     */
    public $creditNoteID = null;

    /**
     * @var int $taxTypeID
     * @access public
     */
    public $taxTypeID = null;

    /**
     * @var float $totalPrice
     * @access public
     */
    public $totalPrice = null;

    /**
     * @param string $briefDescription
     * @param int $creditNoteID
     * @param int $taxTypeID
     * @param float $totalPrice
     * @access public
     */
    public function __construct($briefDescription, $creditNoteID, $taxTypeID, $totalPrice)
    {
      $this->briefDescription = $briefDescription;
      $this->creditNoteID = $creditNoteID;
      $this->taxTypeID = $taxTypeID;
      $this->totalPrice = $totalPrice;
    }

}
