<?php

namespace Drupal\tmgmt_plunet\Service\DataCreditNote30;
















class DataCreditNote30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\StringResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\Result',
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\IntegerResult',
      'PriceLineResult' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\PriceLineResult',
      'PriceLine' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\PriceLine',
      'DoubleResult' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\DoubleResult',
      'CreditNoteItemIN' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\CreditNoteItemIN',
      'DateResult' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\DateResult',
      'CreditNoteListResult' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\CreditNoteListResult',
      'CreditNoteItem' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\CreditNoteItem',
      'PriceLineIN' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\PriceLineIN',
      'BooleanResult' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\BooleanResult',
      'TaxListResult' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\TaxListResult',
      'Tax' => 'Drupal\tmgmt_plunet\Service\DataCreditNote30\Tax');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataCreditNote30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return StringResult
     */
    public function getCurrencyCode($UUID, $creditNoteID)
    {
      return $this->__soapCall('getCurrencyCode', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @param int $addressID
     * @access public
     * @return Result
     */
    public function setAddressID($UUID, $creditNoteID, $addressID)
    {
      return $this->__soapCall('setAddressID', array($UUID, $creditNoteID, $addressID));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @param int $creditNoteID
     * @access public
     * @return Result
     */
    public function setCustomerID($UUID, $customerID, $creditNoteID)
    {
      return $this->__soapCall('setCustomerID', array($UUID, $customerID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return IntegerResult
     */
    public function getInvoiceID($UUID, $creditNoteID)
    {
      return $this->__soapCall('getInvoiceID', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return StringResult
     */
    public function getSubject($UUID, $creditNoteID)
    {
      return $this->__soapCall('getSubject', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $priceLineID
     * @access public
     * @return Result
     */
    public function deletePriceLine($UUID, $priceLineID)
    {
      return $this->__soapCall('deletePriceLine', array($UUID, $priceLineID));
    }

    /**
     * @param string $UUID
     * @param PriceLineIN $priceLineIN
     * @access public
     * @return Result
     */
    public function updatePriceLine($UUID, PriceLineIN $priceLineIN)
    {
      return $this->__soapCall('updatePriceLine', array($UUID, $priceLineIN));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteItemID
     * @param PriceLineIN $priceLineIN
     * @access public
     * @return IntegerResult
     */
    public function insertPriceLine($UUID, $creditNoteItemID, PriceLineIN $priceLineIN)
    {
      return $this->__soapCall('insertPriceLine', array($UUID, $creditNoteItemID, $priceLineIN));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return DoubleResult
     */
    public function getNet($UUID, $creditNoteID)
    {
      return $this->__soapCall('getNet', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return DoubleResult
     */
    public function getGross($UUID, $creditNoteID)
    {
      return $this->__soapCall('getGross', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return DoubleResult
     */
    public function getTax($UUID, $creditNoteID)
    {
      return $this->__soapCall('getTax', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return IntegerResult
     */
    public function getAdressID($UUID, $creditNoteID)
    {
      return $this->__soapCall('getAdressID', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return DateResult
     */
    public function getCreditDate($UUID, $creditNoteID)
    {
      return $this->__soapCall('getCreditDate', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return StringResult
     */
    public function getCreditNoteNr($UUID, $creditNoteID)
    {
      return $this->__soapCall('getCreditNoteNr', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return BooleanResult
     */
    public function getIsExported($UUID, $creditNoteID)
    {
      return $this->__soapCall('getIsExported', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return StringResult
     */
    public function getPONumber($UUID, $creditNoteID)
    {
      return $this->__soapCall('getPONumber', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return DateResult
     */
    public function getPaidDate($UUID, $creditNoteID)
    {
      return $this->__soapCall('getPaidDate', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $priceLineID
     * @access public
     * @return PriceLineResult
     */
    public function getPriceLine($UUID, $priceLineID)
    {
      return $this->__soapCall('getPriceLine', array($UUID, $priceLineID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return TaxListResult
     */
    public function getTaxTypes($UUID, $creditNoteID)
    {
      return $this->__soapCall('getTaxTypes', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return DoubleResult
     */
    public function getOutstanding($UUID, $creditNoteID)
    {
      return $this->__soapCall('getOutstanding', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param dateTime $creditDate
     * @param int $creditNoteID
     * @access public
     * @return Result
     */
    public function setCreditDate($UUID, $creditDate, $creditNoteID)
    {
      return $this->__soapCall('setCreditDate', array($UUID, $creditDate, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @param string $poNumber
     * @access public
     * @return Result
     */
    public function setPONumber($UUID, $creditNoteID, $poNumber)
    {
      return $this->__soapCall('setPONumber', array($UUID, $creditNoteID, $poNumber));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @param boolean $isExported
     * @access public
     * @return Result
     */
    public function setIsExported($UUID, $creditNoteID, $isExported)
    {
      return $this->__soapCall('setIsExported', array($UUID, $creditNoteID, $isExported));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @param dateTime $paidDate
     * @access public
     * @return Result
     */
    public function setPaidDate($UUID, $creditNoteID, $paidDate)
    {
      return $this->__soapCall('setPaidDate', array($UUID, $creditNoteID, $paidDate));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return IntegerResult
     */
    public function getCustomerID($UUID, $creditNoteID)
    {
      return $this->__soapCall('getCustomerID', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param string $subject
     * @param int $creditNoteID
     * @access public
     * @return Result
     */
    public function setSubject($UUID, $subject, $creditNoteID)
    {
      return $this->__soapCall('setSubject', array($UUID, $subject, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return IntegerResult
     */
    public function getStatus($UUID, $creditNoteID)
    {
      return $this->__soapCall('getStatus', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $Status
     * @param int $creditNoteID
     * @access public
     * @return Result
     */
    public function setStatus($UUID, $Status, $creditNoteID)
    {
      return $this->__soapCall('setStatus', array($UUID, $Status, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @param int $currencyType
     * @access public
     * @return DoubleResult
     */
    public function getTaxByCurrencyType($UUID, $creditNoteID, $currencyType)
    {
      return $this->__soapCall('getTaxByCurrencyType', array($UUID, $creditNoteID, $currencyType));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteItemID
     * @param CreditNoteItemIN $creditNoteItemIN
     * @access public
     * @return Result
     */
    public function updateCreditNoteItem($UUID, $creditNoteItemID, CreditNoteItemIN $creditNoteItemIN)
    {
      return $this->__soapCall('updateCreditNoteItem', array($UUID, $creditNoteItemID, $creditNoteItemIN));
    }

    /**
     * @param string $UUID
     * @param CreditNoteItemIN $CreditNoteItemIN
     * @access public
     * @return IntegerResult
     */
    public function insertCreditNoteItem($UUID, CreditNoteItemIN $CreditNoteItemIN)
    {
      return $this->__soapCall('insertCreditNoteItem', array($UUID, $CreditNoteItemIN));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @param int $currencyType
     * @access public
     * @return DoubleResult
     */
    public function getNetByCurrencyType($UUID, $creditNoteID, $currencyType)
    {
      return $this->__soapCall('getNetByCurrencyType', array($UUID, $creditNoteID, $currencyType));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @param int $currencyType
     * @access public
     * @return DoubleResult
     */
    public function getGrossByCurrencyType($UUID, $creditNoteID, $currencyType)
    {
      return $this->__soapCall('getGrossByCurrencyType', array($UUID, $creditNoteID, $currencyType));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteItemID
     * @access public
     * @return Result
     */
    public function deleteCreditNoteItem($UUID, $creditNoteItemID)
    {
      return $this->__soapCall('deleteCreditNoteItem', array($UUID, $creditNoteItemID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @param int $currencyType
     * @param int $taxtypes
     * @access public
     * @return DoubleResult
     */
    public function getTaxByTypeAndCurrencyType($UUID, $creditNoteID, $currencyType, $taxtypes)
    {
      return $this->__soapCall('getTaxByTypeAndCurrencyType', array($UUID, $creditNoteID, $currencyType, $taxtypes));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @param int $currencyType
     * @access public
     * @return DoubleResult
     */
    public function getOutstandingByCurrencyType($UUID, $creditNoteID, $currencyType)
    {
      return $this->__soapCall('getOutstandingByCurrencyType', array($UUID, $creditNoteID, $currencyType));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return CreditNoteListResult
     */
    public function getCreditNoteItemList($UUID, $creditNoteID)
    {
      return $this->__soapCall('getCreditNoteItemList', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param string $description
     * @param int $creditNoteID
     * @access public
     * @return Result
     */
    public function setBriefDescription($UUID, $description, $creditNoteID)
    {
      return $this->__soapCall('setBriefDescription', array($UUID, $description, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return StringResult
     */
    public function getBriefDescription($UUID, $creditNoteID)
    {
      return $this->__soapCall('getBriefDescription', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return IntegerResult
     */
    public function getContactPersonID($UUID, $creditNoteID)
    {
      return $this->__soapCall('getContactPersonID', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $accountID
     * @param int $creditNoteID
     * @access public
     * @return Result
     */
    public function setReceivableAccount($UUID, $accountID, $creditNoteID)
    {
      return $this->__soapCall('setReceivableAccount', array($UUID, $accountID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $accountID
     * @param int $creditNoteID
     * @access public
     * @return Result
     */
    public function setRevenueAccount($UUID, $accountID, $creditNoteID)
    {
      return $this->__soapCall('setRevenueAccount', array($UUID, $accountID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return StringResult
     */
    public function getReceivableAccount($UUID, $creditNoteID)
    {
      return $this->__soapCall('getReceivableAccount', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @access public
     * @return StringResult
     */
    public function getRevenueAccount($UUID, $creditNoteID)
    {
      return $this->__soapCall('getRevenueAccount', array($UUID, $creditNoteID));
    }

    /**
     * @param string $UUID
     * @param int $creditNoteID
     * @param int $contactPersonID
     * @access public
     * @return Result
     */
    public function setContactPersonID($UUID, $creditNoteID, $contactPersonID)
    {
      return $this->__soapCall('setContactPersonID', array($UUID, $creditNoteID, $contactPersonID));
    }

}
