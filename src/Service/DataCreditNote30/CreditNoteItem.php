<?php

namespace Drupal\tmgmt_plunet\Service\DataCreditNote30;

class CreditNoteItem
{

    /**
     * @var string $briefDescription
     * @access public
     */
    public $briefDescription = null;

    /**
     * @var int $creditNoteID
     * @access public
     */
    public $creditNoteID = null;

    /**
     * @var int $creditNoteItemID
     * @access public
     */
    public $creditNoteItemID = null;

    /**
     * @var string $itemNumber
     * @access public
     */
    public $itemNumber = null;

    /**
     * @var int $taxTypeID
     * @access public
     */
    public $taxTypeID = null;

    /**
     * @var float $totalPrice
     * @access public
     */
    public $totalPrice = null;

    /**
     * @param string $briefDescription
     * @param int $creditNoteID
     * @param int $creditNoteItemID
     * @param string $itemNumber
     * @param int $taxTypeID
     * @param float $totalPrice
     * @access public
     */
    public function __construct($briefDescription, $creditNoteID, $creditNoteItemID, $itemNumber, $taxTypeID, $totalPrice)
    {
      $this->briefDescription = $briefDescription;
      $this->creditNoteID = $creditNoteID;
      $this->creditNoteItemID = $creditNoteItemID;
      $this->itemNumber = $itemNumber;
      $this->taxTypeID = $taxTypeID;
      $this->totalPrice = $totalPrice;
    }

}
