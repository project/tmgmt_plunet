<?php

namespace Drupal\tmgmt_plunet\Service\DataCreditNote30;

class Tax
{

    /**
     * @var boolean $active
     * @access public
     */
    public $active = null;

    /**
     * @var string $expenseAccount
     * @access public
     */
    public $expenseAccount = null;

    /**
     * @var string $revenueAccount
     * @access public
     */
    public $revenueAccount = null;

    /**
     * @var string $taxDescription
     * @access public
     */
    public $taxDescription = null;

    /**
     * @var float $taxRate
     * @access public
     */
    public $taxRate = null;

    /**
     * @var int $taxType
     * @access public
     */
    public $taxType = null;

    /**
     * @param boolean $active
     * @param string $expenseAccount
     * @param string $revenueAccount
     * @param string $taxDescription
     * @param float $taxRate
     * @param int $taxType
     * @access public
     */
    public function __construct($active, $expenseAccount, $revenueAccount, $taxDescription, $taxRate, $taxType)
    {
      $this->active = $active;
      $this->expenseAccount = $expenseAccount;
      $this->revenueAccount = $revenueAccount;
      $this->taxDescription = $taxDescription;
      $this->taxRate = $taxRate;
      $this->taxType = $taxType;
    }

}
