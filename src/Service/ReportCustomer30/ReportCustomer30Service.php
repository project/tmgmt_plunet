<?php

namespace Drupal\tmgmt_plunet\Service\ReportCustomer30;







class ReportCustomer30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'SearchFilter_Customer' => 'Drupal\tmgmt_plunet\Service\ReportCustomer30\SearchFilter_Customer',
      'Property' => 'Drupal\tmgmt_plunet\Service\ReportCustomer30\Property',
      'Textmodule' => 'Drupal\tmgmt_plunet\Service\ReportCustomer30\Textmodule',
      'IntegerArrayResult' => 'Drupal\tmgmt_plunet\Service\ReportCustomer30\IntegerArrayResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\ReportCustomer30\Result');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/ReportCustomer30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param SearchFilter_Customer $SearchFilter_Customer
     * @access public
     * @return IntegerArrayResult
     */
    public function search($UUID, SearchFilter_Customer $SearchFilter_Customer)
    {
      return $this->__soapCall('search', array($UUID, $SearchFilter_Customer));
    }

}
