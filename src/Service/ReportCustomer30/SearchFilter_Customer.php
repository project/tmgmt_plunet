<?php

namespace Drupal\tmgmt_plunet\Service\ReportCustomer30;

class SearchFilter_Customer
{

    /**
     * @var int $contact_resourceID
     * @access public
     */
    public $contact_resourceID = null;

    /**
     * @var int $customerType
     * @access public
     */
    public $customerType = null;

    /**
     * @var string $email
     * @access public
     */
    public $email = null;

    /**
     * @var string $languageCode
     * @access public
     */
    public $languageCode = null;

    /**
     * @var string $name1
     * @access public
     */
    public $name1 = null;

    /**
     * @var string $name2
     * @access public
     */
    public $name2 = null;

    /**
     * @var Property[] $propertiesList
     * @access public
     */
    public $propertiesList = null;

    /**
     * @var string $sourceLanguageCode
     * @access public
     */
    public $sourceLanguageCode = null;

    /**
     * @var int $customerStatus
     * @access public
     */
    public $customerStatus = null;

    /**
     * @var Textmodule[] $textmodulesList
     * @access public
     */
    public $textmodulesList = null;

    /**
     * @param int $contact_resourceID
     * @param int $customerType
     * @param string $email
     * @param string $languageCode
     * @param string $name1
     * @param string $name2
     * @param Property[] $propertiesList
     * @param string $sourceLanguageCode
     * @param int $customerStatus
     * @param Textmodule[] $textmodulesList
     * @access public
     */
    public function __construct($contact_resourceID, $customerType, $email, $languageCode, $name1, $name2, $propertiesList, $sourceLanguageCode, $customerStatus, $textmodulesList)
    {
      $this->contact_resourceID = $contact_resourceID;
      $this->customerType = $customerType;
      $this->email = $email;
      $this->languageCode = $languageCode;
      $this->name1 = $name1;
      $this->name2 = $name2;
      $this->propertiesList = $propertiesList;
      $this->sourceLanguageCode = $sourceLanguageCode;
      $this->customerStatus = $customerStatus;
      $this->textmodulesList = $textmodulesList;
    }

}
