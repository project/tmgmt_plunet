<?php

namespace Drupal\tmgmt_plunet\Service\CallbackJob30;


class CallbackJob30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Job' => 'Drupal\tmgmt_plunet\Service\CallbackJob30\Job');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/CallbackJob30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $Authenticationcode
     * @param int $JobID
     * @param int $ProjectType
     * @param int $EventType
     * @access public
     * @return void
     */
    public function ReceiveNotifyCallback($Authenticationcode, $JobID, $ProjectType, $EventType)
    {
      return $this->__soapCall('ReceiveNotifyCallback', array($Authenticationcode, $JobID, $ProjectType, $EventType));
    }

    /**
     * @param string $Authenticationcode
     * @param Job $Job
     * @param int $EventType
     * @access public
     * @return void
     */
    public function receiveObserverCallback($Authenticationcode, Job $Job, $EventType)
    {
      return $this->__soapCall('receiveObserverCallback', array($Authenticationcode, $Job, $EventType));
    }

}
