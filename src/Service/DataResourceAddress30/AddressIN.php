<?php

namespace Drupal\tmgmt_plunet\Service\DataResourceAddress30;

class AddressIN
{

    /**
     * @var int $addressID
     * @access public
     */
    public $addressID = null;

    /**
     * @var int $addressType
     * @access public
     */
    public $addressType = null;

    /**
     * @var string $city
     * @access public
     */
    public $city = null;

    /**
     * @var string $country
     * @access public
     */
    public $country = null;

    /**
     * @var string $description
     * @access public
     */
    public $description = null;

    /**
     * @var string $name1
     * @access public
     */
    public $name1 = null;

    /**
     * @var string $name2
     * @access public
     */
    public $name2 = null;

    /**
     * @var string $office
     * @access public
     */
    public $office = null;

    /**
     * @var string $state
     * @access public
     */
    public $state = null;

    /**
     * @var string $street
     * @access public
     */
    public $street = null;

    /**
     * @var string $street2
     * @access public
     */
    public $street2 = null;

    /**
     * @var string $zip
     * @access public
     */
    public $zip = null;

    /**
     * @param int $addressID
     * @param int $addressType
     * @param string $city
     * @param string $country
     * @param string $description
     * @param string $name1
     * @param string $name2
     * @param string $office
     * @param string $state
     * @param string $street
     * @param string $street2
     * @param string $zip
     * @access public
     */
    public function __construct($addressID, $addressType, $city, $country, $description, $name1, $name2, $office, $state, $street, $street2, $zip)
    {
      $this->addressID = $addressID;
      $this->addressType = $addressType;
      $this->city = $city;
      $this->country = $country;
      $this->description = $description;
      $this->name1 = $name1;
      $this->name2 = $name2;
      $this->office = $office;
      $this->state = $state;
      $this->street = $street;
      $this->street2 = $street2;
      $this->zip = $zip;
    }

}
