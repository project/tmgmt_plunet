<?php

namespace Drupal\tmgmt_plunet\Service\DataResourceContact30;

class ResourceContactIN
{

    /**
     * @var string $academicTitle
     * @access public
     */
    public $academicTitle = null;

    /**
     * @var int $addressID
     * @access public
     */
    public $addressID = null;

    /**
     * @var string $email
     * @access public
     */
    public $email = null;

    /**
     * @var string $externalID
     * @access public
     */
    public $externalID = null;

    /**
     * @var string $fax
     * @access public
     */
    public $fax = null;

    /**
     * @var int $formOfAddress
     * @access public
     */
    public $formOfAddress = null;

    /**
     * @var string $mobilePhone
     * @access public
     */
    public $mobilePhone = null;

    /**
     * @var string $name1
     * @access public
     */
    public $name1 = null;

    /**
     * @var string $name2
     * @access public
     */
    public $name2 = null;

    /**
     * @var string $opening
     * @access public
     */
    public $opening = null;

    /**
     * @var string $phone
     * @access public
     */
    public $phone = null;

    /**
     * @var int $resourceContactID
     * @access public
     */
    public $resourceContactID = null;

    /**
     * @var int $resourceID
     * @access public
     */
    public $resourceID = null;

    /**
     * @var string $skypeID
     * @access public
     */
    public $skypeID = null;

    /**
     * @var int $status
     * @access public
     */
    public $status = null;

    /**
     * @var int $userId
     * @access public
     */
    public $userId = null;

    /**
     * @param string $academicTitle
     * @param int $addressID
     * @param string $email
     * @param string $externalID
     * @param string $fax
     * @param int $formOfAddress
     * @param string $mobilePhone
     * @param string $name1
     * @param string $name2
     * @param string $opening
     * @param string $phone
     * @param int $resourceContactID
     * @param int $resourceID
     * @param string $skypeID
     * @param int $status
     * @param int $userId
     * @access public
     */
    public function __construct($academicTitle, $addressID, $email, $externalID, $fax, $formOfAddress, $mobilePhone, $name1, $name2, $opening, $phone, $resourceContactID, $resourceID, $skypeID, $status, $userId)
    {
      $this->academicTitle = $academicTitle;
      $this->addressID = $addressID;
      $this->email = $email;
      $this->externalID = $externalID;
      $this->fax = $fax;
      $this->formOfAddress = $formOfAddress;
      $this->mobilePhone = $mobilePhone;
      $this->name1 = $name1;
      $this->name2 = $name2;
      $this->opening = $opening;
      $this->phone = $phone;
      $this->resourceContactID = $resourceContactID;
      $this->resourceID = $resourceID;
      $this->skypeID = $skypeID;
      $this->status = $status;
      $this->userId = $userId;
    }

}
