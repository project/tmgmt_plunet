<?php

namespace Drupal\tmgmt_plunet\Service\DataResourceContact30;










class DataResourceContact30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataResourceContact30\StringResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\DataResourceContact30\Result',
      'ResourceContactIN' => 'Drupal\tmgmt_plunet\Service\DataResourceContact30\ResourceContactIN',
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\DataResourceContact30\IntegerResult',
      'IntegerArrayResult' => 'Drupal\tmgmt_plunet\Service\DataResourceContact30\IntegerArrayResult',
      'ResourceContactResult' => 'Drupal\tmgmt_plunet\Service\DataResourceContact30\ResourceContactResult',
      'ResourceContact' => 'Drupal\tmgmt_plunet\Service\DataResourceContact30\ResourceContact',
      'ResourceContactListResult' => 'Drupal\tmgmt_plunet\Service\DataResourceContact30\ResourceContactListResult');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataResourceContact30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param ResourceContactIN $ResourceContactIN
     * @param boolean $enableNullOrEmptyValues
     * @access public
     * @return Result
     */
    public function update($UUID, ResourceContactIN $ResourceContactIN, $enableNullOrEmptyValues)
    {
      return $this->__soapCall('update', array($UUID, $ResourceContactIN, $enableNullOrEmptyValues));
    }

    /**
     * @param string $UUID
     * @access public
     * @return IntegerResult
     */
    public function insert($UUID)
    {
      return $this->__soapCall('insert', array($UUID));
    }

    /**
     * @param string $UUID
     * @param string $SkypeID
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setSkypeID($UUID, $SkypeID, $ContactID)
    {
      return $this->__soapCall('setSkypeID', array($UUID, $SkypeID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getFax($UUID, $ContactID)
    {
      return $this->__soapCall('getFax', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getSkypeID($UUID, $ContactID)
    {
      return $this->__soapCall('getSkypeID', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $Fax
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setFax($UUID, $Fax, $ContactID)
    {
      return $this->__soapCall('setFax', array($UUID, $Fax, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $PhoneNumber
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setMobilePhone($UUID, $PhoneNumber, $ContactID)
    {
      return $this->__soapCall('setMobilePhone', array($UUID, $PhoneNumber, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getEmail($UUID, $ContactID)
    {
      return $this->__soapCall('getEmail', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $PhoneNumber
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setPhone($UUID, $PhoneNumber, $ContactID)
    {
      return $this->__soapCall('setPhone', array($UUID, $PhoneNumber, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getPhone($UUID, $ContactID)
    {
      return $this->__soapCall('getPhone', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $EMail
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setEmail($UUID, $EMail, $ContactID)
    {
      return $this->__soapCall('setEmail', array($UUID, $EMail, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getMobilePhone($UUID, $ContactID)
    {
      return $this->__soapCall('getMobilePhone', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getExternalID($UUID, $ContactID)
    {
      return $this->__soapCall('getExternalID', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $ExternalID
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setExternalID($UUID, $ExternalID, $ContactID)
    {
      return $this->__soapCall('setExternalID', array($UUID, $ExternalID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return IntegerResult
     */
    public function getFormOfAddress($UUID, $ContactID)
    {
      return $this->__soapCall('getFormOfAddress', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $AcademicTitle
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setAcademicTitle($UUID, $AcademicTitle, $ContactID)
    {
      return $this->__soapCall('setAcademicTitle', array($UUID, $AcademicTitle, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getName1($UUID, $ContactID)
    {
      return $this->__soapCall('getName1', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $Name
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setName2($UUID, $Name, $ContactID)
    {
      return $this->__soapCall('setName2', array($UUID, $Name, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return IntegerResult
     */
    public function getAddressID($UUID, $ContactID)
    {
      return $this->__soapCall('getAddressID', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getOpening($UUID, $ContactID)
    {
      return $this->__soapCall('getOpening', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param ResourceContactIN $ResourceContactIN
     * @access public
     * @return IntegerResult
     */
    public function insert2($UUID, ResourceContactIN $ResourceContactIN)
    {
      return $this->__soapCall('insert2', array($UUID, $ResourceContactIN));
    }

    /**
     * @param string $UUID
     * @param string $ExternalID
     * @access public
     * @return IntegerArrayResult
     */
    public function seekByExternalID($UUID, $ExternalID)
    {
      return $this->__soapCall('seekByExternalID', array($UUID, $ExternalID));
    }

    /**
     * @param string $UUID
     * @param string $Name
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setName1($UUID, $Name, $ContactID)
    {
      return $this->__soapCall('setName1', array($UUID, $Name, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $FormOfAddress
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setFormOfAddress($UUID, $FormOfAddress, $ContactID)
    {
      return $this->__soapCall('setFormOfAddress', array($UUID, $FormOfAddress, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getAcademicTitle($UUID, $ContactID)
    {
      return $this->__soapCall('getAcademicTitle', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return StringResult
     */
    public function getName2($UUID, $ContactID)
    {
      return $this->__soapCall('getName2', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param string $Opening
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setOpening($UUID, $Opening, $ContactID)
    {
      return $this->__soapCall('setOpening', array($UUID, $Opening, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $AddressID
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setAddressID($UUID, $AddressID, $ContactID)
    {
      return $this->__soapCall('setAddressID', array($UUID, $AddressID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ResourceID
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setResourceID($UUID, $ResourceID, $ContactID)
    {
      return $this->__soapCall('setResourceID', array($UUID, $ResourceID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ResourceID
     * @access public
     * @return IntegerArrayResult
     */
    public function getAllContacts($UUID, $ResourceID)
    {
      return $this->__soapCall('getAllContacts', array($UUID, $ResourceID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return ResourceContactResult
     */
    public function getContactObject($UUID, $ContactID)
    {
      return $this->__soapCall('getContactObject', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return IntegerResult
     */
    public function getResourceID($UUID, $ContactID)
    {
      return $this->__soapCall('getResourceID', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ContactID
     * @access public
     * @return IntegerResult
     */
    public function getStatus($UUID, $ContactID)
    {
      return $this->__soapCall('getStatus', array($UUID, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $Status
     * @param int $ContactID
     * @access public
     * @return Result
     */
    public function setStatus($UUID, $Status, $ContactID)
    {
      return $this->__soapCall('setStatus', array($UUID, $Status, $ContactID));
    }

    /**
     * @param string $UUID
     * @param int $ResourceID
     * @access public
     * @return ResourceContactListResult
     */
    public function getAllContactObjects($UUID, $ResourceID)
    {
      return $this->__soapCall('getAllContactObjects', array($UUID, $ResourceID));
    }

}
