<?php

namespace Drupal\tmgmt_plunet\Service\DataResourceContact30;



class ResourceContactResult extends Result
{

    /**
     * @var ResourceContact $data
     * @access public
     */
    public $data = null;

    /**
     * @param int $statusCode
     * @param string $statusCodeAlphanumeric
     * @param string $statusMessage
     * @param ResourceContact $data
     * @access public
     */
    public function __construct($statusCode, $statusCodeAlphanumeric, $statusMessage, $data)
    {
      parent::__construct($statusCode, $statusCodeAlphanumeric, $statusMessage);
      $this->data = $data;
    }

}
