<?php

namespace Drupal\tmgmt_plunet\Service\DataPayable30;





















class DataPayable30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Result' => 'Drupal\tmgmt_plunet\Service\DataPayable30\Result',
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataPayable30\StringResult',
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\DataPayable30\IntegerResult',
      'TaxListResult' => 'Drupal\tmgmt_plunet\Service\DataPayable30\TaxListResult',
      'Tax' => 'Drupal\tmgmt_plunet\Service\DataPayable30\Tax',
      'DoubleResult' => 'Drupal\tmgmt_plunet\Service\DataPayable30\DoubleResult',
      'DateResult' => 'Drupal\tmgmt_plunet\Service\DataPayable30\DateResult',
      'PriceLineIN' => 'Drupal\tmgmt_plunet\Service\DataPayable30\PriceLineIN',
      'PriceLineResult' => 'Drupal\tmgmt_plunet\Service\DataPayable30\PriceLineResult',
      'PriceLine' => 'Drupal\tmgmt_plunet\Service\DataPayable30\PriceLine',
      'PriceUnitListResult' => 'Drupal\tmgmt_plunet\Service\DataPayable30\PriceUnitListResult',
      'PriceUnit' => 'Drupal\tmgmt_plunet\Service\DataPayable30\PriceUnit',
      'BooleanResult' => 'Drupal\tmgmt_plunet\Service\DataPayable30\BooleanResult',
      'PriceLineListResult' => 'Drupal\tmgmt_plunet\Service\DataPayable30\PriceLineListResult',
      'PayableItemResultList' => 'Drupal\tmgmt_plunet\Service\DataPayable30\PayableItemResultList',
      'PayableItem' => 'Drupal\tmgmt_plunet\Service\DataPayable30\PayableItem',
      'PriceUnitResult' => 'Drupal\tmgmt_plunet\Service\DataPayable30\PriceUnitResult',
      'PayableItemIN' => 'Drupal\tmgmt_plunet\Service\DataPayable30\PayableItemIN',
      'StringArrayResult' => 'Drupal\tmgmt_plunet\Service\DataPayable30\StringArrayResult');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataPayable30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return StringResult
     */
    public function getCurrency($UUID, $payablesID)
    {
      return $this->__soapCall('getCurrency', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesItemID
     * @param int $priceLineID
     * @access public
     * @return Result
     */
    public function deletePriceLine($UUID, $payablesItemID, $priceLineID)
    {
      return $this->__soapCall('deletePriceLine', array($UUID, $payablesItemID, $priceLineID));
    }

    /**
     * @param string $UUID
     * @param int $PriceUnitID
     * @param string $languageCode
     * @access public
     * @return PriceUnitResult
     */
    public function getPriceUnit($UUID, $PriceUnitID, $languageCode)
    {
      return $this->__soapCall('getPriceUnit', array($UUID, $PriceUnitID, $languageCode));
    }

    /**
     * @param string $UUID
     * @param string $languageCode
     * @access public
     * @return StringArrayResult
     */
    public function getServices_List($UUID, $languageCode)
    {
      return $this->__soapCall('getServices_List', array($UUID, $languageCode));
    }

    /**
     * @param string $UUID
     * @param int $payablesItemID
     * @param PriceLineIN $priceLineIN
     * @access public
     * @return PriceLineResult
     */
    public function updatePriceLine($UUID, $payablesItemID, PriceLineIN $priceLineIN)
    {
      return $this->__soapCall('updatePriceLine', array($UUID, $payablesItemID, $priceLineIN));
    }

    /**
     * @param string $UUID
     * @param int $payablesItemID
     * @param PriceLineIN $priceLineIN
     * @param boolean $createAsFirstItem
     * @access public
     * @return PriceLineResult
     */
    public function insertPriceLine($UUID, $payablesItemID, PriceLineIN $priceLineIN, $createAsFirstItem)
    {
      return $this->__soapCall('insertPriceLine', array($UUID, $payablesItemID, $priceLineIN, $createAsFirstItem));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return DateResult
     */
    public function getValueDate($UUID, $payablesID)
    {
      return $this->__soapCall('getValueDate', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $itemID
     * @access public
     * @return IntegerResult
     */
    public function getPayableID($UUID, $itemID)
    {
      return $this->__soapCall('getPayableID', array($UUID, $itemID));
    }

    /**
     * @param string $UUID
     * @param dateTime $invoiceDate
     * @param int $payablesID
     * @access public
     * @return Result
     */
    public function setInvoiceDate($UUID, $invoiceDate, $payablesID)
    {
      return $this->__soapCall('setInvoiceDate', array($UUID, $invoiceDate, $payablesID));
    }

    /**
     * @param string $UUID
     * @param dateTime $valueDate
     * @param int $payablesID
     * @access public
     * @return Result
     */
    public function setValueDate($UUID, $valueDate, $payablesID)
    {
      return $this->__soapCall('setValueDate', array($UUID, $valueDate, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return DateResult
     */
    public function getInvoiceDate($UUID, $payablesID)
    {
      return $this->__soapCall('getInvoiceDate', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return BooleanResult
     */
    public function getIsExported($UUID, $payablesID)
    {
      return $this->__soapCall('getIsExported', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return DateResult
     */
    public function getPaidDate($UUID, $payablesID)
    {
      return $this->__soapCall('getPaidDate', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @param string $memo
     * @access public
     * @return Result
     */
    public function setMemo($UUID, $payablesID, $memo)
    {
      return $this->__soapCall('setMemo', array($UUID, $payablesID, $memo));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @param boolean $isExported
     * @access public
     * @return Result
     */
    public function setIsExported($UUID, $payablesID, $isExported)
    {
      return $this->__soapCall('setIsExported', array($UUID, $payablesID, $isExported));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @param dateTime $paidDate
     * @access public
     * @return Result
     */
    public function setPaidDate($UUID, $payablesID, $paidDate)
    {
      return $this->__soapCall('setPaidDate', array($UUID, $payablesID, $paidDate));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return StringResult
     */
    public function getMemo($UUID, $payablesID)
    {
      return $this->__soapCall('getMemo', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return IntegerResult
     */
    public function getPaymentMethod($UUID, $payablesID)
    {
      return $this->__soapCall('getPaymentMethod', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return IntegerResult
     */
    public function getResourceID($UUID, $payablesID)
    {
      return $this->__soapCall('getResourceID', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return IntegerResult
     */
    public function getStatus($UUID, $payablesID)
    {
      return $this->__soapCall('getStatus', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $Status
     * @param int $payablesID
     * @access public
     * @return Result
     */
    public function setStatus($UUID, $Status, $payablesID)
    {
      return $this->__soapCall('setStatus', array($UUID, $Status, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return DateResult
     */
    public function getPaymentDueDate($UUID, $payablesID)
    {
      return $this->__soapCall('getPaymentDueDate', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @param dateTime $dueDate
     * @access public
     * @return Result
     */
    public function setPaymentDueDate($UUID, $payablesID, $dueDate)
    {
      return $this->__soapCall('setPaymentDueDate', array($UUID, $payablesID, $dueDate));
    }

    /**
     * @param string $UUID
     * @param int $PaymentItemID
     * @access public
     * @return Result
     */
    public function deletePaymentItem($UUID, $PaymentItemID)
    {
      return $this->__soapCall('deletePaymentItem', array($UUID, $PaymentItemID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return StringResult
     */
    public function getAccountStatement($UUID, $payablesID)
    {
      return $this->__soapCall('getAccountStatement', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return StringResult
     */
    public function getCreditorAccount($UUID, $payablesID)
    {
      return $this->__soapCall('getCreditorAccount', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return TaxListResult
     */
    public function getInvoiceTaxTypes($UUID, $payablesID)
    {
      return $this->__soapCall('getInvoiceTaxTypes', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return IntegerResult
     */
    public function getPaymentCreatorResourceID($UUID, $payablesID)
    {
      return $this->__soapCall('getPaymentCreatorResourceID', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param PayableItemIN $PaymentItemIN
     * @param int $PaymentItemID
     * @param boolean $enableNullOrEmptyValues
     * @access public
     * @return Result
     */
    public function updatePaymentItem($UUID, PayableItemIN $PaymentItemIN, $PaymentItemID, $enableNullOrEmptyValues)
    {
      return $this->__soapCall('updatePaymentItem', array($UUID, $PaymentItemIN, $PaymentItemID, $enableNullOrEmptyValues));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @param string $accountstatement
     * @access public
     * @return Result
     */
    public function setAccountStatement($UUID, $payablesID, $accountstatement)
    {
      return $this->__soapCall('setAccountStatement', array($UUID, $payablesID, $accountstatement));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @param int $currencyTpe
     * @param int $taxType
     * @access public
     * @return DoubleResult
     */
    public function getTotalTaxAmount($UUID, $payablesID, $currencyTpe, $taxType)
    {
      return $this->__soapCall('getTotalTaxAmount', array($UUID, $payablesID, $currencyTpe, $taxType));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return StringResult
     */
    public function getExpenseAccount($UUID, $payablesID)
    {
      return $this->__soapCall('getExpenseAccount', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @param int $currencyTpe
     * @access public
     * @return DoubleResult
     */
    public function getTotalNetAmount($UUID, $payablesID, $currencyTpe)
    {
      return $this->__soapCall('getTotalNetAmount', array($UUID, $payablesID, $currencyTpe));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @param string $externalNumber
     * @access public
     * @return Result
     */
    public function setExternalInvoiceNumber($UUID, $payablesID, $externalNumber)
    {
      return $this->__soapCall('setExternalInvoiceNumber', array($UUID, $payablesID, $externalNumber));
    }

    /**
     * @param string $UUID
     * @param string $accountID
     * @param int $payablesID
     * @access public
     * @return Result
     */
    public function setCreditorAccount($UUID, $accountID, $payablesID)
    {
      return $this->__soapCall('setCreditorAccount', array($UUID, $accountID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param PayableItemIN $PaymentItemIN
     * @access public
     * @return IntegerResult
     */
    public function insertPaymentItem($UUID, PayableItemIN $PaymentItemIN)
    {
      return $this->__soapCall('insertPaymentItem', array($UUID, $PaymentItemIN));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return StringResult
     */
    public function getExternalInvoiceNumber($UUID, $payablesID)
    {
      return $this->__soapCall('getExternalInvoiceNumber', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesID
     * @access public
     * @return PayableItemResultList
     */
    public function getPaymentItemList($UUID, $payablesID)
    {
      return $this->__soapCall('getPaymentItemList', array($UUID, $payablesID));
    }

    /**
     * @param string $UUID
     * @param int $payablesItemID
     * @access public
     * @return PriceLineListResult
     */
    public function getPriceLine_List($UUID, $payablesItemID)
    {
      return $this->__soapCall('getPriceLine_List', array($UUID, $payablesItemID));
    }

    /**
     * @param string $UUID
     * @param string $languageCode
     * @param string $service
     * @access public
     * @return PriceUnitListResult
     */
    public function getPriceUnit_List($UUID, $languageCode, $service)
    {
      return $this->__soapCall('getPriceUnit_List', array($UUID, $languageCode, $service));
    }

}
