<?php

namespace Drupal\tmgmt_plunet\Service\DataPayable30;

class PayableItem
{

    /**
     * @var string $briefDescription
     * @access public
     */
    public $briefDescription = null;

    /**
     * @var int $invoiceID
     * @access public
     */
    public $invoiceID = null;

    /**
     * @var int $itemNumber
     * @access public
     */
    public $itemNumber = null;

    /**
     * @var dateTime $jobDate
     * @access public
     */
    public $jobDate = null;

    /**
     * @var string $jobNo
     * @access public
     */
    public $jobNo = null;

    /**
     * @var int $jobStatus
     * @access public
     */
    public $jobStatus = null;

    /**
     * @var int $payableItemID
     * @access public
     */
    public $payableItemID = null;

    /**
     * @var float $totalprice
     * @access public
     */
    public $totalprice = null;

    /**
     * @param string $briefDescription
     * @param int $invoiceID
     * @param int $itemNumber
     * @param dateTime $jobDate
     * @param string $jobNo
     * @param int $jobStatus
     * @param int $payableItemID
     * @param float $totalprice
     * @access public
     */
    public function __construct($briefDescription, $invoiceID, $itemNumber, $jobDate, $jobNo, $jobStatus, $payableItemID, $totalprice)
    {
      $this->briefDescription = $briefDescription;
      $this->invoiceID = $invoiceID;
      $this->itemNumber = $itemNumber;
      $this->jobDate = $jobDate;
      $this->jobNo = $jobNo;
      $this->jobStatus = $jobStatus;
      $this->payableItemID = $payableItemID;
      $this->totalprice = $totalprice;
    }

}
