<?php

namespace Drupal\tmgmt_plunet\Service;

abstract class AbstractSoapClient extends \SoapClient {

  /**
   * @param array $options A array of config values
   * @param string $wsdl The wsdl file to use
   *
   * @access public
   */
  public function __construct($wsdl = '', array $options) {
    if (!isset($options['soap_version'])) {
      $options['soap_version'] = SOAP_1_2;
    }

    parent::__construct($wsdl, $options);
  }

}
