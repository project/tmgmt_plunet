<?php

namespace Drupal\tmgmt_plunet\Service\CallbackQuote30;


class CallbackQuote30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Quote' => 'Drupal\tmgmt_plunet\Service\CallbackQuote30\Quote');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/CallbackQuote30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $Authenticationcode
     * @param int $QuoteID
     * @param int $EventType
     * @access public
     * @return void
     */
    public function ReceiveNotifyCallback($Authenticationcode, $QuoteID, $EventType)
    {
      return $this->__soapCall('ReceiveNotifyCallback', array($Authenticationcode, $QuoteID, $EventType));
    }

    /**
     * @param string $Authenticationcode
     * @param Quote $Quote
     * @param int $EventType
     * @access public
     * @return void
     */
    public function receiveObserverCallback($Authenticationcode, Quote $Quote, $EventType)
    {
      return $this->__soapCall('receiveObserverCallback', array($Authenticationcode, $Quote, $EventType));
    }

}
