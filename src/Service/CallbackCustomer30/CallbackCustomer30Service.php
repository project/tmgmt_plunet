<?php

namespace Drupal\tmgmt_plunet\Service\CallbackCustomer30;


class CallbackCustomer30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Customer' => 'Drupal\tmgmt_plunet\Service\CallbackCustomer30\Customer');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/CallbackCustomer30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $Authenticationcode
     * @param Customer $Customer
     * @param int $EventType
     * @access public
     * @return void
     */
    public function receiveObserverCallback($Authenticationcode, Customer $Customer, $EventType)
    {
      return $this->__soapCall('receiveObserverCallback', array($Authenticationcode, $Customer, $EventType));
    }

    /**
     * @param string $Authenticationcode
     * @param int $CustomerID
     * @param int $EventType
     * @access public
     * @return void
     */
    public function receiveNotifyCallback($Authenticationcode, $CustomerID, $EventType)
    {
      return $this->__soapCall('receiveNotifyCallback', array($Authenticationcode, $CustomerID, $EventType));
    }

}
