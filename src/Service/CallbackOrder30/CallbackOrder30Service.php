<?php

namespace Drupal\tmgmt_plunet\Service\CallbackOrder30;


class CallbackOrder30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Order' => 'Drupal\tmgmt_plunet\Service\CallbackOrder30\Order');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/CallbackOrder30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $Authenticationcode
     * @param int $OrderID
     * @param int $EventType
     * @access public
     * @return void
     */
    public function ReceiveNotifyCallback($Authenticationcode, $OrderID, $EventType)
    {
      return $this->__soapCall('ReceiveNotifyCallback', array($Authenticationcode, $OrderID, $EventType));
    }

    /**
     * @param string $Authenticationcode
     * @param Order $Order
     * @param int $EventType
     * @access public
     * @return void
     */
    public function receiveObserverCallback($Authenticationcode, $Order, $EventType)
    {
      return \Drupal::service('tmgmt_plunet.callbacks')->receiveOrderCallback($Authenticationcode, $Order->orderID, $EventType);
    }

}
