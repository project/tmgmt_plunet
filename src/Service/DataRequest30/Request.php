<?php

namespace Drupal\tmgmt_plunet\Service\DataRequest30;

class Request
{

    /**
     * @var string $briefDescription
     * @access public
     */
    public $briefDescription = null;

    /**
     * @var dateTime $creationDate
     * @access public
     */
    public $creationDate = null;

    /**
     * @var dateTime $deliveryDate
     * @access public
     */
    public $deliveryDate = null;

    /**
     * @var int $orderID
     * @access public
     */
    public $orderID = null;

    /**
     * @var int[] $orderIDList
     * @access public
     */
    public $orderIDList = null;

    /**
     * @var dateTime $quotationDate
     * @access public
     */
    public $quotationDate = null;

    /**
     * @var int $quoteID
     * @access public
     */
    public $quoteID = null;

    /**
     * @var int[] $quoteIDList
     * @access public
     */
    public $quoteIDList = null;

    /**
     * @var int $requestID
     * @access public
     */
    public $requestID = null;

    /**
     * @var int $status
     * @access public
     */
    public $status = null;

    /**
     * @var string $subject
     * @access public
     */
    public $subject = null;

    /**
     * @param string $briefDescription
     * @param dateTime $creationDate
     * @param dateTime $deliveryDate
     * @param int $orderID
     * @param dateTime $quotationDate
     * @param int $quoteID
     * @param int $requestID
     * @param int $status
     * @param string $subject
     * @access public
     */
    public function __construct($briefDescription, $creationDate, $deliveryDate, $orderID, $quotationDate, $quoteID, $requestID, $status, $subject)
    {
      $this->briefDescription = $briefDescription;
      $this->creationDate = $creationDate;
      $this->deliveryDate = $deliveryDate;
      $this->orderID = $orderID;
      $this->quotationDate = $quotationDate;
      $this->quoteID = $quoteID;
      $this->requestID = $requestID;
      $this->status = $status;
      $this->subject = $subject;
    }

}
