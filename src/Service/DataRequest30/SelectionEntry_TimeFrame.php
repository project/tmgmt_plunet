<?php

namespace Drupal\tmgmt_plunet\Service\DataRequest30;

class SelectionEntry_TimeFrame
{

    /**
     * @var dateTime $dateFrom
     * @access public
     */
    public $dateFrom = null;

    /**
     * @var int $dateRelation
     * @access public
     */
    public $dateRelation = null;

    /**
     * @var dateTime $dateTo
     * @access public
     */
    public $dateTo = null;

    /**
     * @param dateTime $dateFrom
     * @param int $dateRelation
     * @param dateTime $dateTo
     * @access public
     */
    public function __construct($dateFrom, $dateRelation, $dateTo)
    {
      $this->dateFrom = $dateFrom;
      $this->dateRelation = $dateRelation;
      $this->dateTo = $dateTo;
    }

}
