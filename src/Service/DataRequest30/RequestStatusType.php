<?php

namespace Drupal\tmgmt_plunet\Service\DataRequest30;

/**
 * Class RequestStatusType
 *
 * @package Drupal\tmgmt_plunet\Service\DataRequest30
 */
final class RequestStatusType {

  const IN_PREPARATION = 1;
  const PENDING = 2;
  const CANCELED = 5;
  const CHANGED_INTO_QUOTE = 6;
  const CHANGED_INTO_ORDER = 7;
  const NEW_AUTO = 8;
  const REJECTED = 9;

  public static $names = [
    self::IN_PREPARATION => 'In preparation',
    self::PENDING => 'Pending',
    self::CANCELED => 'Canceled',
    self::CHANGED_INTO_QUOTE => 'Changed into quote',
    self::CHANGED_INTO_ORDER => 'Changed into order',
    self::NEW_AUTO => 'New auto',
    self::REJECTED => 'Rejected',
  ];

}
