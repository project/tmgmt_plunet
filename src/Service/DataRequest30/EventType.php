<?php

namespace Drupal\tmgmt_plunet\Service\DataRequest30;

/**
 * Class EventTypes
 *
 * @package Drupal\tmgmt_plunet\Service\DataRequest30
 */
final class EventType {

  const STATUS_CHANGED = 1;
  const NEW_ENTRY_CREATED = 2;
  const ENTRY_DELETED = 3;
  const START_DATE_CHANGED = 4;
  const DELIVERY_DATE_CHANGED = 5;

}
