<?php

namespace Drupal\tmgmt_plunet\Service\DataRequest30;

class SearchFilter_Request
{

    /**
     * @var string $languageCode
     * @access public
     */
    public $languageCode = null;

    /**
     * @var Property[] $propertiesList
     * @access public
     */
    public $propertiesList = null;

    /**
     * @var SelectionEntry_Customer $SelectionEntry_Customer
     * @access public
     */
    public $SelectionEntry_Customer = null;

    /**
     * @var string $sourceLanguage
     * @access public
     */
    public $sourceLanguage = null;

    /**
     * @var int $requestStatus
     * @access public
     */
    public $requestStatus = null;

    /**
     * @var string $targetLanguage
     * @access public
     */
    public $targetLanguage = null;

    /**
     * @var Textmodule[] $textmodulesList
     * @access public
     */
    public $textmodulesList = null;

    /**
     * @var SelectionEntry_TimeFrame $timeFrame
     * @access public
     */
    public $timeFrame = null;

    /**
     * @param string $languageCode
     * @param Property[] $propertiesList
     * @param SelectionEntry_Customer $SelectionEntry_Customer
     * @param string $sourceLanguage
     * @param int $requestStatus
     * @param string $targetLanguage
     * @param Textmodule[] $textmodulesList
     * @param SelectionEntry_TimeFrame $timeFrame
     * @access public
     */
    public function __construct($languageCode, $propertiesList, $SelectionEntry_Customer, $sourceLanguage, $requestStatus, $targetLanguage, $textmodulesList, $timeFrame)
    {
      $this->languageCode = $languageCode;
      $this->propertiesList = $propertiesList;
      $this->SelectionEntry_Customer = $SelectionEntry_Customer;
      $this->sourceLanguage = $sourceLanguage;
      $this->requestStatus = $requestStatus;
      $this->targetLanguage = $targetLanguage;
      $this->textmodulesList = $textmodulesList;
      $this->timeFrame = $timeFrame;
    }

}
