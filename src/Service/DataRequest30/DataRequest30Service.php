<?php

namespace Drupal\tmgmt_plunet\Service\DataRequest30;



















class DataRequest30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\DataRequest30\IntegerResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\DataRequest30\Result',
      'RequestIN' => 'Drupal\tmgmt_plunet\Service\DataRequest30\RequestIN',
      'IntegerArrayResult' => 'Drupal\tmgmt_plunet\Service\DataRequest30\IntegerArrayResult',
      'RequestResult' => 'Drupal\tmgmt_plunet\Service\DataRequest30\RequestResult',
      'Request' => 'Drupal\tmgmt_plunet\Service\DataRequest30\Request',
      'DateResult' => 'Drupal\tmgmt_plunet\Service\DataRequest30\DateResult',
      'BooleanResult' => 'Drupal\tmgmt_plunet\Service\DataRequest30\BooleanResult',
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataRequest30\StringResult',
      'IntegerList' => 'Drupal\tmgmt_plunet\Service\DataRequest30\IntegerList',
      'RequestListResult' => 'Drupal\tmgmt_plunet\Service\DataRequest30\RequestListResult',
      'DoubleResult' => 'Drupal\tmgmt_plunet\Service\DataRequest30\DoubleResult',
      'SearchFilter_Request' => 'Drupal\tmgmt_plunet\Service\DataRequest30\SearchFilter_Request',
      'Property' => 'Drupal\tmgmt_plunet\Service\DataRequest30\Property',
      'SelectionEntry_Customer' => 'Drupal\tmgmt_plunet\Service\DataRequest30\SelectionEntry_Customer',
      'Textmodule' => 'Drupal\tmgmt_plunet\Service\DataRequest30\Textmodule',
      'SelectionEntry_TimeFrame' => 'Drupal\tmgmt_plunet\Service\DataRequest30\SelectionEntry_TimeFrame');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataRequest30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @param string $propertyNameEnglish
     * @param string $propertyValueEnglish
     * @access public
     * @return Result
     */
    public function setProperty($UUID, $requestID, $propertyNameEnglish, $propertyValueEnglish)
    {
      return $this->__soapCall('setProperty', array($UUID, $requestID, $propertyNameEnglish, $propertyValueEnglish));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @param string $propertyNameEnglish
     * @access public
     * @return StringResult
     */
    public function getProperty($UUID, $requestID, $propertyNameEnglish)
    {
      return $this->__soapCall('getProperty', array($UUID, $requestID, $propertyNameEnglish));
    }

    /**
     * @param string $UUID
     * @param RequestIN $RequestIN
     * @param boolean $enableNullOrEmptyValues
     * @access public
     * @return Result
     */
    public function update($UUID, RequestIN $RequestIN, $enableNullOrEmptyValues)
    {
      return $this->__soapCall('update', array($UUID, $RequestIN, $enableNullOrEmptyValues));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function delete($UUID, $requestID)
    {
      return $this->__soapCall('delete', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @access public
     * @return IntegerResult
     */
    public function insert($UUID)
    {
      return $this->__soapCall('insert', array($UUID));
    }

    /**
     * @param string $UUID
     * @param SearchFilter_Request $SearchFilter
     * @access public
     * @return IntegerArrayResult
     */
    public function search($UUID, SearchFilter_Request $SearchFilter)
    {
      return $this->__soapCall('search', array($UUID, $SearchFilter));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerResult
     */
    public function getWordCount($UUID, $requestID)
    {
      return $this->__soapCall('getWordCount', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $wordCount
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setWordCount($UUID, $wordCount, $requestID)
    {
      return $this->__soapCall('setWordCount', array($UUID, $wordCount, $requestID));
    }

    /**
     * @param string $UUID
     * @param RequestIN $RequestIN
     * @access public
     * @return IntegerResult
     */
    public function insert2($UUID, RequestIN $RequestIN)
    {
      return $this->__soapCall('insert2', array($UUID, $RequestIN));
    }

    /**
     * @param string $UUID
     * @param int $customerID
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setCustomerID($UUID, $customerID, $requestID)
    {
      return $this->__soapCall('setCustomerID', array($UUID, $customerID, $requestID));
    }

    /**
     * @param string $UUID
     * @param dateTime $date
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setCreationDate($UUID, $date, $requestID)
    {
      return $this->__soapCall('setCreationDate', array($UUID, $date, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return StringResult
     */
    public function getSubject($UUID, $requestID)
    {
      return $this->__soapCall('getSubject', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $orderID
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setOrderID($UUID, $orderID, $requestID)
    {
      return $this->__soapCall('setOrderID', array($UUID, $orderID, $requestID));
    }

    /**
     * @param string $UUID
     * @param dateTime $date
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setQuotationDate($UUID, $date, $requestID)
    {
      return $this->__soapCall('setQuotationDate', array($UUID, $date, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $quoteID
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setQuoteID($UUID, $quoteID, $requestID)
    {
      return $this->__soapCall('setQuoteID', array($UUID, $quoteID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return DateResult
     */
    public function getQuotationDate($UUID, $requestID)
    {
      return $this->__soapCall('getQuotationDate', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param dateTime $date
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setDeliveryDate($UUID, $date, $requestID)
    {
      return $this->__soapCall('setDeliveryDate', array($UUID, $date, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return DateResult
     */
    public function getDeliveryDate($UUID, $requestID)
    {
      return $this->__soapCall('getDeliveryDate', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return DoubleResult
     */
    public function getPrice($UUID, $requestID)
    {
      return $this->__soapCall('getPrice', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param float $price
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setPrice($UUID, $price, $requestID)
    {
      return $this->__soapCall('setPrice', array($UUID, $price, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return StringResult
     */
    public function getCustomerRefNo($UUID, $requestID)
    {
      return $this->__soapCall('getCustomerRefNo', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param string $CustomerRefNo
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setCustomerRefNo($UUID, $CustomerRefNo, $requestID)
    {
      return $this->__soapCall('setCustomerRefNo', array($UUID, $CustomerRefNo, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerArrayResult
     */
    public function getOrderIDList($UUID, $requestID)
    {
      return $this->__soapCall('getOrderIDList', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @access public
     * @return IntegerArrayResult
     */
    public function getAll_Requests($UUID)
    {
      return $this->__soapCall('getAll_Requests', array($UUID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerArrayResult
     */
    public function getQuoteIDList($UUID, $requestID)
    {
      return $this->__soapCall('getQuoteIDList', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return RequestResult
     */
    public function getRequestObject($UUID, $requestID)
    {
      return $this->__soapCall('getRequestObject', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerResult
     */
    public function orderRequest($UUID, $requestID)
    {
      return $this->__soapCall('orderRequest', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return StringResult
     */
    public function getTypeOfProject($UUID, $requestID)
    {
      return $this->__soapCall('getTypeOfProject', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param boolean $IsRushRequest
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setRushRequest($UUID, $IsRushRequest, $requestID)
    {
      return $this->__soapCall('setRushRequest', array($UUID, $IsRushRequest, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerResult
     */
    public function getWorkflowID($UUID, $requestID)
    {
      return $this->__soapCall('getWorkflowID', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @param int $workflowID
     * @access public
     * @return Result
     */
    public function setWorkflowID($UUID, $requestID, $workflowID)
    {
      return $this->__soapCall('setWorkflowID', array($UUID, $requestID, $workflowID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return BooleanResult
     */
    public function getRushRequest($UUID, $requestID)
    {
      return $this->__soapCall('getRushRequest', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerResult
     */
    public function quoteRequest($UUID, $requestID)
    {
      return $this->__soapCall('quoteRequest', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @param string $TypeOfProject
     * @access public
     * @return Result
     */
    public function setTypeOfProject($UUID, $requestID, $TypeOfProject)
    {
      return $this->__soapCall('setTypeOfProject', array($UUID, $requestID, $TypeOfProject));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerResult
     */
    public function getCustomerID($UUID, $requestID)
    {
      return $this->__soapCall('getCustomerID', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerResult
     */
    public function getOrderID($UUID, $requestID)
    {
      return $this->__soapCall('getOrderID', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param string $displayNo
     * @access public
     * @return IntegerResult
     */
    public function getRequestID($UUID, $displayNo)
    {
      return $this->__soapCall('getRequestID', array($UUID, $displayNo));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerResult
     */
    public function getQuoteID($UUID, $requestID)
    {
      return $this->__soapCall('getQuoteID', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return DateResult
     */
    public function getCreationDate($UUID, $requestID)
    {
      return $this->__soapCall('getCreationDate', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param string $service
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function addService($UUID, $service, $requestID)
    {
      return $this->__soapCall('addService', array($UUID, $service, $requestID));
    }

    /**
     * @param string $UUID
     * @param string $subject
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setSubject($UUID, $subject, $requestID)
    {
      return $this->__soapCall('setSubject', array($UUID, $subject, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerResult
     */
    public function getStatus($UUID, $requestID)
    {
      return $this->__soapCall('getStatus', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param string $sourceLanguage
     * @param string $targetLanguage
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function addLanguageCombination($UUID, $sourceLanguage, $targetLanguage, $requestID)
    {
      return $this->__soapCall('addLanguageCombination', array($UUID, $sourceLanguage, $targetLanguage, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $Status
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setStatus($UUID, $Status, $requestID)
    {
      return $this->__soapCall('setStatus', array($UUID, $Status, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerResult
     */
    public function getCustomerContactID($UUID, $requestID)
    {
      return $this->__soapCall('getCustomerContactID', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @param int $customerContactID
     * @access public
     * @return Result
     */
    public function setCustomerContactID($UUID, $requestID, $customerContactID)
    {
      return $this->__soapCall('setCustomerContactID', array($UUID, $requestID, $customerContactID));
    }

    /**
     * @param string $UUID
     * @param string $ServerAuthenticationString
     * @param string $ServerAddress
     * @param int $EventType
     * @access public
     * @return Result
     */
    public function registerCallback_Notify($UUID, $ServerAuthenticationString, $ServerAddress, $EventType)
    {
      return $this->__soapCall('registerCallback_Notify', array($UUID, $ServerAuthenticationString, $ServerAddress, $EventType));
    }

    /**
     * @param string $UUID
     * @param string $ServerAuthenticationString
     * @param string $ServerAddress
     * @param int $RequestID
     * @access public
     * @return Result
     */
    public function registerCallback_Observer($UUID, $ServerAuthenticationString, $ServerAddress, $RequestID)
    {
      return $this->__soapCall('registerCallback_Observer', array($UUID, $ServerAuthenticationString, $ServerAddress, $RequestID));
    }

    /**
     * @param string $UUID
     * @param int $EventType
     * @access public
     * @return Result
     */
    public function deregisterCallback_Notify($UUID, $EventType)
    {
      return $this->__soapCall('deregisterCallback_Notify', array($UUID, $EventType));
    }

    /**
     * @param string $UUID
     * @param int $RequestID
     * @access public
     * @return Result
     */
    public function deregisterCallback_Observer($UUID, $RequestID)
    {
      return $this->__soapCall('deregisterCallback_Observer', array($UUID, $RequestID));
    }

    /**
     * @param string $UUID
     * @param string $briefDescription
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setBriefDescription($UUID, $briefDescription, $requestID)
    {
      return $this->__soapCall('setBriefDescription', array($UUID, $briefDescription, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return StringResult
     */
    public function getBriefDescription($UUID, $requestID)
    {
      return $this->__soapCall('getBriefDescription', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param string $refLanguage
     * @param string $uncFileName
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function copyDocument_toReferenceFolder($UUID, $refLanguage, $uncFileName, $requestID)
    {
      return $this->__soapCall('copyDocument_toReferenceFolder', array($UUID, $refLanguage, $uncFileName, $requestID));
    }

    /**
     * @param string $UUID
     * @param string $sourceLanguage
     * @param string $uncFileName
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function copyDocument_toSourceFolder($UUID, $sourceLanguage, $uncFileName, $requestID)
    {
      return $this->__soapCall('copyDocument_toSourceFolder', array($UUID, $sourceLanguage, $uncFileName, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return StringResult
     */
    public function getRequestNo_for_View($UUID, $requestID)
    {
      return $this->__soapCall('getRequestNo_for_View', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return BooleanResult
     */
    public function getEN15038Requested($UUID, $requestID)
    {
      return $this->__soapCall('getEN15038Requested', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param string $CustomerRefNo_PreviousOrder
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setCustomerRefNo_PrevOrder($UUID, $CustomerRefNo_PreviousOrder, $requestID)
    {
      return $this->__soapCall('setCustomerRefNo_PrevOrder', array($UUID, $CustomerRefNo_PreviousOrder, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return StringResult
     */
    public function getCustomerRefNo_PrevOrder($UUID, $requestID)
    {
      return $this->__soapCall('getCustomerRefNo_PrevOrder', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param boolean $isEN15038
     * @param int $requestID
     * @access public
     * @return Result
     */
    public function setEN15038Requested($UUID, $isEN15038, $requestID)
    {
      return $this->__soapCall('setEN15038Requested', array($UUID, $isEN15038, $requestID));
    }

    /**
     * @param string $UUID
     * @param string $requestNumber
     * @access public
     * @return RequestResult
     */
    public function getRequestObject2($UUID, $requestNumber)
    {
      return $this->__soapCall('getRequestObject2', array($UUID, $requestNumber));
    }

    /**
     * @param string $UUID
     * @param IntegerList $requestIDList
     * @access public
     * @return RequestListResult
     */
    public function getRequestObjectList($UUID, IntegerList $requestIDList)
    {
      return $this->__soapCall('getRequestObjectList', array($UUID, $requestIDList));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @param int $OrderTemplateID
     * @access public
     * @return IntegerResult
     */
    public function orderRequest_byTemplate($UUID, $requestID, $OrderTemplateID)
    {
      return $this->__soapCall('orderRequest_byTemplate', array($UUID, $requestID, $OrderTemplateID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @param int $MasterProjectID
     * @access public
     * @return Result
     */
    public function setMasterProjectID($UUID, $requestID, $MasterProjectID)
    {
      return $this->__soapCall('setMasterProjectID', array($UUID, $requestID, $MasterProjectID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @access public
     * @return IntegerResult
     */
    public function getMasterProjectID($UUID, $requestID)
    {
      return $this->__soapCall('getMasterProjectID', array($UUID, $requestID));
    }

    /**
     * @param string $UUID
     * @param int $requestID
     * @param int $QuoteTemplateID
     * @access public
     * @return IntegerResult
     */
    public function quoteRequest_byTemplate($UUID, $requestID, $QuoteTemplateID)
    {
      return $this->__soapCall('quoteRequest_byTemplate', array($UUID, $requestID, $QuoteTemplateID));
    }

}
