<?php

namespace Drupal\tmgmt_plunet\Service\DataRequest30;

class SelectionEntry_Customer
{

    /**
     * @var int $customerEntryType
     * @access public
     */
    public $customerEntryType = null;

    /**
     * @var int $mainID
     * @access public
     */
    public $mainID = null;

    /**
     * @param int $customerEntryType
     * @param int $mainID
     * @access public
     */
    public function __construct($customerEntryType, $mainID)
    {
      $this->customerEntryType = $customerEntryType;
      $this->mainID = $mainID;
    }

}
