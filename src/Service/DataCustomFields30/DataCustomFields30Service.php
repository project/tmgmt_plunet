<?php

namespace Drupal\tmgmt_plunet\Service\DataCustomFields30;










class DataCustomFields30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'TextmoduleResult' => 'Drupal\tmgmt_plunet\Service\DataCustomFields30\TextmoduleResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\DataCustomFields30\Result',
      'Textmodule' => 'Drupal\tmgmt_plunet\Service\DataCustomFields30\Textmodule',
      'TextmoduleIN' => 'Drupal\tmgmt_plunet\Service\DataCustomFields30\TextmoduleIN',
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataCustomFields30\StringResult',
      'PropertyResult' => 'Drupal\tmgmt_plunet\Service\DataCustomFields30\PropertyResult',
      'Property' => 'Drupal\tmgmt_plunet\Service\DataCustomFields30\Property',
      'IntegerList' => 'Drupal\tmgmt_plunet\Service\DataCustomFields30\IntegerList');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataCustomFields30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param string $PropertyNameEnglish
     * @param int $PropertyUsageArea
     * @param int $MainID
     * @access public
     * @return PropertyResult
     */
    public function getProperty($UUID, $PropertyNameEnglish, $PropertyUsageArea, $MainID)
    {
      return $this->__soapCall('getProperty', array($UUID, $PropertyNameEnglish, $PropertyUsageArea, $MainID));
    }

    /**
     * @param string $UUID
     * @param string $Flag
     * @param int $TextModuleUsageArea
     * @param int $ID
     * @param string $languageCode
     * @access public
     * @return TextmoduleResult
     */
    public function getTextmodule($UUID, $Flag, $TextModuleUsageArea, $ID, $languageCode)
    {
      return $this->__soapCall('getTextmodule', array($UUID, $Flag, $TextModuleUsageArea, $ID, $languageCode));
    }

    /**
     * @param string $UUID
     * @param string $PropertyNameEnglish
     * @param int $PropertyUsageArea
     * @param int $PropertyValueID
     * @param int $MainID
     * @access public
     * @return Result
     */
    public function setPropertyValue($UUID, $PropertyNameEnglish, $PropertyUsageArea, $PropertyValueID, $MainID)
    {
      return $this->__soapCall('setPropertyValue', array($UUID, $PropertyNameEnglish, $PropertyUsageArea, $PropertyValueID, $MainID));
    }

    /**
     * @param string $UUID
     * @param TextmoduleIN $TextmoduleIN
     * @param int $ID
     * @param string $languageCode
     * @access public
     * @return Result
     */
    public function setTextmodule($UUID, TextmoduleIN $TextmoduleIN, $ID, $languageCode)
    {
      return $this->__soapCall('setTextmodule', array($UUID, $TextmoduleIN, $ID, $languageCode));
    }

    /**
     * @param string $UUID
     * @param string $PropertyNameEnglish
     * @param int $PropertyUsageArea
     * @param IntegerList $PropertyValueList
     * @param int $MainID
     * @access public
     * @return Result
     */
    public function setPropertyValueList($UUID, $PropertyNameEnglish, $PropertyUsageArea, IntegerList $PropertyValueList, $MainID)
    {
      return $this->__soapCall('setPropertyValueList', array($UUID, $PropertyNameEnglish, $PropertyUsageArea, $PropertyValueList, $MainID));
    }

    /**
     * @param string $UUID
     * @param string $PropertyNameEnglish
     * @param int $PropertyValueID
     * @param string $languageCode
     * @access public
     * @return StringResult
     */
    public function getPropertyValueText($UUID, $PropertyNameEnglish, $PropertyValueID, $languageCode)
    {
      return $this->__soapCall('getPropertyValueText', array($UUID, $PropertyNameEnglish, $PropertyValueID, $languageCode));
    }

}
