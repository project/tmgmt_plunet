<?php

namespace Drupal\tmgmt_plunet\Service\DataCustomFields30;

class TextmoduleIN
{

    /**
     * @var dateTime $dateValue
     * @access public
     */
    public $dateValue = null;

    /**
     * @var string $flag
     * @access public
     */
    public $flag = null;

    /**
     * @var string[] $selectedValues
     * @access public
     */
    public $selectedValues = null;

    /**
     * @var string $stringValue
     * @access public
     */
    public $stringValue = null;

    /**
     * @var int $textModuleUsageArea
     * @access public
     */
    public $textModuleUsageArea = null;

    /**
     * @param dateTime $dateValue
     * @param string $flag
     * @param string $stringValue
     * @param int $textModuleUsageArea
     * @access public
     */
    public function __construct($dateValue, $flag, $stringValue, $textModuleUsageArea)
    {
      $this->dateValue = $dateValue;
      $this->flag = $flag;
      $this->stringValue = $stringValue;
      $this->textModuleUsageArea = $textModuleUsageArea;
    }

}
