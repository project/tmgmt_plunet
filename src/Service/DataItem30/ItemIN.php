<?php

namespace Drupal\tmgmt_plunet\Service\DataItem30;

class ItemIN
{

    /**
     * @var string $briefDescription
     * @access public
     */
    public $briefDescription = null;

    /**
     * @var string $comment
     * @access public
     */
    public $comment = null;

    /**
     * @var dateTime $deliveryDeadline
     * @access public
     */
    public $deliveryDeadline = null;

    /**
     * @var int $itemID
     * @access public
     */
    public $itemID = null;

    /**
     * @var int $projectID
     * @access public
     */
    public $projectID = null;

    /**
     * @var int $projectType
     * @access public
     */
    public $projectType = null;

    /**
     * @var string $reference
     * @access public
     */
    public $reference = null;

    /**
     * @var int $status
     * @access public
     */
    public $status = null;

    /**
     * @var float $totalPrice
     * @access public
     */
    public $totalPrice = null;

    /**
     * @param string $briefDescription
     * @param string $comment
     * @param dateTime $deliveryDeadline
     * @param int $itemID
     * @param int $projectID
     * @param int $projectType
     * @param string $reference
     * @param int $status
     * @param float $totalPrice
     * @access public
     */
    public function __construct($briefDescription, $comment, $deliveryDeadline, $itemID, $projectID, $projectType, $reference, $status, $totalPrice)
    {
      $this->briefDescription = $briefDescription;
      $this->comment = $comment;
      $this->deliveryDeadline = $deliveryDeadline;
      $this->itemID = $itemID;
      $this->projectID = $projectID;
      $this->projectType = $projectType;
      $this->reference = $reference;
      $this->status = $status;
      $this->totalPrice = $totalPrice;
    }

}
