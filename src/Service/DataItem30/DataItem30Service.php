<?php

namespace Drupal\tmgmt_plunet\Service\DataItem30;

























class DataItem30Service extends \Drupal\tmgmt_plunet\Service\AbstractSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'StringResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\StringResult',
      'Result' => 'Drupal\tmgmt_plunet\Service\DataItem30\Result',
      'PricelistResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\PricelistResult',
      'Pricelist' => 'Drupal\tmgmt_plunet\Service\DataItem30\Pricelist',
      'ItemListResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\ItemListResult',
      'Item' => 'Drupal\tmgmt_plunet\Service\DataItem30\Item',
      'IntegerResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\IntegerResult',
      'ItemIN' => 'Drupal\tmgmt_plunet\Service\DataItem30\ItemIN',
      'ItemResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\ItemResult',
      'DateResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\DateResult',
      'DoubleResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\DoubleResult',
      'StringArrayResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\StringArrayResult',
      'PricelistEntryList' => 'Drupal\tmgmt_plunet\Service\DataItem30\PricelistEntryList',
      'PricelistEntry' => 'Drupal\tmgmt_plunet\Service\DataItem30\PricelistEntry',
      'IntegerArrayResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\IntegerArrayResult',
      'PriceLineListResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\PriceLineListResult',
      'PriceLine' => 'Drupal\tmgmt_plunet\Service\DataItem30\PriceLine',
      'PriceUnitListResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\PriceUnitListResult',
      'PriceUnit' => 'Drupal\tmgmt_plunet\Service\DataItem30\PriceUnit',
      'PriceLineIN' => 'Drupal\tmgmt_plunet\Service\DataItem30\PriceLineIN',
      'PriceLineResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\PriceLineResult',
      'PricelistListResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\PricelistListResult',
      'PriceUnitResult' => 'Drupal\tmgmt_plunet\Service\DataItem30\PriceUnitResult');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://yourapi.com/DataItem30?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param string $UUID
     * @param ItemIN $ItemIN
     * @param boolean $enableNullOrEmptyValues
     * @access public
     * @return Result
     */
    public function update($UUID, ItemIN $ItemIN, $enableNullOrEmptyValues)
    {
      return $this->__soapCall('update', array($UUID, $ItemIN, $enableNullOrEmptyValues));
    }

    /**
     * @param string $UUID
     * @param int $itemID
     * @param int $projectType
     * @access public
     * @return Result
     */
    public function delete($UUID, $itemID, $projectType)
    {
      return $this->__soapCall('delete', array($UUID, $itemID, $projectType));
    }

    /**
     * @param string $UUID
     * @param int $projectID
     * @param int $projectType
     * @access public
     * @return IntegerResult
     */
    public function insert($UUID, $projectID, $projectType)
    {
      return $this->__soapCall('insert', array($UUID, $projectID, $projectType));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return StringResult
     */
    public function getComment($UUID, $projectType, $itemID)
    {
      return $this->__soapCall('getComment', array($UUID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param string $comment
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return Result
     */
    public function setComment($UUID, $comment, $projectType, $itemID)
    {
      return $this->__soapCall('setComment', array($UUID, $comment, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param ItemIN $ItemIN
     * @access public
     * @return IntegerResult
     */
    public function insert2($UUID, ItemIN $ItemIN)
    {
      return $this->__soapCall('insert2', array($UUID, $ItemIN));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $projectID
     * @access public
     * @return IntegerArrayResult
     */
    public function getAllItems($UUID, $projectType, $projectID)
    {
      return $this->__soapCall('getAllItems', array($UUID, $projectType, $projectID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return IntegerResult
     */
    public function getInvoiceID($UUID, $projectType, $itemID)
    {
      return $this->__soapCall('getInvoiceID', array($UUID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return DoubleResult
     */
    public function getTotalPrice($UUID, $projectType, $itemID)
    {
      return $this->__soapCall('getTotalPrice', array($UUID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return IntegerArrayResult
     */
    public function getJobs($UUID, $projectType, $itemID)
    {
      return $this->__soapCall('getJobs', array($UUID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return ItemResult
     */
    public function getItemObject($UUID, $projectType, $itemID)
    {
      return $this->__soapCall('getItemObject', array($UUID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param dateTime $deliveryDate
     * @param int $itemID
     * @access public
     * @return Result
     */
    public function setDeliveryDate($UUID, $deliveryDate, $itemID)
    {
      return $this->__soapCall('setDeliveryDate', array($UUID, $deliveryDate, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $itemID
     * @access public
     * @return DateResult
     */
    public function getDeliveryDate($UUID, $itemID)
    {
      return $this->__soapCall('getDeliveryDate', array($UUID, $itemID));
    }

    /**
     * @param string $UUID
     * @param string $pathOrUrl
     * @param boolean $overwriteExistingPriceLines
     * @param int $catType
     * @param int $projectType
     * @param boolean $copyResultsToItem
     * @param int $itemID
     * @access public
     * @return Result
     */
    public function setCatReport($UUID, $pathOrUrl, $overwriteExistingPriceLines, $catType, $projectType, $copyResultsToItem, $itemID)
    {
      return $this->__soapCall('setCatReport', array($UUID, $pathOrUrl, $overwriteExistingPriceLines, $catType, $projectType, $copyResultsToItem, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param float $totalPrice
     * @param int $itemID
     * @access public
     * @return Result
     */
    public function setTotalPrice($UUID, $projectType, $totalPrice, $itemID)
    {
      return $this->__soapCall('setTotalPrice', array($UUID, $projectType, $totalPrice, $itemID));
    }

    /**
     * @param string $UUID
     * @param base64Binary $FileByteStream
     * @param string $FilePathName
     * @param int $Filesize
     * @param boolean $overwriteExistingPriceLines
     * @param int $catType
     * @param int $projectType
     * @param boolean $copyResultsToItem
     * @param int $itemID
     * @access public
     * @return Result
     */
    public function setCatReport2($UUID, $FileByteStream, $FilePathName, $Filesize, $overwriteExistingPriceLines, $catType, $projectType, $copyResultsToItem, $itemID)
    {
      return $this->__soapCall('setCatReport2', array($UUID, $FileByteStream, $FilePathName, $Filesize, $overwriteExistingPriceLines, $catType, $projectType, $copyResultsToItem, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $itemID
     * @param int $projectType
     * @param int $priceLineID
     * @access public
     * @return Result
     */
    public function deletePriceLine($UUID, $itemID, $projectType, $priceLineID)
    {
      return $this->__soapCall('deletePriceLine', array($UUID, $itemID, $projectType, $priceLineID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return StringResult
     */
    public function getItemReference($UUID, $projectType, $itemID)
    {
      return $this->__soapCall('getItemReference', array($UUID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $PriceUnitID
     * @param string $languageCode
     * @access public
     * @return PriceUnitResult
     */
    public function getPriceUnit($UUID, $PriceUnitID, $languageCode)
    {
      return $this->__soapCall('getPriceUnit', array($UUID, $PriceUnitID, $languageCode));
    }

    /**
     * @param string $UUID
     * @param int $itemID
     * @param int $projectType
     * @access public
     * @return PricelistResult
     */
    public function getPricelist($UUID, $itemID, $projectType)
    {
      return $this->__soapCall('getPricelist', array($UUID, $itemID, $projectType));
    }

    /**
     * @param string $UUID
     * @param string $languageCode
     * @access public
     * @return StringArrayResult
     */
    public function getServices_List($UUID, $languageCode)
    {
      return $this->__soapCall('getServices_List', array($UUID, $languageCode));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $projectID
     * @param string $sourceLanguage
     * @param string $targetLanguage
     * @access public
     * @return IntegerResult
     */
    public function get_ByLanguage($UUID, $projectType, $projectID, $sourceLanguage, $targetLanguage)
    {
      return $this->__soapCall('get_ByLanguage', array($UUID, $projectType, $projectID, $sourceLanguage, $targetLanguage));
    }

    /**
     * @param string $UUID
     * @param int $itemID
     * @param int $projectType
     * @param PriceLineIN $priceLineIN
     * @access public
     * @return PriceLineResult
     */
    public function updatePriceLine($UUID, $itemID, $projectType, PriceLineIN $priceLineIN)
    {
      return $this->__soapCall('updatePriceLine', array($UUID, $itemID, $projectType, $priceLineIN));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @param string $reference
     * @access public
     * @return Result
     */
    public function setItemReference($UUID, $projectType, $itemID, $reference)
    {
      return $this->__soapCall('setItemReference', array($UUID, $projectType, $itemID, $reference));
    }

    /**
     * @param string $UUID
     * @param int $itemID
     * @param int $projectType
     * @param PriceLineIN $priceLineIN
     * @param boolean $createAsFirstItem
     * @access public
     * @return PriceLineResult
     */
    public function insertPriceLine($UUID, $itemID, $projectType, PriceLineIN $priceLineIN, $createAsFirstItem)
    {
      return $this->__soapCall('insertPriceLine', array($UUID, $itemID, $projectType, $priceLineIN, $createAsFirstItem));
    }

    /**
     * @param string $UUID
     * @param int $itemID
     * @param int $projectType
     * @param int $priceListID
     * @access public
     * @return Result
     */
    public function setPricelist($UUID, $itemID, $projectType, $priceListID)
    {
      return $this->__soapCall('setPricelist', array($UUID, $itemID, $projectType, $priceListID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return IntegerResult
     */
    public function getOrderID($UUID, $projectType, $itemID)
    {
      return $this->__soapCall('getOrderID', array($UUID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return IntegerResult
     */
    public function getStatus($UUID, $projectType, $itemID)
    {
      return $this->__soapCall('getStatus', array($UUID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param string $sourceLanguage
     * @param string $targetLanguage
     * @param int $projectType
     * @param int $projectID
     * @param int $itemID
     * @access public
     * @return IntegerResult
     */
    public function addLanguageCombination($UUID, $sourceLanguage, $targetLanguage, $projectType, $projectID, $itemID)
    {
      return $this->__soapCall('addLanguageCombination', array($UUID, $sourceLanguage, $targetLanguage, $projectType, $projectID, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $status
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return Result
     */
    public function setStatus($UUID, $status, $projectType, $itemID)
    {
      return $this->__soapCall('setStatus', array($UUID, $status, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param string $sourceLanguage
     * @param string $targetLanguage
     * @param int $projectType
     * @param int $projectID
     * @param int $itemID
     * @access public
     * @return IntegerResult
     */
    public function seekLanguageCombination($UUID, $sourceLanguage, $targetLanguage, $projectType, $projectID, $itemID)
    {
      return $this->__soapCall('seekLanguageCombination', array($UUID, $sourceLanguage, $targetLanguage, $projectType, $projectID, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $Status
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return IntegerArrayResult
     */
    public function getJobsWithStatus($UUID, $Status, $projectType, $itemID)
    {
      return $this->__soapCall('getJobsWithStatus', array($UUID, $Status, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return DateResult
     */
    public function getDeliveryDeadline($UUID, $projectType, $itemID)
    {
      return $this->__soapCall('getDeliveryDeadline', array($UUID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $languageCombinationID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return Result
     */
    public function setLanguageCombinationID($UUID, $languageCombinationID, $projectType, $itemID)
    {
      return $this->__soapCall('setLanguageCombinationID', array($UUID, $languageCombinationID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param dateTime $deadline
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return Result
     */
    public function setDeliveryDeadline($UUID, $deadline, $projectType, $itemID)
    {
      return $this->__soapCall('setDeliveryDeadline', array($UUID, $deadline, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param string $description
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return Result
     */
    public function setBriefDescription($UUID, $description, $projectType, $itemID)
    {
      return $this->__soapCall('setBriefDescription', array($UUID, $description, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return StringResult
     */
    public function getTargetLanguage($UUID, $projectType, $itemID)
    {
      return $this->__soapCall('getTargetLanguage', array($UUID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return StringResult
     */
    public function getBriefDescription($UUID, $projectType, $itemID)
    {
      return $this->__soapCall('getBriefDescription', array($UUID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return StringResult
     */
    public function getSourceLanguage($UUID, $projectType, $itemID)
    {
      return $this->__soapCall('getSourceLanguage', array($UUID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $projectID
     * @param int $projectType
     * @access public
     * @return ItemListResult
     */
    public function getAllItemObjects($UUID, $projectID, $projectType)
    {
      return $this->__soapCall('getAllItemObjects', array($UUID, $projectID, $projectType));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return IntegerResult
     */
    public function getDocumentStatus($UUID, $projectType, $itemID)
    {
      return $this->__soapCall('getDocumentStatus', array($UUID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $status
     * @access public
     * @return ItemListResult
     */
    public function getItemsByStatus1($UUID, $projectType, $status)
    {
      return $this->__soapCall('getItemsByStatus1', array($UUID, $projectType, $status));
    }

    /**
     * @param string $UUID
     * @param int $projectID
     * @param int $projectType
     * @param int $status
     * @access public
     * @return ItemListResult
     */
    public function getItemsByStatus2($UUID, $projectID, $projectType, $status)
    {
      return $this->__soapCall('getItemsByStatus2', array($UUID, $projectID, $projectType, $status));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $status
     * @param int $documentStatus
     * @access public
     * @return ItemListResult
     */
    public function getItemsByStatus3($UUID, $projectType, $status, $documentStatus)
    {
      return $this->__soapCall('getItemsByStatus3', array($UUID, $projectType, $status, $documentStatus));
    }

    /**
     * @param string $UUID
     * @param int $projectID
     * @param int $projectType
     * @param int $status
     * @param int $documentStatus
     * @access public
     * @return ItemListResult
     */
    public function getItemsByStatus4($UUID, $projectID, $projectType, $status, $documentStatus)
    {
      return $this->__soapCall('getItemsByStatus4', array($UUID, $projectID, $projectType, $status, $documentStatus));
    }

    /**
     * @param string $UUID
     * @param int $documentStatus
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return Result
     */
    public function setDocumentStatus($UUID, $documentStatus, $projectType, $itemID)
    {
      return $this->__soapCall('setDocumentStatus', array($UUID, $documentStatus, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $status
     * @param int $documentStatus
     * @param int $currencyType
     * @access public
     * @return ItemListResult
     */
    public function getItemsByStatus3ByCurrencyType($UUID, $projectType, $status, $documentStatus, $currencyType)
    {
      return $this->__soapCall('getItemsByStatus3ByCurrencyType', array($UUID, $projectType, $status, $documentStatus, $currencyType));
    }

    /**
     * @param string $UUID
     * @param int $workflowID
     * @param int $projectType
     * @param int $itemID
     * @access public
     * @return Result
     */
    public function copyJobsFromWorkflow($UUID, $workflowID, $projectType, $itemID)
    {
      return $this->__soapCall('copyJobsFromWorkflow', array($UUID, $workflowID, $projectType, $itemID));
    }

    /**
     * @param string $UUID
     * @param ItemIN $ItemIn
     * @access public
     * @return IntegerResult
     */
    public function insertLanguageIndependentItem($UUID, ItemIN $ItemIn)
    {
      return $this->__soapCall('insertLanguageIndependentItem', array($UUID, $ItemIn));
    }

    /**
     * @param string $UUID
     * @param int $projectID
     * @param int $projectType
     * @param int $status
     * @param int $documentStatus
     * @param int $currentType
     * @access public
     * @return ItemListResult
     */
    public function getItemsByStatus4ByCurrencyType($UUID, $projectID, $projectType, $status, $documentStatus, $currentType)
    {
      return $this->__soapCall('getItemsByStatus4ByCurrencyType', array($UUID, $projectID, $projectType, $status, $documentStatus, $currentType));
    }

    /**
     * @param string $UUID
     * @param int $itemID
     * @param int $projectType
     * @access public
     * @return PricelistListResult
     */
    public function getPricelist_List($UUID, $itemID, $projectType)
    {
      return $this->__soapCall('getPricelist_List', array($UUID, $itemID, $projectType));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $projectID
     * @param int $currencyType
     * @access public
     * @return ItemResult
     */
    public function getLanguageIndependentItemObject($UUID, $projectType, $projectID, $currencyType)
    {
      return $this->__soapCall('getLanguageIndependentItemObject', array($UUID, $projectType, $projectID, $currencyType));
    }

    /**
     * @param string $UUID
     * @param int $projectID
     * @param int $projectType
     * @param int $currencyType
     * @access public
     * @return ItemListResult
     */
    public function getAllItemObjectsByCurrency($UUID, $projectID, $projectType, $currencyType)
    {
      return $this->__soapCall('getAllItemObjectsByCurrency', array($UUID, $projectID, $projectType, $currencyType));
    }

    /**
     * @param string $UUID
     * @param string $sourceLanguage
     * @param string $targetLanguage
     * @param int $projectType
     * @param int $projectID
     * @access public
     * @return IntegerResult
     */
    public function addLanguageCombination2($UUID, $sourceLanguage, $targetLanguage, $projectType, $projectID)
    {
      return $this->__soapCall('addLanguageCombination2', array($UUID, $sourceLanguage, $targetLanguage, $projectType, $projectID));
    }

    /**
     * @param string $UUID
     * @param int $itemID
     * @param int $projectType
     * @param int $currencyType
     * @access public
     * @return PriceLineListResult
     */
    public function getPriceLine_ListByCurrency($UUID, $itemID, $projectType, $currencyType)
    {
      return $this->__soapCall('getPriceLine_ListByCurrency', array($UUID, $itemID, $projectType, $currencyType));
    }

    /**
     * @param string $UUID
     * @param int $PricelistID
     * @param string $SourceLanguage
     * @param string $TargetLanguage
     * @access public
     * @return PricelistEntryList
     */
    public function getPricelistEntry_List($UUID, $PricelistID, $SourceLanguage, $TargetLanguage)
    {
      return $this->__soapCall('getPricelistEntry_List', array($UUID, $PricelistID, $SourceLanguage, $TargetLanguage));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @param int $currencyType
     * @access public
     * @return DoubleResult
     */
    public function getTotalPriceByCurrencyType($UUID, $projectType, $itemID, $currencyType)
    {
      return $this->__soapCall('getTotalPriceByCurrencyType', array($UUID, $projectType, $itemID, $currencyType));
    }

    /**
     * @param string $UUID
     * @param int $itemID
     * @param int $projectType
     * @access public
     * @return PriceLineListResult
     */
    public function getPriceLine_List($UUID, $itemID, $projectType)
    {
      return $this->__soapCall('getPriceLine_List', array($UUID, $itemID, $projectType));
    }

    /**
     * @param string $UUID
     * @param int $projectType
     * @param int $itemID
     * @param int $currencyType
     * @access public
     * @return ItemResult
     */
    public function getItemObjectByCurrencyType($UUID, $projectType, $itemID, $currencyType)
    {
      return $this->__soapCall('getItemObjectByCurrencyType', array($UUID, $projectType, $itemID, $currencyType));
    }

    /**
     * @param string $UUID
     * @param string $languageCode
     * @param string $service
     * @access public
     * @return PriceUnitListResult
     */
    public function getPriceUnit_List($UUID, $languageCode, $service)
    {
      return $this->__soapCall('getPriceUnit_List', array($UUID, $languageCode, $service));
    }

}
