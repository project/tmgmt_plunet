<?php

namespace Drupal\tmgmt_plunet\Service\DataItem30;



class PriceUnitResult extends Result
{

    /**
     * @var PriceUnit $data
     * @access public
     */
    public $data = null;

    /**
     * @param int $statusCode
     * @param string $statusCodeAlphanumeric
     * @param string $statusMessage
     * @param PriceUnit $data
     * @access public
     */
    public function __construct($statusCode, $statusCodeAlphanumeric, $statusMessage, $data)
    {
      parent::__construct($statusCode, $statusCodeAlphanumeric, $statusMessage);
      $this->data = $data;
    }

}
