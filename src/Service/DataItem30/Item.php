<?php

namespace Drupal\tmgmt_plunet\Service\DataItem30;

class Item
{

    /**
     * @var string $briefDescription
     * @access public
     */
    public $briefDescription = null;

    /**
     * @var string $comment
     * @access public
     */
    public $comment = null;

    /**
     * @var dateTime $deliveryDeadline
     * @access public
     */
    public $deliveryDeadline = null;

    /**
     * @var int $invoiceID
     * @access public
     */
    public $invoiceID = null;

    /**
     * @var int $itemID
     * @access public
     */
    public $itemID = null;

    /**
     * @var int[] $jobIDList
     * @access public
     */
    public $jobIDList = null;

    /**
     * @var int $orderID
     * @access public
     */
    public $orderID = null;

    /**
     * @var int $projectID
     * @access public
     */
    public $projectID = null;

    /**
     * @var int $projectType
     * @access public
     */
    public $projectType = null;

    /**
     * @var string $reference
     * @access public
     */
    public $reference = null;

    /**
     * @var string $sourceLanguage
     * @access public
     */
    public $sourceLanguage = null;

    /**
     * @var int $status
     * @access public
     */
    public $status = null;

    /**
     * @var string $targetLanguage
     * @access public
     */
    public $targetLanguage = null;

    /**
     * @var float $totalPrice
     * @access public
     */
    public $totalPrice = null;

    /**
     * @param string $briefDescription
     * @param string $comment
     * @param dateTime $deliveryDeadline
     * @param int $invoiceID
     * @param int $itemID
     * @param int $orderID
     * @param int $projectID
     * @param int $projectType
     * @param string $reference
     * @param string $sourceLanguage
     * @param int $status
     * @param string $targetLanguage
     * @param float $totalPrice
     * @access public
     */
    public function __construct($briefDescription, $comment, $deliveryDeadline, $invoiceID, $itemID, $orderID, $projectID, $projectType, $reference, $sourceLanguage, $status, $targetLanguage, $totalPrice)
    {
      $this->briefDescription = $briefDescription;
      $this->comment = $comment;
      $this->deliveryDeadline = $deliveryDeadline;
      $this->invoiceID = $invoiceID;
      $this->itemID = $itemID;
      $this->orderID = $orderID;
      $this->projectID = $projectID;
      $this->projectType = $projectType;
      $this->reference = $reference;
      $this->sourceLanguage = $sourceLanguage;
      $this->status = $status;
      $this->targetLanguage = $targetLanguage;
      $this->totalPrice = $totalPrice;
    }

}
