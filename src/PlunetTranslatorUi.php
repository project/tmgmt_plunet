<?php

namespace Drupal\tmgmt_plunet;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\TranslatorPluginUiBase;

/**
 * Plunet translator UI.
 */
class PlunetTranslatorUi extends TranslatorPluginUiBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();

    $form['api_url'] = [
      '#type' => 'url',
      '#title' => t('Plunet API URL'),
      '#default_value' => $translator->getSetting('api_url'),
      '#description' => t('Please enter the Plunet API base URL.'),
      '#required' => TRUE,
    ];
    $form['api_user'] = [
      '#type' => 'textfield',
      '#title' => t('Plunet API username'),
      '#default_value' => $translator->getSetting('api_user'),
      '#description' => t('Please enter your Plunet API username.'),
      '#required' => TRUE,
    ];
    $form['api_password'] = [
      '#type' => 'textfield',
      '#title' => t('Plunet API password'),
      '#default_value' => $translator->getSetting('api_password'),
      '#description' => t('Please enter your Plunet API password.'),
      '#required' => TRUE,
    ];
    $form['site_token'] = [
      '#type' => 'textfield',
      '#title' => t('Custom site token'),
      '#default_value' => $translator->getSetting('site_token'),
      '#description' => t('Please enter the custom site token used as a Plunet API identifier when sending callback requests.'),
      '#required' => TRUE,
    ];
    $form['due_date_offset'] = [
      '#type' => 'number',
      '#min' => 1,
      '#title' => t('Default due date offset in working days'),
      '#default_value' => $translator->getSetting('due_date_offset') ?: 3,
      '#description' => $this->t('Number of working days in the future that are going to be applied as a default due date value.'),
    ];
    $form['plunet_languages'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Plunet language mappings'),
      '#description' => $this->t('A list of language mappings used by Plunet API. Please enter full language names.'),
    ];
    $plunet_languages = $translator->getSetting('plunet_languages') ?? [];
    foreach ($translator->getRemoteLanguagesMappings() as $local_langcode => $remote_language) {
      $form['plunet_languages'][$local_langcode] = [
        '#type' => 'textfield',
        '#size' => 30,
        '#required' => TRUE,
        '#title' => \Drupal::languageManager()->getLanguageName($local_langcode),
        '#default_value' => $plunet_languages[$local_langcode] ?? $this->getDefaultPlunetLanguageMapping($local_langcode),
      ];
    }

    $form += parent::addConnectButton();
    $form['connect']['#submit'] = [
      [$this, 'testPlunetApiConnection'],
    ];

    return $form;
  }

  /**
   * Returns a corresponding Plunet language code for the given Drupal langcode.
   */
  protected function getDefaultPlunetLanguageMapping(string $langcode) {
    switch ($langcode) {
      case 'en':
        return 'English';
      case 'de':
        return 'German (CH)';
      case 'fr':
        return 'French (CH)';
      case 'it':
        return 'Italian (CH)';
      default:
        return '';
    }
  }

  /**
   * Form submit callback to test the Plunet API connection.
   */
  public function testPlunetApiConnection(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    /** @var \Drupal\tmgmt_plunet\Plugin\tmgmt\Translator\PlunetTranslator $translator_plugin */
    $translator_plugin = $translator->getPlugin();
    $translator_plugin->setTranslator($translator);

    try {
      if ($translator_plugin->isValidAccount($form_state->getValue(['settings', 'api_url']), $form_state->getValue(['settings', 'api_user']), $form_state->getValue(['settings', 'api_password']))) {
        $this->messenger()->addStatus(t('Successfully connected!'));
        return TRUE;
      }
    }
    catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
      return FALSE;
    }

    $this->messenger()->addError(t('Connection failed. Please check the entered credentials!'));
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutInfo(JobInterface $job) {
    $form = [];

    if ($job->isActive()) {
      $form['actions']['pull'] = [
        '#type' => 'submit',
        '#value' => $this->t('Fetch translations'),
        '#submit' => [[$this, 'submitFetchTranslations']],
        '#weight' => -10,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(array $form, FormStateInterface $form_state, JobInterface $job) {
    $settings['description'] = [
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#default_value' => $job->getSetting('description'),
      '#description' => t('Set the order request description.'),
    ];
    // Get a number of offset days or fallback to 3 working days.
    $offset_days = $job->getTranslator()->getSetting('due_date_offset') ?: 3;
    $deadline = $job->getSetting('deadline');
    if (isset($deadline['object']) && $deadline['object'] instanceof DrupalDateTime) {
      $default_due = $deadline['object'];
    }
    else {
      $default_due = new DrupalDateTime("+$offset_days weekday 12:00");
      $timezone = 'Europe/Zurich';
      $default_due->setTimeZone(timezone_open($timezone));
    }
    $settings['deadline'] = [
      '#type' => 'datetime',
      '#date_timezone' => $timezone,
      '#title' => $this->t('Due date'),
      '#date_increment' => ':30',
      '#default_value' => $default_due,
      '#element_validate' => [[get_class($this), 'validateDueDate']],
      '#description' => $this->t('Due will be set using %timezone timezone.', ['%timezone' => $timezone]),
    ];

    return $settings;
  }

  /**
   * Validate that the due date is in the future.
   *
   * @param array $element
   *   The input element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateDueDate(array $element, FormStateInterface &$form_state) {
    $current_date = new DrupalDateTime();
    $current_date->setTimeZone(timezone_open('Europe/Zurich'));
    if (isset($element['#value']['object'])) {
      /** @var \Drupal\Core\Datetime\DrupalDateTime $deadline */
      $deadline = $element['#value']['object'];

      if ($deadline <= $current_date) {
        $form_state->setError($element, t('Due date must be in the future.'));
      }
      elseif (!$form_state->isRebuilding()) {
        $form_state->setValue([
          'settings',
          'deadline',
          // Use the UTC time as remote system defaults to Europe/Zurich time.
        ], $deadline->format('Y-m-d\TH:i:s', ['timezone' => 'UTC']));
      }
    }
    else {
      $form_state->setError($element, t('Due date is invalid. Please enter in mm/dd/yyyy format or select from calendar.'));
    }
  }

  /**
   * Submit callback to fetch translations from Plunet API.
   */
  public function submitFetchTranslations(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\tmgmt\Entity\Job $job */
    $job = $form_state->getFormObject()->getEntity();

    /** @var \Drupal\tmgmt_plunet\Plugin\tmgmt\Translator\PlunetTranslator $translator_plugin */
    $translator_plugin = $job->getTranslator()->getPlugin();
    $translator_plugin->setTranslator($job->getTranslator());
    $translator_plugin->fetchTranslations($job);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    /** @var \Drupal\tmgmt\Entity\Job $job */

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    /** @var \Drupal\tmgmt_plunet\Plugin\tmgmt\Translator\PlunetTranslator $translator_plugin */
    $translator_plugin = $translator->getPlugin();
    $translator_plugin->setTranslator($translator);
    // Clear the remote UUID as the credentials might have changed.
    $translator_plugin->clearRemoteUuid();
  }

}
