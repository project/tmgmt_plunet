<?php

namespace Drupal\tmgmt_plunet;

use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\Entity\JobItem;
use Drupal\tmgmt\Entity\Translator;
use Drupal\tmgmt_plunet\Service\DataRequest30\EventType;

/**
 * Handles Plunet callback calls.
 */
class PlunetCallbacks {

  /**
   * Whether the given event is supported.
   */
  protected function isSupportedEvent($event_type) {
    return in_array($event_type, [
      EventType::STATUS_CHANGED,
      EventType::NEW_ENTRY_CREATED,
    ]);
  }

  /**
   * Whether the given site token/authentication code is valid.
   */
  protected function getTranslatorByToken($token) {
    $translator_ids = \Drupal::entityQuery('tmgmt_translator')->condition('settings.site_token', $token)->range(0, 1)->execute();
    if (empty($translator_ids)) {
      throw new \SoapFault('Server', 'Invalid authentication code.');
    }
    $translator_id = reset($translator_ids);
    /** @var  \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = Translator::load($translator_id);

    return $translator;
  }

  /**
   * Triggers for the order request callback.
   */
  public function receiveOrderRequestCallback($auth_code, $request_id, $event_type) {
    if (!$this->isSupportedEvent($event_type)) {
      throw new \SoapFault('Server', 'Unsupported event type.');
    }
    $translator = $this->getTranslatorByToken($auth_code);

    $job_ids = \Drupal::entityQuery('tmgmt_job')->condition('reference', $request_id)->range(0, 1)->execute();
    if (empty($job_ids)) {
      throw new \SoapFault('Server', 'No matching jobs.');
    }
    $job_id = reset($job_ids);
    $job = Job::load($job_id);

    // Trigger fetch translations call.
    $translator->getPlugin()->fetchTranslations($job);
  }

  /**
   * Triggers for the order callback.
   */
  public function receiveOrderCallback($auth_code, $order_id, $event_type) {
    if (!$this->isSupportedEvent($event_type)) {
      throw new \SoapFault('Server', 'Unsupported event type.');
    }
    $translator = $this->getTranslatorByToken($auth_code);

    $job_item_ids = \Drupal::entityQuery('tmgmt_job_item')->condition('remote_identifier_3', $order_id)->range(0, 1)->execute();
    if (empty($job_item_ids)) {
      throw new \SoapFault('Server', 'No matching job items.');
    }
    $job_item_id = reset($job_item_ids);
    /** @var \Drupal\tmgmt\JobItemInterface $job_item */
    $job_item = JobItem::load($job_item_id);

    // Trigger fetch translations call.
    $translator->getPlugin()->fetchTranslations($job_item->getJob());
  }

}
